import React from "react";

const Footer = () => {
    return (
        <>
            <footer>
                <div className="footer-app-newsletter text-center">
                    <div className="container-fluid">
                        <div className="row align-items-center">
                            <div className="col-md-6 download-app">
                                <h1>Download theShoply App</h1>
                                <div className="app-download-btn">
                                    <button className="android-app-btn btn">
                                        <i className="fab fa-google-play"></i>
                                        <span className="get-it-on">Get Android App On</span>
                                        <span className="get-from">Google Play</span>
                                    </button>
                                    <button className="ios-app-btn btn">
                                        <i className="fab fa-app-store-ios"></i>
                                        <span className="get-it-on">Get IOS App On</span>
                                        <span className="get-from">App Store</span>
                                    </button>
                                </div>
                            </div>
                            <div className="col-md-6 subscribe-newsletter">
                                <h1>Subscribe for latest offer</h1>
                                <div className="subscribe-form">
                                    <form action="#">
                                        <div className="input-group">
                                            <input type="text" className="form-control" placeholder="Enter your phone number" required />
                                            <div className="input-group-append">
                                                <button className="btn" type="button" title="press the bell icon">
                                                    <i className="far fa-bell"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="footer-details">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-lg-3">
                                <div className="footer-details-wrap">
                                    <div className="footer-logo">
                                        <a href="index.html">
                                            <img src="/img/logo.png" alt="theShoply logo" />
                                        </a>
                                    </div>
                                    <div className="footer-help-support">
                                        <p className="f-hs">
                                            <a href="#">About us</a>,
                                            <a href="#">Contact us</a>,
                                            <a href="#">Privacy policy</a>,
                                            <a href="#">Purchase policy</a>,
                                            <a href="#">Cookie policy</a>,
                                            <a href="#">Terms & conditions</a>,
                                            <a href="#">Returns policy</a>,
                                            <a href="#">FAQ.</a>
                                        </p>
                                        <p className="f-si">
                                            <a href="#"><i className="fab fa-facebook-f"></i></a>
                                            <a href="#"><i className="fab fa-twitter"></i></a>
                                            <a href="#"><i className="fab fa-pinterest-p"></i></a>
                                            <a href="#"><i className="fab fa-linkedin-in"></i></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-6">
                                <div className="footer-product-cat">
                                    <h1>Populer Category</h1>
                                    <ul>
                                        <li><a href="#"><i className="fas fa-caret-right"></i> Baby Products</a></li>
                                        <li><a href="#"><i className="fas fa-caret-right"></i> Food & Beverage</a></li>
                                        <li><a href="#"><i className="fas fa-caret-right"></i> Beauty & Health</a></li>
                                        <li><a href="#"><i className="fas fa-caret-right"></i> Home Appliences</a></li>
                                        <li><a href="#"><i className="fas fa-caret-right"></i> Electronics</a></li>
                                        <li><a href="#"><i className="fas fa-caret-right"></i> Pet Care</a></li>
                                        <li><a href="#"><i className="fas fa-caret-right"></i> Home Decoration</a></li>
                                        <li><a href="#"><i className="fas fa-caret-right"></i> Office Equipment</a></li>
                                        <li><a href="#"><i className="fas fa-caret-right"></i> Miscellaneous</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-lg-3">
                                <div className="footer-contact-details">
                                    <h1>Contact Us</h1>
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <img src="/img/location.png" alt="Address" />
                                                <p>Laily Bhaban, 03 Assam Colony,
                                                    Rajshahi-6203, Bangladesh</p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="tel:+8801718659502" className="sdsp-info-box">
                                                <img src="/img/help-line.png" alt="Hotline" />
                                                <p>
                                                    <span className="sdsp-title">Hotline Number</span>
                                                    <span className="sdsp-details">+880 1913 800 800</span>
                                                </p>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="mailto:info@onushorit.com" className="sdsp-info-box">
                                                <img src="/img/email.png" alt="Hotline" />
                                                <p>
                                                    <span className="sdsp-title">Email Us</span>
                                                    <span className="sdsp-details">support@desktopit.com.bd</span>
                                                </p>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="footer-copyright">
                    <div className="container-fluid">
                        <div className="row align-items-center">
                            <div className="col-lg-6 col-md-7 col-sm-7">
                                <img className="pay-with" src="/img/paywith.png" alt="Pay with SSLCommerz" />
                            </div>
                            <div className="col-lg-3 offset-lg-3 col-md-5 col-sm-5">
                                <p className="fcr-text"><i className="far fa-copyright"></i> theShoply 2020</p>
                                <p className="fpb-text">Powered by <a href="http://desktopit.com.bd/" target="_blank">Desktop IT</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>

        </>
    )
}
export default Footer;
