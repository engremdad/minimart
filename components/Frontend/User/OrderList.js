import React from 'react';


const OrderList = (props) => {

    return (
        <>
            <div className="col-md-7 mb-3">
                <div className="custom-card-box">
                    <div className="custom-card-box-header">
                        <div className="fbh-title">
                            <i className="icon-list2" />
                            <h2>Order List</h2>
                        </div>
                    </div>
                    <div className="custom-card-box-content p-md-3 p-2">
                        <div className="row table-top-option mb-md-3 mb-2">
                            <div className="col-6">
                                <div className="showing-table-row">
                                    <label className="">Show</label>
                                    <select className="custom-select" defaultValue={`25`}>
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                            </div>
                            <div className="col-6">
                                <div className="table-search">
                                    <label className="">Search</label>
                                    <input type="text" className="form-control"
                                        placeholder="Search here ..." />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="table-responsive custom-table custom-hight-800">
                                    <table className="table table-bordered table-hover mb-0 carsour-pointer">
                                        <thead className="thead-light">
                                            <tr>
                                                <th scope="col">Order</th>
                                                <th scope="col">Total</th>
                                                <th scope="col">Payment</th>
                                                <th scope="col">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                props.ListOrder.length > 0 ? (props.ListOrder.map(item =>
                                                (
                                                    <tr key={item.id} onClick={(e) => {
                                                        e.preventDefault();
                                                        props.showOrderDetails(item.id);
                                                    }}>

                                                        <td className="nowrap text-center">
                                                            {item.order_number}
                                                        </td>

                                                        <td className="text-center text-nowrap">
                                                            ৳ {item.grand_total}
                                                        </td>

                                                        <td className="text-center">
                                                            {item.payment_status}
                                                        </td>
                                                        <td className="text-center">
                                                            {item.order_status}
                                                        </td>

                                                    </tr>
                                                )
                                                )) : (
                                                        <tr>
                                                            <td colSpan="4" className="text-center">No Order Found</td>
                                                        </tr>
                                                    )
                                            }

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default OrderList;