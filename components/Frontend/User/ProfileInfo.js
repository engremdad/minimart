import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";
import axios from "axios";
import process from "../../../next.config";

const ProfileInfo = (props) => {

    const api_url = process.env.mode === 'development' ? process.env.api_url_dev : process.env.api_url;

    const [loading, setLoading] = useState(false);
    const [message, setMessage] = useState('');
    const [errors, setErrors] = useState({});
    const [isError, setIsError] = useState(false);

    const [userData, setUserData] = useState({
        first_name: '',
        last_name: '',
        email: '',
        phone: ''
    });

    useEffect(() => {
        setUserData({
            first_name: props.authUser.first_name,
            last_name: props.authUser.last_name,
            email: props.authUser.email,
            phone: props.authUser.phone
        });
    }, [props.authUser])


    let handleChange = (e) => {
        e.preventDefault();
        let name = e.target.name;
        let value = e.target.value;

        setUserData({...userData, [name]: value});

        setErrors({
            ...errors,
            [name]: ''
        });
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        setLoading(true);


        let response = await axios.post(`${api_url}/auth/customers/update`, userData)
            .then((res) => {
                return res.data;
            }).catch((err) => {
                return err;
            });

        if (response.success) {
            setLoading(false);
            setIsError(false);
            setMessage(response.message);
        } else {
            setLoading(false);
            setMessage(response.message);
            setErrors(response.errors);
            setIsError(true);
        }

    }

    return (
        <>
            <div className="card checkout-details-card rounded-0 mb-4">

                <div className="card-header">
                    <span className="badge">01</span>
                    Personal Details
                </div>

                <div className="card-body">
                    <form>
                        <div className="form-group">
                            <label>Your Name</label>
                            <div className="row no-gutters">

                                <div className="col-6">

                                    <input
                                        id="firstname"
                                        name="first_name"
                                        onChange={handleChange}
                                        defaultValue={userData && userData.first_name}
                                        type="text"
                                        className={`form-control rounded-0 ${errors.first_name && `is-invalid`}`}
                                        placeholder="First Name"/>

                                    <div id="first_name"
                                         className={errors.first_name && `invalid-feedback`}>
                                        {errors && errors.first_name}
                                    </div>

                                </div>

                                <div className="col-6">

                                    <input
                                        id="lastname"
                                        name="last_name"
                                        onChange={handleChange}
                                        defaultValue={userData && userData.last_name}
                                        type="text"
                                        className={`form-control rounded-0 ${errors.last_name && `is-invalid`}`}
                                        placeholder="Last Name"/>

                                    <div id="last_name"
                                         className={errors.last_name && `invalid-feedback`}>
                                        {errors && errors.last_name}
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div className="form-group">
                            <label>Email Address</label>

                            <input
                                id="email_address"
                                name="email"
                                onChange={handleChange}
                                defaultValue={userData && userData.email}
                                type="email"
                                className={`form-control rounded-0 ${errors.email && `is-invalid`}`}
                                placeholder="e.g. desktopit@gmail.com" readOnly/>

                            <div id="email_address"
                                 className={errors.email && `invalid-feedback`}>
                                {errors && errors.email}
                            </div>

                        </div>
                        <div className="form-group">
                            <label className="col-12">Mobile Number</label>

                            <div className="input-group mb-3">

                                <div className="input-group-prepend rounded-0">
                                    <span className="input-group-text rounded-0" id="basic-addon1">+880</span>
                                </div>

                                <input
                                    id="phone_number"
                                    name="phone"
                                    type="text"
                                    onChange={handleChange}
                                    defaultValue={userData && userData.phone}
                                    className={`form-control rounded-0 ${errors.phone && `is-invalid`}`}
                                    placeholder="e.g. 1913800877"/>

                                <div id="phone"
                                     className={errors.phone && `invalid-feedback`}>
                                    {errors && errors.phone}
                                </div>

                            </div>

                        </div>

                        <div className="form-group d-flex justify-content-end text-center">

                            <button
                                onClick={handleSubmit}
                                type="submit"
                                className={`btn btn-primary`}>
                                {loading ? `Updating...` : `Update`}
                                <i className={loading ? `spinner-border spinner-border-sm` : ''}/>
                            </button>

                        </div>


                    </form>

                    <div className={`${isError ? "text-danger" : "text-success"} text-left`}>{message}</div>
                </div>
            </div>
        </>
    );
};

const mapStateToProps = (store) => {
    const {authUser, customerAllAddresses} = store.deliveryStore;
    return {authUser, customerAllAddresses};

}


export default connect(mapStateToProps)(ProfileInfo);