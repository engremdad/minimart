import React from 'react';

const OrderDetails = (props) => {

    return (
        <>

            <div className="custom-card-box mb-3">
                <div className="custom-card-box-header">
                    <div className="fbh-title">
                        <i className="icon-eye-regular" />
                        <h2>Order Details</h2>
                        <h2 className="ml-auto border-0"><a href="#" data-toggle="modal" data-target="#reportOrderModal">Report Issues</a></h2>
                    </div>
                </div>
                <div className="custom-card-box-content p-md-3 p-2">
                    <div className="row">
                        <div className="col-lg-12">

                            <div className="card mb-md-3 mb-2 rounded-0">
                                <div className="card-body card-da-dtl rounded-0">
                                    <i className="icon-map-marker-alt-solid"></i>
                                    <h6 className="card-title mb-2">Delivery address</h6>
                                    <p className="card-subtitle mb-0 text-muted">{(props.OrderInfo) && (props.OrderInfo.address) ? props.OrderInfo.address:''}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="table-responsive custom-table">
                                <table className="table table-bordered table-hover mb-0 table-order-details">
                                    <thead className="thead-light">
                                        <tr>
                                            <th scope="col">SL</th>
                                            <th scope="col">Img</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Qty.</th>
                                            <th scope="col">Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            ((props.OrderInfo) && (props.OrderInfo.saleProducts.length > 0)) ? (props.OrderInfo.saleProducts.map((item, index) =>
                                            (
                                                <tr key={item.id} className="text-center">
                                                    <td scope="row">
                                                        {++index}
                                                    </td>

                                                    <td className="text-center">
                                                        <img src={item.product.feature_image} alt="Product Img" />
                                                    </td>
                                                    <td className="text-center">
                                                        <h6 className="text-875 mb-1">{item.product.name}</h6>
                                                        <p className="mb-0 text-75 text-muted">{item.product.weight} {item.product.sale_unit} / ৳ {item.unit_price}</p>
                                                    </td>
                                                    <td className="text-center text-nowrap">
                                                        {item.quantity} {item.sale_unit}
                                                    </td>
                                                    <td className="text-center text-nowrap">
                                                        ৳ {item.price}
                                                    </td>
                                                </tr>
                                            )
                                            )) : (
                                                    <tr>
                                                        <td colSpan="5" className="text-center">No Order Found</td>
                                                    </tr>
                                                )
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div className="modal fade" id="reportOrderModal" tabindex="-1" aria-labelledby="reportOrderLebel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content rounded-0 border-0">
                        <div className="modal-header">
                            <h5 className="modal-title" id="reportOrderLebel">What went wrong?</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <div className="check-list-report">
                                <input type="checkbox" id="1" className="vh" />
                                <label for="1">Wrong product</label>
                            </div>
                            <div className="check-list-report">
                                <input type="checkbox" id="2" className="vh" />
                                <label for="2">Expaired product</label>
                            </div>
                            <div className="check-list-report">
                                <input type="checkbox" id="3" className="vh" />
                                <label for="3">LMissing product</label>
                            </div>
                            <div className="check-list-report">
                                <input type="checkbox" id="4" className="vh" />
                                <label for="4">Product quality was bad</label>
                            </div>
                            <div className="check-list-report">
                                <input type="checkbox" id="5" className="vh" />
                                <label for="5">My order arived late</label>
                            </div>
                            <div className="check-list-report">
                                <input type="checkbox" id="6" className="vh" />
                                <label for="6">I don't like the packaging</label>
                            </div>
                            <div class="form-group mt-4 pt-1">
                                <textarea class="form-control rounded-0" rows="3" placeholder="Please discribe issues ..."></textarea>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary rounded-0" data-dismiss="modal">Cancel</button>
                            <button type="button" className="btn btn-danger rounded-0">Submit Report</button>
                        </div>
                    </div>
                </div>
            </div>

        </>
    );
};

export default OrderDetails;