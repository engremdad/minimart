import React from 'react';
import { connect } from "react-redux";
import { SHOW_CART_MENU } from "../../../store/frontend/actions/productAction";


const CartNav = (props) => {

    return (
        <>
            <div
                className="cart-fixed-menu-open shadow"
                onClick={(event => {
                    event.preventDefault();
                    props.showCartMenu();
                })}
                data-menu="#cartNav">

                <div className="cart-items">
                    <i className="icon-shopping-bag-solid"></i>
                    <span className="total-cart-item"> {props.cart.length > 0 ? props.cart.length : 0} Items</span>
                </div>

                <div className="cart-amounts">
                    <i className="icon-coin"></i>
                    <span className="total-cart-amounts">৳ {props.sub_total}</span>
                </div>

                <div className="view-cart-btn"><i className="icon-long-arrow-alt-left-solid"></i>
                    <span className="view-cart">View Cart</span>
                </div>
            </div>
        </>

    );
};

const mapStateToPros = (store) => {
    const { cart, sub_total } = store.cartStore;
    return { cart, sub_total }
}

const mapDispatchToProps = (dispatch) => {
    return {
        showCartMenu: () => dispatch({ type: SHOW_CART_MENU }),
    }
}

export default connect(mapStateToPros, mapDispatchToProps)(CartNav)