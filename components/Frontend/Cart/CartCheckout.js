import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";
import {CART_CHECK_OUT} from "../../../store/frontend/actions/cartAction";
import Storage from "../../../Helpers/Auth/Storage";
import Router from "next/router";


const CartCheckout = (props) => {

    const api_url = process.env.mode === 'development' ? process.env.api_url_dev : process.env.api_url;


    const [isLogin, setIsLogin] = useState(false);

    useEffect(() => {
        if (Storage.isLoggedIn()) {
            setIsLogin(true);
        } else {
            setIsLogin(false);
        }
    }, []);

    const checkOut = async () => {

        // let response = await axios.post(`${api_url}/sales`, props.cartStore).then(result => {
        //     return result.data;
        // }).catch(err => {
        //     return err;
        // });
        //
        // if (response.success) {
        //     props.cartCheckout();
        //     CartStorage.removeCartItem();
        // }

        await Router.push('/cart/checkout');
    }

    return (
        <>
            <div className="cart-checkout">
                <button data-toggle={!isLogin ? `modal` : ``}
                        data-target={!isLogin ? `#signin_modal` : ``}>
                            <span className="check-out-text"
                                  onClick={() => isLogin ? props.cartStore.sub_total > 0 ? checkOut() : '' : ''}><i
                                className="fas fa-shield-alt"></i> Check Out</span>
                    <span className="cart-total">৳ {props.cartStore.sub_total}</span>
                </button>
            </div>
        </>
    );
};


const mapDispatchToProps = (dispatch) => {
    return {cartCheckout: () => dispatch({type: CART_CHECK_OUT})};
}

const mapStateToProps = (store) => {
    const cartStore = store.cartStore;
    return {cartStore};
}

export default connect(mapStateToProps, mapDispatchToProps)(CartCheckout);