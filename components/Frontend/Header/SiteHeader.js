import Head from "next/head";
import React from "react";

const AdminHeader = (props)=>{
    return(
       <>
           <Head>
               <title>{props.title ? props.title : 'theShoply - Best Online Grocery Shop in Bangladesh | Buy Grocery, Cosmetics, Diapers, Food items, Vegetables Online and Get Delivery in 90 Minutes'}</title>
               <meta name="keywords" content={props.keywords ? props.keywords : 'bangladesh online grocery, bangladesh bazaar, best bazaar, daily bazaar, daily shop, large shop, bd shop, shop, online store,  buy, sell, home shop, Meat, Oil, Chal, Free home delivery, Fresh vegetables, formalin free, free return'} />
               <meta name="description" content={props.description ? props.description : 'Order grocery and food online with same-day home delivery. Save money, save time'} />
               <meta property="og:title" content={props.ogTitle ? props.ogTitle : 'bangladesh online grocery, bangladesh bazaar, best bazaar, daily bazaar, daily shop, large shop, bd shop, shop, online store,  buy, sell, home shop, Meat, Oil, Chal, Free home delivery, Fresh vegetables, formalin free, free return'} />
               <meta property="og:type" content={props.ogType ? props.ogType : 'website'} />
               <meta property="og:image" content={props.ogImage ? props.ogImage : 'https://cdn.chaldal.net/asset/Egg.Grocery.Fabric/Egg.Grocery.Web/1.5.0+Release-1564/Default/resources/images/egg.png?q=low&alpha=1'} />
               <meta property="og:site_name" content={props.ogSiteName ? props.ogSiteName : 'the Shoply'} />
               <meta property="og:description" content={props.ogDescription ? props.ogDescription : 'Order grocery and food online with same-day home delivery. Save money, save time'} />
           </Head>
       </>
    );
}
export default AdminHeader;
