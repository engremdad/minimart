import React, { useEffect, useState } from 'react';
import { useRouter } from "next/router";
import Storage from "../../../Helpers/Auth/Storage";

const LoginSection = () => {

    const router = useRouter();

    const [isLogin, setIsLogin] = useState(false);

    useEffect(() => {
        if (Storage.isLoggedIn()) {
            setIsLogin(true);
        } else {
            setIsLogin(false);
        }
    });

    const logout = () => {

        setIsLogin(false);
        Storage.clear();
        router.reload();
    }

    return (
        <>
            <div className="btn-group">

                {
                    isLogin ? (
                        <>
                            <a href="#"
                                role="button"
                                className="btn dropdown-toggle"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false">
                                <i className="far fa-user"></i>
                            </a>
                            <div className="dropdown-menu dropdown-menu-right shadow-sm">
                                <a href="#"
                                    className="dropdown-item"
                                    onClick={() => router.push('/user/user-info')}
                                    type="button">
                                    <i className="icon-user-regular"></i>
                                    <span>User Information</span>

                                </a>
                                <a href="#"
                                    className="dropdown-item"
                                    onClick={() => router.push('/user/order-info')}
                                    type="button">
                                    <i className="icon-list2"></i>
                                    <span>Order Information</span>
                                </a>

                                <a href="#"
                                    className="dropdown-item"
                                    onClick={() => router.push('/user/change-password')}
                                    type="button">
                                    <i className="icon-key"></i>
                                    <span>Change Password</span>
                                </a>
                                <a href="#" className="dropdown-item" type="button"
                                    onClick={() => logout()}>
                                    <i className="icon-switch"></i>
                                    <span>Logout</span>
                                </a>
                            </div>
                        </>
                    ) : (
                            <a
                                className="btn dropdown-toggle"
                                data-toggle="modal"
                                data-target="#signin_modal">
                                <i className="far fa-user"></i>
                            </a>
                        )
                }


            </div>
        </>
    );
};

export default LoginSection;