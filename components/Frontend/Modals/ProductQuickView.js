import React, {useState} from 'react';
import {ADD_TO_CART} from "../../../store/frontend/actions/cartAction";
import {connect} from "react-redux";

const ProductQuickView = (props) => {

    const [count, setCount] = useState(1);

    return (
        <>
            <div className="modal fade" id="product_quick_view_modal" tabIndex="-1"
                 aria-labelledby="productQuickViewLabel"
                 aria-hidden="true">
                <div className="modal-dialog modal-xl modal-dialog-centered">
                    <div className="modal-content rounded-0 border-0">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body product-details-main">
                            <div className="product-details-wrap">
                                <div className="pdw-top">
                                    <div className="pwdt-left pwdt-grid">
                                        <div id="productDisplayCarousel" className="carousel slide carousel-fade"
                                             data-ride="carousel">
                                            <div className="carousel-inner">
                                                {props.product.product_image ? props.product.product_image.map((image, index) =>
                                                    <div className={`carousel-item ${index ? "" : "active"}`}>
                                                        <img src={image}
                                                             className="d-block w-100" alt="Product Name"/>
                                                    </div>
                                                ) : ""}
                                            </div>
                                            <ol className="carousel-indicators">
                                                {props.product.product_image ? props.product.product_image.map((image, index) =>
                                                    <li
                                                        key={++index}
                                                        data-target="#productDisplayCarousel"
                                                        data-slide-to={index}
                                                        className={`${index ? "" : "active"}`}>
                                                        <img className="d-block w-100"
                                                             src={image} alt="Product Name"/>
                                                    </li>
                                                ) : ""}

                                            </ol>
                                        </div>
                                    </div>
                                    <div className="pwdt-right pwdt-grid">
                                        <div className="product-title">
                                            <h1>{props.product.name}</h1>
                                        </div>
                                        <div className="product-review">
                                            <div className="review-summary">
                                                <i className="icon-star-full"></i>
                                                <i className="icon-star-full"></i>
                                                <i className="icon-star-full"></i>
                                                <i className="icon-star-half"></i>
                                                <i className="icon-star-empty"></i>
                                                <span>(5 reviews)</span>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div className="product-qan-price-off">
                                            <div className="product-qan-price">
                                                <div className="product-quantity">
                                                    <span>{props.product.weight} {props.product.sale_unit}</span>
                                                </div>
                                                <div className="product-price">
                                                    <span
                                                        className="off-price">৳ {props.product.selling_price - props.product.discount}</span>
                                                    {props.product.discount ?
                                                        (<span
                                                            className="original-price">৳ {props.product.selling_price}</span>) : ""
                                                    }
                                                </div>
                                            </div>
                                            <div className="off-percentage">
                                                <span>20% off</span>
                                            </div>
                                        </div>
                                        <div className="product-small-des">
                                            <p>
                                                {props.product.description}
                                            </p>
                                        </div>
                                        <div className="availability-sku">
                                            <div className="sku">
                                                SKU : <span>{props.product.code}</span>
                                            </div>
                                            <div className="availability">
                                                AVAILABILITY : <span
                                                className="badge badge-pill badge-primary">In Stock</span>
                                            </div>
                                        </div>
                                        <div className="add-to-cart">
                                            <div className="cart-count input-group">
                                                <button className="decrise"
                                                        onClick={() => {
                                                            if (count > 1)
                                                                setCount(count - 1);
                                                        }}
                                                >
                                                </button>
                                                <input name="qty" type="text" readOnly value={count}/>
                                                <button className="incrise"
                                                        onClick={() => setCount(count + 1)}
                                                >
                                                </button>
                                            </div>
                                            <div className="acrt-btn">
                                                <button
                                                    className="btn btn-atc"
                                                    onClick={event => {
                                                        event.preventDefault();
                                                        props.addToCart(props.product, count)
                                                    }}
                                                >
                                                    Add to cart
                                                </button>
                                            </div>
                                        </div>
                                        <div className="social-share">
                                            <div className="share-icon">
                                                <a href="#">
                                                    <i className="icon-facebook"></i>
                                                </a>
                                                <a href="#">
                                                    <i className="icon-twitter"></i>
                                                </a>
                                                <a href="#">
                                                    <i className="icon-linkedin2"></i>
                                                </a>
                                                <a href="#">
                                                    <i className="icon-whatsapp"></i>
                                                </a>
                                                <a href="#">
                                                    <i className="icon-instagram"></i>
                                                </a>
                                                <a href="#">
                                                    <i className="icon-envelope-regular"></i>
                                                </a>
                                            </div>
                                            <div className="add-to-wishlist">
                                                <button className="btn btn-atw">
                                                    <i className="icon-heart-regular"></i>
                                                    <span>Add to wishlist</span>
                                                </button>
                                            </div>
                                        </div>
                                        <div className="pay-with">
                                            <img src="img/paywith.png" alt="Pay with"/>
                                        </div>

                                    </div>
                                </div>
                                <div className="pdw-bottom"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};
const mapDispatchToProps = (dispatch) => {

    return {
        addToCart: (product, qty) => dispatch({type: ADD_TO_CART, payload: {product, qty}}),
    }
}
export default connect(null, mapDispatchToProps)(ProductQuickView)