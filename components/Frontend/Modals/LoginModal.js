import React, {useState} from 'react';
import axios from "axios";
import {useRouter} from "next/router";
import Storage from "../../../Helpers/Auth/Storage";

const LoginModal = () => {

    const router = useRouter();

    const api_url = process.env.mode === 'development' ? process.env.api_url_dev : process.env.api_url;

    const [loading, setLoading] = useState(false);
    const [isError, setIsError] = useState(false);
    const [errors, setErrors] = useState([]);
    const [loginData, setLoginData] = useState({
        email: '',
        password: ''
    });

    const handleLoginData = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setLoginData({...loginData, [name]: value});
    }


    const handleLogin = async (event) => {
        event.preventDefault();

        setLoading(true);

        let response = await axios.post(`${api_url}/auth/customers/login`, loginData).then((result) => {
            return result.data;
        }).catch((exception) => {
            return exception;
        });

        // Success Login
        if (response.success) {

            // To hide the modal dialog
            hideModal();

            setLoading(false);

            //Save authentication to cookies
            Storage.store(JSON.stringify(response.data));

            if (Storage.isLoggedIn()) {
                router.reload();
            }
        }
        // Login Failed
        else {
            setLoading(false);
            setIsError(true);
            setErrors(response.errors);
        }
    }

    const hideModal = () => {
        $('#signin_modal').modal('hide');
    }


    return (
        <>
            <div className="modal fade" id="signin_modal" tabIndex="-1"
                 aria-labelledby="signInModalLevel"
                 aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="signInModalLevel">Sign In</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <div className="row">

                                <div className="col-12">

                                    <div className="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                                         aria-orientation="vertical">

                                        <a className="nav-link active"
                                           id="v-pills-email-login-tab"
                                           data-toggle="pill"
                                           href="#v-pills-email-login"
                                           role="tab"
                                           aria-controls="v-pills-email-login"
                                           aria-selected="true">
                                            <i className="icon-envelope-regular mr-2 pt-1"/>Login
                                            with <strong>Email</strong>

                                        </a>

                                        {/*<a className="nav-link"*/}
                                        {/*   id="v-pills-phone-login-tab"*/}
                                        {/*   data-toggle="pill"*/}
                                        {/*   href="#v-pills-phone-login"*/}
                                        {/*   role="tab"*/}
                                        {/*   aria-controls="v-pills-phone-login"*/}
                                        {/*   aria-selected="false">*/}
                                        {/*    <i className="icon-phone mr-2"/> Login with <strong>Phone Number</strong>*/}
                                        {/*</a>*/}

                                    </div>

                                </div>

                                <div className="col-12 mt-4">

                                    <div className="tab-content" id="v-pills-tabContent">

                                        <div
                                            className="tab-pane fade show active"
                                            id="v-pills-email-login"
                                            role="tabpanel"
                                            aria-labelledby="v-pills-email-login-tab">

                                            <div className="login-form">
                                                <form>

                                                    <div className="form-group">

                                                        <label
                                                            className="sr-only"
                                                            htmlFor="email">
                                                            User ID
                                                        </label>

                                                        <div className="input-group">
                                                            <div className="input-group-prepend">
                                                                <div className="input-group-text">
                                                                    <i className="icon-user-regular"/>
                                                                </div>
                                                            </div>

                                                            <input
                                                                type="email"
                                                                className={`form-control ${isError && errors ? errors.email ? 'is-invalid' : '' : ''}`}
                                                                id="email"
                                                                name="email"
                                                                onChange={handleLoginData}
                                                                placeholder="Username or Email"/>

                                                            <div id="email"
                                                                 className={isError && errors ? errors.email ? `invalid-feedback` : 'd-none' : `d-none`}>
                                                                {isError && errors ? errors.email ? errors.email : '' : ''}
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div className="form-group">

                                                        <label
                                                            className="sr-only"
                                                            htmlFor="password">
                                                            Password
                                                        </label>

                                                        <div className="input-group">

                                                            <div className="input-group-prepend">
                                                                <div className="input-group-text">
                                                                    <i className="icon-key"/>
                                                                </div>
                                                            </div>

                                                            <input
                                                                type="password"
                                                                className={`form-control ${isError && errors ? errors.password ? 'is-invalid' : '' : ''}`}
                                                                id="password"
                                                                name="password"
                                                                onChange={handleLoginData}
                                                                placeholder="Password"/>

                                                            <div id="password"
                                                                 className={isError && errors ? errors.password ? `invalid-feedback` : 'd-none' : `d-none`}>
                                                                {isError && errors ? errors.password ? errors.password : '' : ''}
                                                            </div>
                                                        </div>
                                                    </div>

                                                </form>

                                            </div>

                                        </div>

                                        <div
                                            className="tab-pane fade"
                                            id="v-pills-phone-login"
                                            role="tabpanel"
                                            aria-labelledby="v-pills-phone-login-tab">


                                            <div className="login-form">

                                                <form>

                                                    <div className="form-group">

                                                        <label
                                                            className="sr-only"
                                                            htmlFor="phone">
                                                            Phone
                                                        </label>

                                                        <div className="input-group">

                                                            <div className="input-group-prepend">
                                                                <div className="input-group-text">
                                                                    <i className="icon-phone"/>
                                                                </div>
                                                            </div>

                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                id="phone"
                                                                placeholder="Phone"/>
                                                        </div>
                                                    </div>

                                                </form>

                                            </div>

                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>

                        <div className="modal-footer">
                            <button
                                onClick={handleLogin}
                                type="submit"
                                className="btn btn-warning btn-lg btn-block">
                                {loading ? 'Please Wait' : 'Login / SignUp'}
                                <i className={`mb-1 ml-2 ${loading ? 'spinner-border spinner-border-sm' : 'icon-sign-in-alt-solid'}`}/>
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </>
    );
};


export default LoginModal;