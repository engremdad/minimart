import ProductCategory from "./ProductCategory";
import {useEffect, useState} from "react";
import {connect} from "react-redux";
import {SET_MENU_CATEGORIES} from "../../../store/frontend/actions/menuActions";
import axios from "axios";
import Storage from "../../../Helpers/Auth/Storage";
import {useRouter} from "next/router";

const LeftSideBar = (props) => {

    const router = useRouter();


    const api_url = process.env.mode === 'development' ? process.env.api_url_dev : process.env.api_url;

    const [user, setUser] = useState('');

    useEffect(() => {
        if (Storage.isLoggedIn()) {
            setUser(Storage.getUser());
        }

        if (props.menuCategories.length === 0) {
            getAllMenuCategories().then(res => {
                // console.log(res);
            }).catch(err => {
                // console.log(err);
            })
        }
    }, []);

    const getAllMenuCategories = async () => {

        let menuCategoryResponse = await axios.get(`${api_url}/getAllCategoryWithChild/menuShow`)
            .then((result) => {
                return result.data;
            }).catch((exception) => {
                return exception;
            });

        if (menuCategoryResponse.success) {
            props.setMenuCategories(menuCategoryResponse.data);
        }
    }

    const logout = () => {
        Storage.clear();

        if (!Storage.isLoggedIn()) {
            router.reload();
        }
    }


    return (
        <>
            <nav id="sidebar" className={props.toggleChange ? 'shadow-lg active' : 'shadow-lg'}>
                <div className="sidebar-wrap">

                    <div className="client-login-area">
                        <a href="#"><img src="/img/abc.png" alt="User Login"/></a>
                        <div className="client-login-detail">
                            <p className="welcome-text">Good Afternoon</p>
                            <h4 className="cla-name" title="Md. Khaza Khaled Lizar"><a href="#">{user}</a></h4>
                            <p
                                onClick={(event) => {
                                    event.preventDefault();
                                    logout();
                                }}
                                className="cla-log-in-out">
                                <a href="#">Logout</a>
                            </p>
                        </div>
                    </div>

                    <ul className="list-unstyled sale-promo">
                        <span className="section-tag">Spacial Deals</span>
                        <li>
                            <a href="#" className="nav-link">
                                <img src="/img/product-category/best-price.png" alt="Top selling items"/>
                                <span>Best selling items</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" className="nav-link">
                                <img src="/img/product-category/offer.png" alt="Best Offers"/>
                                <span>Best Offers</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" className="nav-link">
                                <img src="/img/product-category/star.png" alt="Top rated items"/>
                                <span>Top rated items</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" className="nav-link">
                                <img src="/img/product-category/gift.png" alt="Top rated items"/>
                                <span>Combo sales</span>
                            </a>
                        </li>
                    </ul>


                    <ProductCategory menuCategories={props.menuCategories}/>

                    <div className="sd-support-policy">
                        <span className="section-tag">Help & Support</span>
                        <div className="sdsp-info">
                            <a href="tel:+8801718659502" className="sdsp-info-box">
                                <img src="/img/help-line.png" alt="Hotline"/>
                                <p>
                                    <span className="sdsp-title">Hotline Number</span>
                                    <span className="sdsp-details">+880 1913 800 800</span>
                                </p>
                            </a>
                            <a href="mailto:info@onushorit.com" className="sdsp-info-box">
                                <img src="/img/email.png" alt="Hotline"/>
                                <p>
                                    <span className="sdsp-title">Email Us</span>
                                    <span className="sdsp-details">support@desktopit.com.bd</span>
                                </p>
                            </a>
                            <a href="#" className="sdsp-info-box">
                                <img src="/img/power1.png" alt="DESKTOP IT"/>
                                <p>
                                    <span className="sdsp-title">Powered By</span>
                                    <span className="sdsp-details">desktopit.com.bd</span>
                                </p>
                            </a>
                        </div>
                        <div className="sdsp-link">
                            <span className="section-tag">Help & Support</span>
                            <p>
                                <a href="#">About us</a>,
                                <a href="#">Contact us</a>,
                                <a href="#">Privacy policy</a>,
                                <a href="#">Purchase policy</a>,
                                <a href="#">Cookie policy</a>,
                                <a href="#">Terms & conditions</a>,
                                <a href="#">Returns policy</a>
                            </p>
                        </div>
                        <div className="sdsp-copyright">
                            <p><i className="far fa-copyright"></i> Deskto Shop 2020</p>
                        </div>
                    </div>

                </div>
            </nav>
        </>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        setMenuCategories: (menu_categories) => dispatch({type: SET_MENU_CATEGORIES, payload: {menu_categories}}),
    }
}

const mapStateToProps = (store) => {

    const {menuCategories} = store.menuStore;

    return {menuCategories};
}


export default connect(mapStateToProps, mapDispatchToProps)(LeftSideBar);