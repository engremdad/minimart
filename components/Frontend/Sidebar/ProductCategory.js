import React from "react";
import MultiCategory from "./MultiCategory";
import Router, {useRouter} from "next/router";

const ProductCategory = (props) => {


    const router = useRouter();
    const cat_slug = router.query.slug;

    const hasSubmenu = 'has-sub-menu';
    const menuCollapsed = ' collapsed';

    const showProductByCategory = (slug) => {
        Router.push({
            pathname: '/category/[slug]',
            as: '/category/[slug]',
            query: {slug: slug},
        });
    }

    const hasChild = (parent) => {
        let flag = false;

        if (parent.child_categories.length > 0) {
            parent.child_categories.map((cat) => {
                if (cat.slug === cat_slug) {
                    flag = true;
                }

                if (cat.child_categories.length > 0) {
                    cat.child_categories.map((child_cat) => {
                        if (child_cat.slug === cat_slug) {
                            flag = true;
                        }
                    })
                }
            })

        }

        return flag;

    }

    return (
        <div className="accordion" id="sidebarMainMenu">
            <span className="section-tag">Product Category</span>
            {
                props.menuCategories.map((parent) => {
                    return (

                        <div
                            className="card" key={parent.id}>
                            <div className={`card-header ${parent.slug === cat_slug ? `menu_active` : ``}`}
                                 id={`singleLevelHeading${parent.id}`}
                                 onClick={() => {
                                     showProductByCategory(parent.slug);
                                 }}>

                                <h2 className="mb-0">

                                    <a
                                        className={`nav-link ${parent.child_categories.length ? cat_slug === parent.slug ? hasSubmenu : hasChild(parent) ? hasSubmenu : hasSubmenu + menuCollapsed : ''}`}
                                        data-toggle={parent.child_categories.length ? `collapse` : ``}
                                        data-target={parent.child_categories.length ? `#singleLevelMenu${parent.id}` : ``}
                                        aria-expanded={cat_slug === parent.slug ? true : hasChild(parent) ? true : false}
                                        aria-controls={parent.child_categories.length ? `singleLevelMenu${parent.id}` : ``}
                                        href={parent.slug}>
                                        <img src={parent.menu_icon} alt={parent.name}/>
                                        <span>{parent.name}</span>
                                    </a>

                                </h2>
                            </div>

                            {
                                parent.child_categories.length ? parent.child_categories.map((children_cat) => {

                                    return (
                                        <div
                                            id={`singleLevelMenu${parent.id}`}
                                            className={cat_slug === parent.slug ? `collapse show` : hasChild(parent) ? `collapse show` : `collapse`}
                                            aria-labelledby={`singleLevelHeading${parent.id}`}
                                            data-parent="#sidebarMainMenu" key={children_cat.id}
                                        >

                                            <div className="card-body">

                                                <div className="accordion sl-accordian"
                                                     id={`multiLevel${children_cat.id}`}>

                                                    <div className="card">


                                                        <div
                                                            className={children_cat.slug === cat_slug ? `menu_active` : ``}
                                                            id={`multiLevelMenu${children_cat.id}`}
                                                            onClick={(e) => {
                                                                e.preventDefault();
                                                                showProductByCategory(children_cat.slug);
                                                            }}>
                                                            <h2 className="mb-0">

                                                                <a
                                                                    href={children_cat.slug}
                                                                    className={`nav-link ${children_cat.child_categories.length ? cat_slug === children_cat.slug ? `has-sub-menu` : hasChild(children_cat) ? `has-sub-menu` : `has-sub-menu collapsed` : ``}`}
                                                                    data-toggle={children_cat.child_categories.length ? `collapse` : ``}
                                                                    data-target={`#multiLevelItem${children_cat.id}`}
                                                                    aria-expanded={cat_slug === children_cat.slug ? true : false}
                                                                    aria-controls={`multiLevelItem${children_cat.id}`}>

                                                                    {children_cat.name}

                                                                </a>
                                                            </h2>
                                                        </div>


                                                        {
                                                            children_cat.child_categories.length ?

                                                                <MultiCategory category={children_cat}/> : ''
                                                        }


                                                    </div>


                                                </div>
                                            </div>


                                        </div>
                                    )

                                }) : ''
                            }


                        </div>

                    )
                })
            }


        </div>

    );
};

export default ProductCategory;