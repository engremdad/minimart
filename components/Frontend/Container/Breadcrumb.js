const Breadcrumb = () =>{
    return (
        <>
            <div className="container-fluid">
                <div className="page-title">
                    <div className="row">
                        <div className="col-lg-6">
                            <h3>Layout Light</h3>
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item"><a href="../ltr/index.html">Home</a></li>
                                <li className="breadcrumb-item">Color Version</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default Breadcrumb;
