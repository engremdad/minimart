import React from 'react';
import ProductQuickView from "../../Modals/ProductQuickView";
import {connect} from "react-redux";
import {SET_MODAL_PRODUCT} from "../../../../store/frontend/actions/productAction";
import SingleProduct from "../Home/SingleProduct";
import axios from "axios";


const ProductList = (props) => {

    const api_url = process.env.mode === 'development' ? process.env.api_url_dev : process.env.api_url;

    const singleProduct = async (slug) => {
        let response = await axios.get(`${api_url}/products/${slug}`).then((result) => {
            console.log(result.data);
            return result.data;
        }).catch((error) => {
            return error;
        });

        if (response.success) {
            props.setModalProduct(response.data);
        }
    }

    return (
        <>
            <section className="product-block">
                <div className="container-fluid">
                    <div className="row text-center">
                        <div className="col-md-12">
                            <h3>Products for category: {props.slug}</h3>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">

                            <div className="product-showcase-wrap">

                                {props.products ? props.products.map(product => (

                                        <SingleProduct product={product} key={product.id} singleProduct={singleProduct}/>
                                    )
                                ) : <h1>No Data Found</h1>}
                            </div>

                        </div>
                    </div>
                </div>
            </section>

            <ProductQuickView product={props.modalProduct}/>
        </>
    );
};

const mapStateToProps = (store) => {
    const {modalProduct} = store.productStore;
    return {modalProduct};
}


const mapDispatchToProps = (dispatch) => {

    return {
        setModalProduct: (modalProduct) => dispatch({type: SET_MODAL_PRODUCT, payload: {modalProduct}})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductList)