import React from 'react';
import {ADD_TO_CART} from "../../../../store/frontend/actions/cartAction";
import {connect} from "react-redux";
import Router from "next/router";

const SingleProduct = (props) => {
    console.log('props',props)
    return (
        <>
            <div className="product-box-grid">
                <div className="product-box">
                    <div className="product-box-wrap">
                        <div className="product-box-image">
                            <img src={props.product.feature_image} alt={props.product.name}/>
                        </div>
                        <div className="product-details">
                            <h6 className="product-title">{props.product.name}</h6>
                            <p className="product-unit">{props.product.weight} {props.product.sale_unit}</p>
                            <p className="product-price">৳ {props.product.selling_price}</p>
                        </div>
                        <div className="pbw-hover">
                            <div className="cart">
                                <a href="#" onClick={event => {
                                    event.preventDefault();
                                    props.addToCart(props.product)
                                }}><i
                                    className="icon-shopping-basket"/> Add to Cart</a>
                            </div>
                            <div className="view-option">

                                <a href={props.product.slug}
                                   data-toggle="modal"
                                   data-target="#product_quick_view_modal"
                                   onClick={(event => {
                                       event.preventDefault();
                                       props.singleProduct(props.product.slug);
                                   })}
                                >
                                    <i className="icon-eye"> </i>Quick View
                                </a>

                            </div>
                        </div>
                    </div>
                    <div className="product-cart">
                        <a href={`/product/${encodeURIComponent(props.product.slug)}`}
                           onClick={(e) => {
                               e.preventDefault();
                               Router.push({
                                   pathname: '/product/[slug]',
                                   query: {slug: props.product.slug},
                               })
                           }}
                        ><i className="icon-eye"/> View</a>
                        <a href="#"
                           onClick={event => {
                               event.preventDefault();
                               props.addToCart(props.product)
                           }}
                        ><i className="icon-shopping-basket"/>Cart</a>
                    </div>
                </div>
            </div>
        </>
    );
};

const mapDispatchToProps = (dispatch) => {

    return {
        addToCart: (product) => dispatch({type: ADD_TO_CART, payload: {product}}),
    }
}

export default connect(null, mapDispatchToProps)(SingleProduct);