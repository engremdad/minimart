import React from 'react';
import Image from 'next/image'
import {connect} from "react-redux";
import Router from "next/router";

const Category = (props) => {

    const showProductByCategory = (slug) => {
        Router.push({
            pathname: '/category/[slug]',
            as: '/category/[slug]',
            query: {slug: slug},
        })
    }

    return (
        <>
            <section className="product-category">
                <div className="container-fluid text-center">
                    <div className="row">
                        <div className="col-md-12">
                            <h3>Find your needs from wide range of product categories</h3>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">

                            {props.homeCategories.map(category =>
                                <div className="product-category-wrap" title={category.name} key={category.id}
                                     onClick={() => showProductByCategory(category.slug)}>

                                    <a href={`/category/${encodeURIComponent(category.slug)}`}>
                                        <Image src={category.image} height={80} width={80} alt={category.name}/>
                                        <h5>{category.name}</h5>
                                    </a>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

const mapStateToProps = (store) => {

    const {homeCategories} = store.menuStore;

    return {homeCategories};
}

export default connect(mapStateToProps, null)(Category);

