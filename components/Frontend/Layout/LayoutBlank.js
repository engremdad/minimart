const LayoutBlank = (props) => {

    return (
        <>
            <div className="wrapper admin-login-form">
                {props.children}
            </div>

        </>
    )
}

export default LayoutBlank;
