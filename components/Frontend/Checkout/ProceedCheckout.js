import React from 'react';
import {connect} from "react-redux";
import {SET_DELIVERY_TYPE} from "../../../store/frontend/actions/deliveryActions";

const ProceedCheckout = (props) => {

    return (
        <>
            <div className="card-header">
                <span className="badge">
                    03
                </span>
                Proceed to Checkout
            </div>

            <div className="card-body">
                <form>
                    <div className="form-group">
                        <div className="row">
                            <div className="col-12">
                                <div className="payment-gs row">
                                    <div className="col-md-6">
                                        <div className="radio-button">

                                            <label className="radio-button__label-wrapper"
                                                   htmlFor="cash_on_delivery">

                                                <h3 className="radio-button__label-title">
                                                    Cash on Delivery
                                                </h3>

                                                <span
                                                    className="radio-button__label-subtext">
                                                       Pay after received your order.
                                                </span>

                                                <input type="radio"
                                                       id="cash_on_delivery"
                                                       className="radio-button__input"
                                                       onChange={() => {
                                                           props.setDeliveryType('cash_on_delivery');
                                                       }}
                                                       checked={props.deliveryStore.delivery_type === 'cash_on_delivery'}/>

                                                <div className="radio-button__custom-indicator"></div>

                                            </label>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="radio-button">

                                            <label
                                                className="radio-button__label-wrapper"
                                                htmlFor="ssl_commerze">

                                                <h3 className="radio-button__label-title">
                                                    SSLCOMMERZ
                                                </h3>

                                                <span
                                                    className="radio-button__label-subtext">
                                                    Cards, Mobile Banking, NET Banking.
                                                </span>

                                                <input type="radio"
                                                       id="ssl_commerze"
                                                       className="radio-button__input"
                                                       onChange={() => {
                                                           props.setDeliveryType('ssl_commerze');
                                                       }}
                                                       checked={props.deliveryStore.delivery_type === 'ssl_commerze'}/>
                                                <div
                                                    className="radio-button__custom-indicator"></div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <div className="alert alert-warning mt-3 rounded-0 text-center"
                     role="alert">
                    <i className="icon-warning mr-3"></i>
                    <span> By clicking or tapping proceed. I agree to theShoply <a href="#">Trems &
                        Conditions</a>.</span>
                </div>

                <p className="text-center delivery-amount">৳ {props.cartStore.shipping_price_by_date + props.cartStore.shipping_price_by_address} Delivery
                    charge included
                </p>
                <div className="checkout-page-action">
                    <button>
                        <span
                            className="cpa-amount">৳ {props.cartStore.grand_total + props.cartStore.shipping_price_by_date + props.cartStore.shipping_price_by_address}</span>
                        <span
                            className="cpa-proceed"
                            onClick={() => {
                                console.log(props.cartStore, props.deliveryStore);
                            }}>Proceed</span>
                    </button>
                </div>
            </div>
        </>
    );
};

const mapStateToProps = store => {

    const {cartStore, deliveryStore} = store;

    return {cartStore, deliveryStore};
}

const mapDispatchToProps = (dispatch) => {
    return {
        setDeliveryType: (delivery_type) => dispatch({type: SET_DELIVERY_TYPE, payload: {delivery_type}})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProceedCheckout);