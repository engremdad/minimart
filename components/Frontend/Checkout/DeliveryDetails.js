import React, {useState} from 'react';
import AddDeliveryAddress from "./AddDeliveryAddress";
import DeliveryAddresses from "./DeliveryAddresses";
import {connect} from "react-redux";
import axios from "axios";
import {ADD_SHIPPING_PRICE_BY_DATE, SET_DELIVERY_DATE} from "../../../store/frontend/actions/cartAction";
import process from "../../../next.config";

const DeliveryDetails = (props) => {

    const api_url = process.env.mode === 'development' ? process.env.api_url_dev : process.env.api_url;

    const [message, setMessage] = useState('');

    // console.log(props.cartStore.delivery_date);

    const addDeliveryCharge = async (e) => {

        setMessage('');

        let deliveryDate = new Date(e.target.value);

        let presentDate = new Date(Date.now());

        let dayCount = Math.ceil((deliveryDate - presentDate) / (1000 * 60 * 60 * 24));

        if (dayCount > 0) {

            let response = await axios.get(`${api_url}/delivery-price/${dayCount}`)
                .then((result) => {
                    return result.data;
                }).catch((exception) => {
                    return exception;
                });

            if (response.success) {
                let shipping_charge = response.data;
                props.addShippingPriceByDate(shipping_charge.charge);
                props.setDeliveryDate(deliveryDate);
            } else {
                setMessage(response.message);
                props.addShippingPriceByDate(0);
            }
        } else {
            setMessage('Delivery time must be greater than 1 day..');

            props.addShippingPriceByDate(0);
        }

    }

    return (
        <>
            <div className="card-header">
                <span className="badge">01</span>
                Delivery Details
            </div>

            <div className="card-body">

                <div className="delivery-time">
                    <form>
                        <div className="form-group">
                            <label>Delivery time</label>
                            <div className="row no-gutters">

                                <div className="col-12">

                                    {/*Delivery Date*/}
                                    <input
                                        type="date"
                                        id="deliveryDate"
                                        onChange={(e) => {
                                            addDeliveryCharge(e);
                                        }}
                                        defaultValue={props.cartStore.delivery_date}
                                        className="form-control rounded-0"/>

                                    <p className="text-danger">{message}</p>

                                </div>

                                {/*<div className="col-6">*/}
                                {/*    <select className="form-control rounded-0">*/}
                                {/*        <option>ASAP</option>*/}
                                {/*        <option>Take Your Time</option>*/}
                                {/*        <option>Others</option>*/}
                                {/*    </select>*/}
                                {/*</div>*/}
                            </div>
                        </div>
                    </form>
                </div>

                <div className="delivery-address">


                    <DeliveryAddresses/>

                    {props.deliveryStore.expandAddDelivery && <AddDeliveryAddress/>}

                </div>

            </div>
        </>
    );
};

const mapDispatchToProps = (dispatch) => {
    return {
        addShippingPriceByDate: (shipping_price) => dispatch({
            type: ADD_SHIPPING_PRICE_BY_DATE,
            payload: {shipping_price}
        }),

        setDeliveryDate: (delivery_date) => dispatch({
            type: SET_DELIVERY_DATE,
            payload: {delivery_date}
        })
    }
}

const mapStateToProps = (store) => {
    const {deliveryStore, cartStore} = store;


    return {deliveryStore, cartStore};
}

export default connect(mapStateToProps, mapDispatchToProps)(DeliveryDetails);