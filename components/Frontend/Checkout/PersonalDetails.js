import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";
import {SET_AUTH_USER} from "../../../store/frontend/actions/deliveryActions";

const PersonalDetails = (props) => {

    const [userData, setUserData] = useState({
        first_name: '',
        last_name: '',
        email: '',
        phone: ''
    });

    useEffect(() => {

        setUserData({
            first_name: props.authUser.first_name,
            last_name: props.authUser.last_name,
            email: props.authUser.email,
            phone: props.authUser.phone
        });

    }, [props.authUser])


    let handleChange = (e) => {
        e.preventDefault();

        let name = e.target.name;
        let value = e.target.value;

        let user = {...userData, [name]: value};

        props.setAuthUser(user);

        setUserData({...userData, user});
    }

    // console.log(userData);

    return (
        <>
            <div className="card-header">
                <span className="badge">02</span>
                Personal Details
            </div>

            <div className="card-body">
                <form>
                    <div className="form-group">
                        <label>Name</label>

                        <div className="row no-gutters">

                            <div className="col-6">

                                <input
                                    name={`first_name`}
                                    type="text"
                                    defaultValue={userData.first_name}
                                    onChange={handleChange}
                                    className="form-control rounded-0"
                                    placeholder="First Name"/>
                            </div>

                            <div className="col-6">

                                <input
                                    name={`last_name`}
                                    onChange={handleChange}
                                    type="text"
                                    defaultValue={userData.last_name}
                                    className="form-control rounded-0"
                                    placeholder="Last Name"/>
                            </div>

                        </div>
                    </div>
                    <div className="form-group">
                        <label>Email Address</label>

                        <input
                            name={'email'}
                            type="email"
                            defaultValue={userData.email}
                            placeholder="e.g. email@gmail.com"
                            className="form-control rounded-0" readOnly/>
                    </div>

                    <div className="form-group">

                        <label className="col-12">Mobile Number</label>

                        <div className="input-group mb-3">

                            <div className="input-group-prepend rounded-0">

                                <span
                                    className="input-group-text rounded-0"
                                    id="basic-addon1">
                                    +880
                                </span>

                            </div>

                            <input
                                name={'phone'}
                                type="text"
                                defaultValue={userData.phone}
                                onChange={handleChange}
                                className="form-control rounded-0"
                                placeholder="e.g. 1913800877"/>
                        </div>
                    </div>
                </form>
            </div>
        </>
    );
};

const mapStateToProps = store => {
    const {authUser} = store.deliveryStore;
    return {authUser};
}

const mapDispatchToProps = (dispatch) => {
    return {
        // changeAuthUser: (userData) => dispatch({type: CHANGE_AUTH_USER, payload: {userData}}),
        setAuthUser: (userData) => dispatch({type: SET_AUTH_USER, payload: {userData}}),
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(PersonalDetails);