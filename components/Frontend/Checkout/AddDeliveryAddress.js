import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";
import {
    ADD_CUSTOMER_SHIPPING_ADDRESS,
    EDIT_CUSTOMER_SHIPPING_ADDRESS,
    SET_AREAS,
    UPDATE_CUSTOMER_SHIPPING_ADDRESS
} from "../../../store/frontend/actions/deliveryActions";
import axios from "axios";
import process from "../../../next.config";

const AddDeliveryAddress = (props) => {

    const api_url = process.env.mode === 'development' ? process.env.api_url_dev : process.env.api_url;

    const [errors, setErrors] = useState({});

    const [addressFormData, setAddressFormData] = useState({
        order_id: '',
        shop_id: 1,
        name: '',
        phone: '',
        location_id: '',
        area_id: '',
        address: '',
    });

    useEffect(() => {
        setAddressFormData({
                order_id: props.editableShippingAddress.order_id ?? '',
                shop_id: props.editableShippingAddress.shop_id ?? 1,
                name: props.editableShippingAddress.name ?? '',
                phone: props.editableShippingAddress.phone ?? '',
                location_id: props.editableShippingAddress.location_id ?? '',
                area_id: props.editableShippingAddress.area_id ?? '',
                address: props.editableShippingAddress.address ?? '',
            }
        )
    }, [props.editableShippingAddress])

    const handleChange = (e) => {
        e.preventDefault();

        let name = e.target.name;
        let value = e.target.value;

        if (name === 'location_id') {

            let areas = props.allAreas.filter(area => {
                return area.location_id == value;
            });

            props.setAreas(areas);
        }

        setAddressFormData({
            ...addressFormData,
            [name]: value
        });

        setErrors({
            ...errors,
            [name]: ''
        });

    }

    const handleSubmit = async (e, type) => {
        e.preventDefault();

        if (type === 'add') {

            let response = await axios.post(`${api_url}/customer-addresses`, addressFormData)
                .then((result) => {
                    return result.data;
                }).catch((error) => {
                    return error;
                });

            if (response.success) {
                let address = response.data;
                props.addCustomerShippingAddress(address);
                resetFormData();
            } else {
                setErrors(response.errors);
            }
        }

        if (type === 'update') {

            let response = await axios.put(`${api_url}/customer-addresses/${props.editableShippingAddress.id}`, addressFormData)
                .then((result) => {
                    return result.data;
                }).catch((error) => {
                    return error;
                });

            if (response.success) {

                let address = response.data;

                // For reset Customer Shipping Address
                props.editCustomerShippingAddress();

                // After edit a set the updated customer shipping address
                props.updateCustomerShippingAddress(address);
                resetFormData();
            } else {
                setErrors(response.errors);
            }
        }
    }

    const validate = () => {
        let errors = {};
        if (isEmpty(addressFormData.name)) {
            errors.name = "The name field is required.";
        }
        if (isEmpty(addressFormData.phone)) {
            errors.phone = "Phone number is required.";
        }

        if (isEmpty(addressFormData.location_id)) {
            errors.location_id = "The location field is required.";
        }
        if (isEmpty(addressFormData.area_id)) {
            errors.area_id = "The area field is required.";
        }
        if (isEmpty(addressFormData.address)) {
            errors.address = "The address field is required."
        }
        setErrors(errors);


        return (Object.keys(errors).length === 0
            && errors.constructor === Object) ? {
            success: true,
            message: "",
            errors: ""
        } : {
            success: false,
            message: "Please fix the following issues",
            errors: errors
        };
    }

    const isEmpty = (value) => value === null || value === '';

    const resetFormData = () => {
        setAddressFormData({
            order_id: '',
            shop_id: 1,
            name: '',
            phone: '',
            location_id: '',
            area_id: '',
            address: '',
        })
    }


    return (
        <>
            <form className="delevery-address-form">

                <div className="form-group">

                    <label>Name <span className="text-danger">*</span></label>

                    <input
                        name={'name'}
                        onChange={handleChange}
                        value={addressFormData.name}
                        type="text"
                        className={`form-control rounded-0 ${errors.name && `is-invalid`}`}
                        placeholder="Name"/>

                    <div id="name"
                         className={errors.name && `invalid-feedback`}>
                        {errors && errors.name}
                    </div>
                </div>

                <div className="form-group">

                    <label className="form-group">Mobile Number <span className="text-danger">*</span></label>

                    <div className="input-group mb-3">

                        <div className="input-group-prepend rounded-0">

                                <span
                                    className="input-group-text rounded-0"
                                    id="basic-addon1">
                                    +880
                                </span>

                        </div>

                        <input
                            name={'phone'}
                            value={addressFormData.phone}
                            onChange={handleChange}
                            type="text"
                            className={`form-control rounded-0 ${errors.phone && `is-invalid`}`}
                            placeholder="e.g. 1913800877"/>

                        <div id="phone"
                             className={errors.phone && `invalid-feedback`}>
                            {errors && errors.phone}
                        </div>

                    </div>
                </div>


                <div className="form-group">

                    <label>Select Location <span className="text-danger">*</span></label>

                    <select
                        name={'location_id'}
                        value={addressFormData.location_id}
                        onChange={handleChange}
                        className={`form-control rounded-0 ${errors.location_id && `is-invalid`}`}>

                        <option value="">Select Location....</option>

                        {
                            props.locations && props.locations.map((location, index) => (
                                <option value={location.id} key={index}>{location.name}</option>
                            ))
                        }

                    </select>

                    <div id="location_id"
                         className={errors.location_id && `invalid-feedback`}>
                        {errors && errors.location_id}
                    </div>

                </div>

                <div className="form-group">

                    <label>Select Area <span className="text-danger">*</span></label>

                    <select
                        name={'area_id'}
                        value={addressFormData.area_id}
                        onChange={handleChange}
                        className={`form-control rounded-0 ${errors.area_id && `is-invalid`}`}>

                        <option value="">Select Area</option>

                        {
                            props.areas && props.areas.map((area, index) => (
                                <option value={area.id} key={index}>{area.name}</option>
                            ))
                        }

                    </select>

                    <div id="area_id"
                         className={errors.area_id && `invalid-feedback`}>
                        {errors && errors.area_id}
                    </div>
                </div>

                <div className="form-group">
                    <label>Address Details <span className="text-danger">*</span></label>

                    <textarea
                        name={'address'}
                        className={`form-control rounded-0 ${errors.address && `is-invalid`}`}
                        onChange={handleChange}
                        value={addressFormData.address}
                        rows="2"></textarea>

                    <div id="address"
                         className={errors.address && `invalid-feedback`}>
                        {errors && errors.address}
                    </div>
                </div>

                <div className="form-group d-flex justify-content-end text-center">

                    <button
                        onClick={(event) => {
                            event.preventDefault()
                            validate().success ?
                                handleSubmit(event, props.editableShippingAddress ? 'update' : 'add') : ''
                        }}
                        className="btn btn-add-update">
                        {props.editableShippingAddress ? `Update` : `Add`}
                    </button>

                </div>

            </form>
        </>
    );
};

const mapDispatchToProps = (dispatch) => {
    return {
        setAreas: (areas) => dispatch({
            type: SET_AREAS,
            payload: {areas}
        }),

        addCustomerShippingAddress: (address) => dispatch({
            type: ADD_CUSTOMER_SHIPPING_ADDRESS,
            payload: {address}
        }),

        editCustomerShippingAddress: (address) => dispatch({
            type: EDIT_CUSTOMER_SHIPPING_ADDRESS,
            payload: {address}
        }),

        updateCustomerShippingAddress: (address) => dispatch({
            type: UPDATE_CUSTOMER_SHIPPING_ADDRESS,
            payload: {address}
        })
    }
}

const mapStateToProps = store => {
    const {editableShippingAddress, locations, areas, allAreas} = store.deliveryStore;

    return {editableShippingAddress, locations, areas, allAreas};
}

export default connect(mapStateToProps, mapDispatchToProps)(AddDeliveryAddress);