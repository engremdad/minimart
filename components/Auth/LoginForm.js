import AdminHeader from "../Admin/Header/AdminHeader";
import {useEffect, useState} from 'react';
import axios from 'axios';
import Router from "next/router";
import login from './login.module.css';
import Storage from "../../Helpers/Auth/Storage";

const LoginForm = () => {

    const [isError, setIsError] = useState(false);
    const [loading, setLoading] = useState(false);
    const [message, setMessage] = useState('');
    const [errors, setErrors] = useState([]);

    const [loginData, setLoginData] = useState({
        email: '',
        password: ''
    });

    const handleLoginData = (event) => {
        setIsError(false);
        setErrors([]);
        setMessage('');
        setLoading(false);

        const name = event.target.name;
        const value = event.target.value;
        setLoginData({...loginData, [name]: value});
    }

    const handleLogin = async (event) => {
        event.preventDefault();

        setLoading(true);

        let response = await axios.post(`/auth/login`, loginData).then((result) => {
            return result.data;
        }).catch((exception) => {
            return exception;
        });

        // Success Login
        if (response.success) {
            setLoading(false);
            setIsError(false);
            setMessage(response.data.message);

            //Save authentication to cookies
            Storage.store(JSON.stringify(response.data));

            if (Storage.isLoggedIn()) {
                await Router.push('/admin/dashboard');
            }
        }
        // Login Failed
        else {
            setErrors(response.errors);
            setIsError(true);
            setLoading(false);
            setMessage(response.message);
            console.log(response.errors)
        }
    }


    useEffect(() => {
        if (Storage.isLoggedIn()) {
            Router.push('/admin/dashboard');
        }
    });


    return (
        <>
            <AdminHeader>Login | theShoply</AdminHeader>
            <div className="login-logo">
                <div className="login-logo-box">
                    <a href="index.html">
                        <img src="/img/logo.png" alt="theThemely Logo"/>
                    </a>
                    <h1>Admin Login</h1>
                </div>
            </div>
            <div className="login-form">
                <form>

                    <div className="form-group">
                        <label className="sr-only" htmlFor="email">User ID</label>
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <div className="input-group-text"><i className="far fa-user"/></div>
                            </div>

                            <input type="email"
                                   className={`form-control ${isError && errors ? errors.email ? 'is-invalid' : '' : ''}`}
                                   id="email"
                                   name="email"
                                   onChange={handleLoginData}
                                   placeholder="Username or Email"/>

                            <div id="email"
                                 className={isError && errors ? errors.email ? `invalid-feedback ${login.error}` : 'd-none' : `d-none`}>
                                {isError && errors ? errors.email ? errors.email : '' : ''}
                            </div>

                        </div>
                    </div>

                    <div className="form-group">
                        <label className="sr-only" htmlFor="password">Password</label>
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <div className="input-group-text"><i className="fas fa-key"/></div>
                            </div>

                            <input
                                type="password"
                                id="password"
                                className={`form-control ${isError && errors ? errors.password ? 'is-invalid' : '' : ''}`}
                                name="password"
                                onChange={handleLoginData}
                                placeholder="Password"/>
                            <div id="password"
                                 className={isError && errors ? errors.password ? `invalid-feedback ${login.error}` : 'd-none' : `d-none`}>
                                {isError && errors ? errors.password ? errors.password : '' : ''}
                            </div>
                        </div>
                    </div>

                    <span className={isError ? message ? login.error : login.success : `d-none`}>{message}</span>

                    <button
                        onClick={handleLogin}
                        type="submit"
                        className="btn">
                        {loading ? 'Please Wait' : 'Login'}
                        <i className={loading ? `spinner-border spinner-border-sm` : 'icon-sign-in-alt-solid'}/>
                    </button>

                    {/*<p className="forgot-password-text">Forgot Password? <a href="#">Click Here</a></p>*/}

                </form>

            </div>
        </>
    )
}

export default LoginForm;