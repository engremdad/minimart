import React from 'react';
import {connect} from "react-redux";
import {
    ADD_PURCHASE_PAYMENT,
    DELETE_PURCHASE,
    SET_PAYMENT_AMOUNT,
    SET_PAYMENT_DATE,
    SET_PAYMENT_REFERENCE_NO,
    SET_PURCHASE_PAYMENT_DOCUMENT,
    SET_PURCHASE_PAYMENT_METHOD,
    SET_PURCHASE_PAYMENT_NOTE
} from "../../../../store/admin/actions/purchase/purchaseListAction";
import axios from "axios";
import process from "../../../../next.config";
import {useRouter} from "next/router";

const PurchaseDetails = (props) => {
    const router = useRouter();

    const api_url = process.env.mode === 'development' ? process.env.api_url_dev : process.env.api_backend_url;

    const addingPayment = async () => {
        let response = await axios.post(`${api_url}/purchase-payment-histories`, props.purchase_store).then(result => {
            return result.data;
        }).catch(err => {
            return err;
        });

        if (response.success) {
            props.addPurchasePayment();
            router.reload();
            $('#add_payment').modal('hide');
        }
    }
    const deletingPurchase = async (purchase_id) => {
        props.deletePurchase(purchase_id);
        const delete_url = `${process.env.api_backend_url}/purchases/${purchase_id}`;
        const deleteForm = new FormData();
        deleteForm.append("_method", "DELETE");
        let response = await axios.post(delete_url, deleteForm).then(result => {
            return result.data;
        }).catch(err => {
            return err;
        });
        if (response.success) {
            props.deletePurchase(purchase_id);
        }
    }

    return (
        <>

            <div className="custom-card-box mb-3">
                <div className="custom-card-box-header border-bottom-0">
                    <div className="row">
                        <div className="col-lg-12">
                            <ul className="nav global-nav">
                                <li className="nav-item">
                                    <a className="nav-link " data-toggle="modal"
                                       data-target="#add_payment" href="#">
                                        <i className="icon-plus-square-regular mr-1"/>
                                        <span>Add Payment</span>
                                    </a>
                                </li>
                                {/*<li className="nav-item">*/}
                                {/*    <a className="nav-link " href="#">*/}
                                {/*        <i className="icon-edit-solid mr-1"/>*/}
                                {/*        <span>Edit Purchase</span>*/}
                                {/*    </a>*/}
                                {/*</li>*/}

                                {/*<li className="nav-item dropdown">*/}
                                {/*    <a className="nav-link dropdown-toggle " href="#"*/}
                                {/*       id="navbarDropdown"*/}
                                {/*       role="button" data-toggle="dropdown" aria-haspopup="true"*/}
                                {/*       aria-expanded="false">*/}
                                {/*        <i className="icon-menu3 mr-1"/>*/}
                                {/*        <span>Actions</span>*/}
                                {/*    </a>*/}
                                {/*    <div className="dropdown-menu dropdown-menu-right shadow"*/}
                                {/*         aria-labelledby="navbarDropdown">*/}
                                {/*        <a className="dropdown-item " href="#"> Download as PDF</a>*/}
                                {/*        <div className="dropdown-divider"/>*/}
                                {/*        <a className="dropdown-item " href="#"> Email Purchase</a>*/}
                                {/*        <div className="dropdown-divider"/>*/}
                                {/*        <a className="dropdown-item " href="#">Print Barcodes</a>*/}
                                {/*        <div className="dropdown-divider"/>*/}
                                {/*        <a className="dropdown-item" href="#"> Return Purchase</a>*/}
                                {/*        <div className="dropdown-divider"/>*/}
                                {/*        <a className="dropdown-item" href="#"*/}
                                {/*           onClick={(event) => {*/}
                                {/*               event.preventDefault();*/}
                                {/*               deletingPurchase(props.purchase_store.purchase.id);*/}
                                {/*           }}*/}
                                {/*        >*/}
                                {/*            Delete Purchase</a>*/}
                                {/*    </div>*/}
                                {/*</li>*/}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div className="custom-card-box mb-3">
                <div className="custom-card-box-header">
                    <div className="fbh-title ">
                        <i className="icon-eye-regular"/>
                        <h2>Purchase Details <span>#{props.purchase_store.purchase.reference_no}</span></h2>
                    </div>
                </div>
                <div className="custom-card-box-content p-md-3 p-2">

                    <div className="row">
                        <div className="col-lg-12">
                            <div className="table-responsive custom-table custom-hight-320">
                                <table className="table table-bordered table-hover mb-0 table-order-details">
                                    <thead className="thead-light">
                                    <tr>
                                        <th scope="col">SL</th>
                                        <th scope="col">Img</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Qty.</th>
                                        <th scope="col">Unit Cost.</th>
                                        <th scope="col">Subtotal</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        ((props.purchase_store.purchase) && (props.purchase_store.purchase.product_purchases) && (props.purchase_store.purchase.product_purchases.length > 0)) ? (props.purchase_store.purchase.product_purchases.map((item, index) =>
                                            (
                                                <tr key={item.id} className="text-center">
                                                    <td scope="row">
                                                        {++index}
                                                    </td>

                                                    <td className="text-center">
                                                        <img src={item.product.feature_image} alt="Product Img"/>
                                                    </td>
                                                    <td className="text-center">
                                                        <h6 className="text-875 mb-1">{item.product.name}</h6>
                                                    </td>
                                                    <td className="text-center text-nowrap">
                                                        {item.purchase_quantity}
                                                    </td>
                                                    <td className="text-center text-nowrap">
                                                        ৳ {item.net_unit_cost}
                                                    </td>
                                                    <td className="text-center text-nowrap">
                                                        ৳ {item.sub_total}
                                                    </td>
                                                </tr>
                                            )
                                        )) : (
                                            <tr>
                                                <td colSpan="6" className="text-center">No Purchase Details Found</td>
                                            </tr>
                                        )
                                    }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="modal fade" id="add_payment" tabIndex="-1" aria-labelledby="add_paymentLebel"
                 aria-hidden="true">

                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content rounded-0 border-0">
                        <div className="modal-header">
                            <h5 className="modal-title" id="reportOrderLebel">Add Payment</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <div className="row">
                                <div className="col-xl-6 col-lg-6 col-md-6">
                                    <div className="form-group">

                                        <label>Payment Date *</label>

                                        <input type="date"
                                               className="form-control"
                                               value={props.purchase_store.payment_date}
                                               onChange={(event) => {
                                                   event.preventDefault();
                                                   props.setPaymentDate(event.target.value);
                                               }}
                                               required/>
                                    </div>
                                </div>
                                <div className="col-xl-6 col-lg-6 col-md-6">
                                    <div className="form-group">
                                        <label>Reference No</label>
                                        <input type="text"
                                               className="form-control"
                                               value={props.purchase_store.reference_no ? props.purchase_store.reference_no : ''}
                                               onChange={(event) => {
                                                   event.preventDefault();
                                                   props.setReferenceNo(event.target.value);
                                               }}
                                               placeholder="Reference No"
                                               required/>
                                    </div>
                                </div>
                                <div className="col-xl-6 col-lg-6 col-md-6">
                                    <div className="form-group">

                                        <label>Amount *</label>

                                        <input type="number"
                                               className="form-control"
                                               placeholder={props.purchase_store.purchase.grand_total - props.purchase_store.purchase.paid_amount}
                                               value={props.purchase_store.amount ? props.purchase_store.amount : ''}
                                               onChange={(event) => {
                                                   event.preventDefault();
                                                   props.setPaymentAmount(event.target.value);
                                               }}
                                               required/>
                                    </div>
                                </div>
                                <div className="col-xl-6 col-lg-6 col-md-6">
                                    <label htmlFor="payment_method">Payment Method *</label>
                                    <select className="form-control" id="payment_method"
                                            defaultValue={props.purchase_store.payment_method ?? ""}
                                            onChange={(event) => {
                                                event.preventDefault();
                                                props.setPurchasePaymentMethod(event.target.value);
                                            }}>
                                        <option value={'Cash'}>Cash</option>
                                        <option value={'Check'}>Check</option>
                                        <option value={'Bkash'}>Bkash</option>
                                        <option value={'Rocket'}>Rocket</option>
                                        <option value={'Nagad'}>Nagad</option>
                                        <option value={'Credit Card'}>Credit Card</option>
                                    </select>
                                </div>
                                <div className="col-xl-12 col-lg-12 col-md-12">
                                    <div className="form-group">
                                        <label>Attach Document</label>
                                        <div className="custom-file">
                                            <input type="file"
                                                   className="custom-file-input"
                                                   name="image"
                                                   value={props.purchase_store.document ? props.purchase_store.document : ''}
                                                   onChange={(event) => {
                                                       event.preventDefault();
                                                       props.setPurchasePaymentDocument(event.target.files[0]);
                                                   }}
                                                   id="customFile"/>
                                            <label className="custom-file-label"
                                                   htmlFor="customFile">Choose file</label>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="exampleFormControlTextarea1">Payment Note</label>
                                        <textarea className="form-control" id="exampleFormControlTextarea1"
                                                  rows="3"
                                                  value={props.purchase_store.note ? props.purchase_store.note : ''}
                                                  onChange={(event) => {
                                                      event.preventDefault();
                                                      props.setPaymentNote(event.target.value);
                                                  }}
                                        />
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary rounded-0" data-dismiss="modal">Cancel
                            </button>
                            <button type="button" className="btn btn-primary rounded-0"
                                    onClick={() => addingPayment()}
                            >Add Payment
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        </>
    );
};

const mapDispatchToProps = (dispatch) => {

    return {
        setPaymentDate: (payment_date) => dispatch({type: SET_PAYMENT_DATE, payload: {payment_date}}),
        setReferenceNo: (payment_reference_no) => dispatch({
            type: SET_PAYMENT_REFERENCE_NO,
            payload: {payment_reference_no}
        }),
        setPaymentAmount: (payment_amount) => dispatch({type: SET_PAYMENT_AMOUNT, payload: {payment_amount}}),
        setPurchasePaymentMethod: (purchase_payment_method) => dispatch({
            type: SET_PURCHASE_PAYMENT_METHOD,
            payload: {purchase_payment_method}
        }),
        setPurchasePaymentDocument: (payment_document) => dispatch({
            type: SET_PURCHASE_PAYMENT_DOCUMENT,
            payload: {payment_document}
        }),
        setPaymentNote: (payment_note) => dispatch({
            type: SET_PURCHASE_PAYMENT_NOTE,
            payload: {payment_note}
        }),
        addPurchasePayment: () => dispatch({
            type: ADD_PURCHASE_PAYMENT,
            payload: ''
        }),
        deletePurchase: (purchase_id) => dispatch({
            type: DELETE_PURCHASE,
            payload: {purchase_id}
        }),


    }
}

const mapStateToProps = (store) => {
    let purchase_store = store.purchaseListStore;
    return {purchase_store}

}


export default connect(mapStateToProps, mapDispatchToProps)(PurchaseDetails);
