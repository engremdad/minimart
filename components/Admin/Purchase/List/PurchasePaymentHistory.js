import React from 'react';
import { connect } from "react-redux";

const PurchasePaymentHistory = (props) => {
    return (
        <>
            <div className="custom-card-box mb-3">
                <div className="custom-card-box-header">
                    <div className="fbh-title">
                        <i className="icon-table" />
                        <h2>Payment History</h2>
                    </div>
                </div>
                <div className="custom-card-box-content p-md-3 p-2">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="table-responsive custom-table">
                                <table className="table table-bordered table-hover mb-0 table-order-details">
                                    <thead className="thead-light">
                                        <tr>
                                            <th scope="col" className="text-left">SL</th>
                                            <th scope="col">Payment Method</th>
                                            <th scope="col">Reference No</th>
                                            <th scope="col">Amount</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Date & Time</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            ((props.purchase.purchasePaymentHistory) && (props.purchase.purchasePaymentHistory.length > 0)) ? (props.purchase.purchasePaymentHistory.map((item, index) =>
                                            (
                                                <tr key={index} >
                                                    <td scope="row" className="text-left">
                                                        {++index}
                                                    </td>
                                                    <td scope="row" className="text-left">
                                                        {item.payment_method}
                                                    </td>
                                                    <td scope="row" className="text-left">
                                                        {item.reference_no}
                                                    </td>
                                                    <td scope="row" className="text-left">
                                                        {item.amount}
                                                    </td>
                                                    <td className="text-center">
                                                        {item.status}

                                                    </td>
                                                    <td className="text-center">
                                                        {item.payment_date}

                                                    </td>
                                                </tr>
                                            )
                                            )) : (
                                                    <tr>
                                                        <td colSpan="6" className="text-center">No Payment History Found</td>
                                                    </tr>
                                                )
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </>
    );
};

const mapDispatchToProps = (dispatch) => {

    return {
        // setPurchaseList: (purchaseList) => dispatch(
        //     {type: SET_PURCHASE_LIST, payload: {purchaseList}}),
        // setPurchase: (purchase) => dispatch({type: SET_PURCHASE, payload: {purchase}})
    }
}

const mapStateToProps = (store) => {
    const { purchase } = store.purchaseListStore;
    return { purchase };
}


export default connect(mapStateToProps, mapDispatchToProps)(PurchasePaymentHistory);