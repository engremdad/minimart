import React, { useEffect, useState } from 'react';
import { connect } from "react-redux";
import axios from "axios";
import { SET_PURCHASE, SET_PURCHASE_LIST } from "../../../../store/admin/actions/purchase/purchaseListAction";
import ReactPaginate from "react-paginate";
import ContentLoader, { Facebook } from 'react-content-loader';

const PurchaseList = (props) => {
    const [show, setShow] = useState(10);
    const [keywords, setKeywords] = useState('');
    const [meta, setMeta] = useState('');
    const [page, setPage] = useState(0);

    useEffect(() => {
        let mounted = true;
        fetchPurchaseList().then((response) => {
            if (mounted) {
                props.setPurchaseList(response.data);
                setMeta(response.meta);
            }
        }).finally(() => {
            mounted = false;
        })
    }, [show, keywords, page]);

    const fetchPurchaseList = async () => {

        let url = `backend/purchases?page=${page}&show=${show}&keywords=${keywords}`;

        return await axios.get(url)
            .then((result) => result.data.data).catch((error) => error);
    }

    return (
        <>
            <div className="col-md-7 mb-3">
                <div className="custom-card-box">
                    <div className="custom-card-box-header">
                        <div className="fbh-title">
                            <i className="icon-list2" />
                            <h2>Purchase List</h2>
                        </div>
                    </div>
                    <div className="custom-card-box-content p-md-3 p-2">
                        <div className="row table-top-option mb-md-3 mb-2">
                            <div className="col-6">
                                <div className="showing-table-row">
                                    <label className="">Show</label>
                                    <select
                                        className="custom-select"
                                        defaultValue={`10`}
                                        onChange={(e) => {
                                            setMeta('');
                                            setShow(e.target.value);
                                            setPage(1);
                                        }}>
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                            </div>
                            <div className="col-6">
                                <div className="table-search">
                                    <label className="">Search</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        placeholder="Search here ..."
                                        onChange={(e) => {
                                            setTimeout(() => {
                                                setMeta('');
                                                setKeywords(e.target.value);
                                                setPage(1);
                                            }, 500);
                                        }}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="table-responsive custom-table custom-hight-500">
                                    <table className="table table-bordered table-hover mb-0 carsour-pointer">
                                        <thead className="thead-light">
                                            <tr>
                                                <th scope="col">SL</th>
                                                <th scope="col">Date</th>
                                                <th scope="col">Reference No</th>
                                                <th scope="col">Supplier</th>
                                                <th scope="col">Purchase Status</th>
                                                <th scope="col">Grand Total</th>
                                                <th scope="col">Paid</th>
                                                <th scope="col">Balance</th>
                                                <th scope="col">Payment Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                props.purchaseList.length > 0 ? (props.purchaseList.map((item, index) =>
                                                (
                                                    <tr
                                                        key={item.id}
                                                        onClick={(e) => {
                                                            e.preventDefault();
                                                            props.setPurchase(item);
                                                        }}
                                                        className={`text-center ${(props.purchase ? props.purchase.id === item.id : '') ? 'bg-warning' : ''}`}>

                                                        <td className="nowrap text-center ">{meta.from ? index + meta.from : ''}</td>

                                                        <td className="nowrap text-center">
                                                            {item.purchase_date}
                                                        </td>
                                                        <td className="nowrap text-center">
                                                            {item.reference_no}
                                                        </td>

                                                        <td className="nowrap text-center">
                                                            {item.supplier}
                                                        </td>

                                                        <td className="nowrap text-center">
                                                            {item.status}
                                                        </td>

                                                        <td className="text-center text-nowrap">
                                                            ৳ {item.grand_total}
                                                        </td>

                                                        <td className="text-center">
                                                            {item.paid_amount}
                                                        </td>

                                                        <td className="text-center">
                                                            ৳ {item.grand_total - item.paid_amount}
                                                        </td>

                                                        <td className="text-center">
                                                            {item.payment_status}
                                                        </td>

                                                    </tr>
                                                )
                                                )) : (
                                                    <tr>
                                                        <td colSpan={7}>
                                                            <ContentLoader
                                                                speed={5}
                                                                width={909}
                                                                height={500}
                                                                viewBox="0 0 400 150"
                                                                backgroundColor="#f3f3f3"
                                                                foregroundColor="#ecebeb"
                                                                {...props}
                                                            >
                                                                <circle cx="10" cy="20" r="8" />
                                                                <rect x="25" y="15" rx="5" ry="5" width="300" height="10" />
                                                                <circle cx="10" cy="50" r="8" />
                                                                <rect x="25" y="45" rx="5" ry="5" width="300" height="10" />
                                                                <circle cx="10" cy="80" r="8" />
                                                                <rect x="25" y="75" rx="5" ry="5" width="300" height="10" />
                                                                <circle cx="10" cy="110" r="8" />
                                                                <rect x="25" y="105" rx="5" ry="5" width="300" height="10" />
                                                                <circle cx="10" cy="140" r="8" />
                                                                <rect x="25" y="135" rx="5" ry="5" width="300" height="10" />
                                                                <circle cx="10" cy="170" r="8" />
                                                                <rect x="25" y="165" rx="5" ry="5" width="300" height="10" />
                                                                <circle cx="10" cy="200" r="8" />
                                                                <rect x="25" y="195" rx="5" ry="5" width="300" height="10" />
                                                            </ContentLoader>
                                                        </td>
                                                    </tr>

                                                    )
                                            }

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>

                    {meta && (
                        <nav
                            aria-label="Page navigation example"
                            className="d-flex justify-content-center">
                            <ReactPaginate
                                previousLabel={"Previous"}
                                nextLabel={"Next"}
                                breakLabel={"..."}
                                pageCount={meta.last_page}
                                marginPagesDisplayed={2}
                                pageRangeDisplayed={2}
                                onPageChange={(pageNumber) => {
                                    setPage((pageNumber.selected + 1))
                                }}
                                containerClassName={"pagination"}
                                pageClassName={"page-item"}
                                pageLinkClassName={"page-link"}
                                activeClassName={"active"}
                                previousClassName={"page-item"}
                                previousLinkClassName={"page-link"}
                                nextClassName={"page-item"}
                                nextLinkClassName={"page-link"}
                                breakClassName={"page-item"}
                                breakLinkClassName={"page-link"} />
                        </nav>
                    )}

                </div>
            </div>
        </>
    );
};

const mapDispatchToProps = (dispatch) => {

    return {
        setPurchaseList: (purchaseList) => dispatch(
            { type: SET_PURCHASE_LIST, payload: { purchaseList } }),
        setPurchase: (purchase) => dispatch({ type: SET_PURCHASE, payload: { purchase } })
    }
}

const mapStateToProps = (store) => {
    const { purchaseList, purchase } = store.purchaseListStore;
    return { purchaseList, purchase };
}


export default connect(mapStateToProps, mapDispatchToProps)(PurchaseList);
