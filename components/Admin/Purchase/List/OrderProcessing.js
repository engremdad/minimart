import React from 'react';

const OrderProcessing = (props) => {
    return (
        <>

            <div className="custom-card-box mb-3">
                <div className="custom-card-box-header">
                    <div className="fbh-title text-white bg-dark">
                        <i className="icon-table" />
                        <h2>Order Processing History</h2>
                    </div>
                </div>
                <div className="custom-card-box-content p-md-3 p-2">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="table-responsive custom-table">
                                <table className="table table-bordered table-hover mb-0 table-order-details">
                                    <thead className="thead-light">
                                        <tr>
                                            <th scope="col" className="text-left">Status</th>
                                            <th scope="col">Date & Time</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        ((props.OrderInfo.saleHistory) && (props.OrderInfo.saleHistory.length > 0)) ? (props.OrderInfo.saleHistory.map((item, index) =>
                                            (
                                                <tr>
                                                    <td scope="row" className="text-left">
                                                        {item.sale_history_status}
                                                    </td>

                                                    <td className="text-center">
                                                        {item.time}

                                                    </td>
                                                </tr>
                                            )
                                        )) : (
                                            <tr>
                                                <td colSpan="2" className="text-center">No Order Found</td>
                                            </tr>
                                        )
                                    }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </>
    );
};

export default OrderProcessing;