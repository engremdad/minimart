import React, {useEffect, useState} from "react";
import axios from "axios";
import Success from "../../Notification/Success";
import Error from "../../Notification/Error";
import Router from 'next/router'
import {connect} from "react-redux";

const PurchaseTable = (props) => {

    const [purchases, setPurchases] = useState(null);

    const [successMessage, setSuccessMessage] = useState({
        status: false,
        message: "",
    });

    const [errorMessage, setErrorMessage] = useState({
        status: false,
        message: "",
    });
    4;

    const [pagination, setPagination] = useState(null);

    useEffect(() => {
        let mounted = true;
        fetchPurchaseApi().then((PurchasesData) => {
            if (mounted) {
                setPurchases(PurchasesData.data);
                //setPagination(PurchasesData.data.meta.last_page);
            }
        });
        return () => {
            mounted = false;
        };
    }, []);

    const fetchPurchaseApi = async () => {
        const purchaseApiUrl = await `${process.env.api_backend_url}/purchases`;
        return await axios
            .get(purchaseApiUrl)
            .then((apiResponse) => apiResponse.data)
            .catch((errors) => errors);
    };

    const onclikPiganteDataFetching = async (pageNumber) => {
        const purchaseApiUrl = await `${process.env.api_backend_url}/purchases?page=${pageNumber}`;
        return await axios
            .get(purchaseApiUrl)
            .then((apiResponse) => {
                setPurchases(apiResponse.data);
                //setPagination(apiResponse.data.data.meta.last_page);
            })
            .catch((errors) => errors);
    };


    return (
        <>
            {successMessage.status && <Success message={successMessage.message}/>}
            {errorMessage.status && <Error message={errorMessage.message}/>}
            <section className="custom-card">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="custom-card-box">
                                <div className="custom-card-box-header">
                                    <div className="fbh-title">
                                        <i className="icon-list2"/>
                                        <h2>Purchases List</h2>
                                    </div>
                                </div>
                                <div className="custom-card-box-content">
                                    <div className="row table-top-option mb-3">
                                        <div className="col-sm-6">
                                            <div className="showing-table-row">
                                                <label className="">Show</label>
                                                <select className="custom-select" defaultValue={`25`}>
                                                    <option value="10">10</option>
                                                    <option value="25">25</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div className="col-sm-6">
                                            <div className="table-search">
                                                <label className="">Search</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    placeholder="type your need"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <div className="table-responsive custom-table">
                                                <table className="table table-bordered table-hover table-striped">
                                                    <thead className="thead-light">
                                                    <tr>
                                                        <th scope="col">SL</th>
                                                        <th scope="col">Date</th>
                                                        <th scope="col">Reference No</th>
                                                        <th scope="col">Supplier</th>
                                                        <th scope="col">Purchase Status</th>
                                                        <th scope="col">Grand Total</th>
                                                        <th scope="col">Paid</th>
                                                        {/*<th scope="col">Balance</th>*/}
                                                        <th scope="col">Payment Status</th>
                                                        <th scope="col">Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="table">
                                                    {purchases &&
                                                    purchases.map((purchase, index) => (
                                                        <tr key={index}>
                                                            <td scope="row">{index + 1}</td>
                                                            <td className="nowrap">
                                                                {purchase.purchase_date ?? "-"}
                                                            </td>
                                                            <td className="nowrap">
                                                                {purchase.reference_no ?? "-"}
                                                            </td>
                                                            <td className="nowrap">
                                                                {purchase.supplier ?? "-"}
                                                            </td>
                                                            <td className="nowrap">
                                                                {purchase.status ?? "-"}
                                                            </td>
                                                            <td className="nowrap">
                                                                {purchase.grand_total ?? "-"}
                                                            </td>
                                                            <td className="nowrap">
                                                                {purchase.paid_amount ?? "-"}
                                                            </td>
                                                            {/*<td className="nowrap">*/}
                                                            {/*    {purchase.grand_total-purchase.paid_amount ?? "-"}*/}
                                                            {/*</td>*/}

                                                            <td className="text-center">
                                                                {purchase.payment_status ?? "-"}
                                                            </td>
                                                            <td className="text-center">
                                                                <div className="dropdown action-btn-group">
                                                                    <button
                                                                        className="btn btn-outline-primary rounded-0 btn-sm dropdown-toggle"
                                                                        type="button"
                                                                        data-toggle="dropdown"
                                                                        aria-haspopup="true"
                                                                        aria-expanded="false"
                                                                    >
                                                                        Actions
                                                                    </button>
                                                                    <div className="dropdown-menu">
                                                                        <a
                                                                            className="dropdown-item"
                                                                            href="#"
                                                                            onClick={(event) => {
                                                                                event.preventDefault();
                                                                            }}
                                                                        >
                                                                            <i className="icon-image text-info"/>
                                                                            View Payment
                                                                        </a>
                                                                        <a
                                                                            className="dropdown-item"
                                                                            href="#"
                                                                            onClick={(event) => {
                                                                                event.preventDefault();
                                                                            }}
                                                                        >
                                                                            <i className="icon-image text-info"/>
                                                                            Add Payment
                                                                        </a>
                                                                        <a className="dropdown-item"
                                                                           onClick={(event) => {
                                                                               event.preventDefault();
                                                                               Router.push({pathname: `/admin/purchase/edit/${purchase.id}`});
                                                                           }}
                                                                        >
                                                                            <i className="icon-pencil-alt text-primary"/>{" "}
                                                                            Edit
                                                                        </a>
                                                                        {/*</Link>*/}

                                                                        <a
                                                                            className="dropdown-item"
                                                                            href="#"
                                                                            onClick={(event) => {
                                                                                event.preventDefault();
                                                                            }}
                                                                        >
                                                                            <i className="icon-image text-info"/>
                                                                            View
                                                                        </a>

                                                                        <a
                                                                            className="dropdown-item"
                                                                            href="#"
                                                                            onClick={(event) => {
                                                                                event.preventDefault();
                                                                                // onClickDelete(index, permission.id);
                                                                            }}
                                                                        >
                                                                            <i className="icon-trash-alt-regular text-danger"/>
                                                                            Delete
                                                                        </a>
                                                                        <div className="dropdown-divider"/>

                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    ))}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div className="col-lg-12">
                                            <nav aria-label="Page navigation example">
                                                <ul className="pagination justify-content-end">
                                                    <li className="page-item disabled">
                                                        <a
                                                            className="page-link"
                                                            href="#"
                                                            tabIndex="-1"
                                                            aria-disabled="true"
                                                        >
                                                            Previous
                                                        </a>
                                                    </li>

                                                    {[...Array(pagination)].map((empty, i) => (
                                                        <li key={++i} className="page-item">
                                                            <a
                                                                className="page-link"
                                                                href="#"
                                                                onClick={(event) => {
                                                                    event.preventDefault();
                                                                    onclikPiganteDataFetching(i);
                                                                }}
                                                            >
                                                                {i + 1}
                                                            </a>
                                                        </li>
                                                    ))}
                                                    <li className="page-item">
                                                        <a className="page-link" href="#">
                                                            Next
                                                        </a>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}

const mapDispatchToProps = (store) => {
    return store;
}

export default connect(mapDispatchToProps)(PurchaseTable);