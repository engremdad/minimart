import React, {useEffect,useState} from 'react';
import {connect} from "react-redux";
import {useRouter} from "next/router";
import axios from "axios";
import process from "../../../../next.config";
import {EDIT_PURCHASE} from "../../../../store/admin/actions/purchase/purchaseCartActions";
import PurchaseLeftBar from "../Edit/PurchaseLeftBar";
import SupplierProductSearch from "../Edit/SupplierProductSearch";
import ItemList from "../Edit/ItemList";
import PurchaseCalculation from "../Edit/PurchaseCalculation";
import SubmitPurchase from "../Edit/SubmitPurchase";

const EditPurchase = (props) => {
    const {asPath, route, query} = useRouter();
    const purchaseAPiDataFetching = async (slug) => {
        return await axios
            .get(`/backend/purchases/${slug}`)
            .then((responseApi) => responseApi.data)
            .catch((errors) => errors);
    };
   const [purchaseData , setPurchaseData] = useState([]);

    useEffect(() => {
        let mounted = true;
        if (asPath !== route) {
            purchaseAPiDataFetching(query.purchase_id).then((purchase) => {
                if (mounted) {
                    //console.log(purchase.data)
                    setPurchaseData(purchase.data)
                    props.editPurchase(purchase)
                }
            });
        }
        return () => (mounted = false);
    }, [asPath, route, query]);


    return (
        <>
            <section className="custom-card add-purchase">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="custom-card-box">
                                <div className="custom-card-box-header">
                                    <div className="fbh-title">
                                        <i className="icon-plus-square-regular"/>
                                        <h2>Add Purchase</h2>
                                    </div>
                                    <div className="fbh-info">
                                        <p>Please fill in the information below. The field labels marked with * are
                                            required input fields.
                                        </p>
                                    </div>
                                </div>
                                <div className="custom-card-box-content">
                                    <div className="row">
                                        <div className="col-xl-3 mb-4">
                                            <div className="purchase-options shadow-sm">
                                                <PurchaseLeftBar/>
                                            </div>
                                        </div>
                                        <div className="col-xl-9">
                                            <div className="purchase-details shadow-sm">
                                                <SupplierProductSearch/>
                                                <ItemList/>
                                                <PurchaseCalculation/>

                                                <SubmitPurchase/>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
};

const mapStateToProps = (store) => {
    const {purchaseCart} = store;
    return {purchaseCart};
}
const mapDispatchToProps = (dispatch) => {
    return {
        editPurchase: (purchase) => dispatch({type: EDIT_PURCHASE, payload: {purchase}}),
    };
}


export default connect(mapStateToProps, mapDispatchToProps)(EditPurchase);