import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";
import axios from "axios";
import process from "../../../../next.config";
import {PURCHASE_ADD_TO_CART, SET_SUPPLIER_ID} from "../../../../store/admin/actions/purchase/purchaseCartActions";

const SupplierProductSearch = (props) => {
    const [suppliers, setSuppliers] = useState(null);
    const [searchProductList, setSearchProductList] = useState(null);
    const [word, setWord] = useState(null);

    const userListApi = async () => {
        const userApiUrl = `${process.env.api_backend_url}/users`;
        return await axios.get(userApiUrl).then((apiResponse) => (apiResponse.data)).catch((errors) => (errors));
    }

    useEffect(() => {
        let mounted = true;

        userListApi().then((suppliers) => {
            mounted && (
                suppliers.success && setSuppliers(suppliers.data) || !suppliers.success && setSuppliers('')
            )
        });

        return () => {
            mounted = false
        };
    }, [])

    const WAIT_INTERVAL = 500;
    const ENTER_KEY = 13;
    let keyword;
    let timer = null;

    const searchProducts = (e) => {
        e.preventDefault();

        clearTimeout(timer);

        keyword = e.target.value;
        setWord(keyword)

        timer = setTimeout(getProducts, WAIT_INTERVAL);
    }


    const getProducts = async () => {

        let response = await axios.post(`${process.env.api_backend_url}/search/products`, {
            search_keyword: keyword
        }).then(result => {
            return result.data;
        }).catch(err => {
            return err;
        });

        if (response.success) {
            setSearchProductList(response.data);
        }
    }


    return (
        <>
            <div className="row">
                <div className="col-sm-12">
                    <div className="form-group">
                        <label>Supplier *</label>
                        <div className="input-group">
                            <select
                                onChange={(e) => props.addSupplier(e.target.value)}
                                value={props.purchaseCart.supplier_id}
                                className="custom-select">
                                <option value="">Select Supplier</option>
                                {
                                    suppliers && suppliers.map((supplier, index) => (
                                        <option key={index} value={supplier.id}>{supplier.name}</option>
                                    ))
                                }
                            </select>
                            <div className="input-group-append">
                                <button className="input-group-text" type="button">
                                    <i className="icon-eye-solid"/>
                                </button>
                            </div>
                            <div className="input-group-append">
                                <button className="input-group-text" type="button">
                                    <i className="icon-plus-square-regular"/>
                                </button>
                            </div>

                        </div>
                        {props.purchaseCart.errorMessage.supplier && (
                            <div className="form-text text-danger">
                                {props.purchaseCart.errorMessage.supplier}
                            </div>
                        )}
                    </div>
                </div>


                <div className="col-sm-12">
                    <div className="form-group custom-sr-box">
                        <label>Search Product</label>
                        <div className="input-group mb-2">
                            <div className="input-group-prepend">
                                <div className="input-group-text">
                                    <i className="icon-barcode"/>
                                </div>
                            </div>
                            <input type="text"
                                   id="search_products"
                                   className="form-control border-right-0"
                                   onChange={searchProducts}
                                   placeholder="Please search products to order list"
                                   value={word}
                                   autoComplete="off"
                            />
                            <div className="input-group-prepend">
                                <button className="input-group-text">
                                    <i className="icon-plus-square-regular"/>
                                </button>
                            </div>
                        </div>
                        {
                            (searchProductList && searchProductList.length > 0) ?
                                (
                                    <ul className="src-ul">
                                        {
                                            searchProductList && searchProductList.map((product, index) => (
                                                <li key={index} value={product.id} onClick={(event) => {
                                                    event.preventDefault();
                                                    setWord('');
                                                    setSearchProductList('');
                                                    props.addPurchaseItem(product);
                                                }}>{product.name}</li>
                                            ))
                                        }

                                    </ul>
                                ) : ''
                        }

                    </div>
                </div>

            </div>
        </>
    );
};

const mapDispatchToProps = (dispatch) => {
    return {
        addPurchaseItem: (product) => {
            dispatch({type: PURCHASE_ADD_TO_CART, payload: {product}})
        },

        addSupplier: (supplier_id) => {
            dispatch({type: SET_SUPPLIER_ID, payload: {supplier_id}})
        }
    };
}

const mapStateToProps = (store) => {

    const {purchaseCart} = store;
    return {purchaseCart};
}

export default connect(mapStateToProps, mapDispatchToProps)(SupplierProductSearch);