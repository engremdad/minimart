import React , {useEffect} from 'react';

import {connect} from "react-redux";
import {
    SET_DISCOUNT,
    SET_ORDER_TAX,
    SET_PURCHASE_DATE,
    SET_REFERENCE_NO, SET_SHIPPING_COST,
    SET_STATUS,
    SET_WAREHOUSE, UPLOAD_DOCUMENT
} from "../../../../store/admin/actions/purchase/purchaseCartActions";
const PurchaseLeftBar = (props) => {
    useEffect(() => {
        const bsCustomFileInput = require("bs-custom-file-input");
        bsCustomFileInput.init();
    }, []);
    // console.log('uzzal',props.purchaseData)
    return (
        <>
            <form action="#">
                <div className="row">
                    <div className="col-xl-12 col-lg-6 col-md-6">
                        <div className="form-group">

                            <label>Date *</label>

                            <input type="date"
                                   className="form-control"
                                   value={props.purchaseCart.purchase_date}
                                   onChange={(event) => {
                                       event.preventDefault();
                                       props.setPurchaseDate(event.target.value);
                                   }}
                                   required/>
                            {props.purchaseCart.errorMessage.purchaseDate && (
                                <div className="form-text text-danger">
                                    {props.purchaseCart.errorMessage.purchaseDate}
                                </div>
                            )}
                        </div>
                    </div>
                    <div className="col-xl-12 col-lg-6 col-md-6">
                        <div className="form-group">
                            <label>Reference No *</label>
                            <input type="text"
                                   className="form-control"
                                   value={props.purchaseCart.reference_no}
                                   onChange={(event) => {
                                       event.preventDefault();
                                       props.setReferenceNo(event.target.value);
                                   }}
                                   placeholder="Invoice Number"
                                   required/>
                            {props.purchaseCart.errorMessage.referenceNo && (
                                <div className="form-text text-danger">
                                    {props.purchaseCart.errorMessage.referenceNo}
                                </div>
                            )}
                        </div>
                    </div>
                    <div className="col-xl-12 col-lg-6 col-md-6">
                        <div className="form-group">
                            <label>Status *</label>
                            <select
                                className="form-control"
                                onClick={(event) => {
                                    event.preventDefault();
                                    props.setStatus(event.target.value);
                                }}
                            >
                                <option value="">Select one</option>
                                <option value="Received">Received</option>
                                <option value="Pending">Pending</option>
                                <option value="Ordered">Ordered</option>
                            </select>
                            {props.purchaseCart.errorMessage.status && (
                                <div className="form-text text-danger">
                                    {props.purchaseCart.errorMessage.status}
                                </div>
                            )}
                        </div>
                    </div>
                    <div className="col-xl-12 col-lg-6 col-md-6">
                        <div className="form-group">
                            <label>Attach Document</label>
                            <div className="custom-file">
                                <input type="file"
                                       className="custom-file-input"
                                       name="image"
                                       //value={props.purchaseCart.document}
                                       onChange={(event) => {
                                           event.preventDefault();
                                           props.uploadDocument(event.target.files[0]);
                                       }}
                                       id="customFile"/>
                                <label className="custom-file-label"
                                       htmlFor="customFile">Choose file</label>
                            </div>
                        </div>
                    </div>
                    <div className="col-xl-12 col-lg-6 col-md-6">
                        <div className="form-group">
                            <label>Order VAT/Tax</label>
                            <input type="text"
                                   className="form-control"
                                   placeholder="0.00"
                                   value={props.purchaseCart.total_tax?props.purchaseCart.total_tax:''}
                                   onChange={(event) => {
                                       event.preventDefault();
                                       props.setOrderTax(event.target.value);
                                   }}
                            />
                        </div>
                    </div>
                    <div className="col-xl-12 col-lg-6 col-md-6">
                        <div className="form-group">
                            <label>Total Discount</label>
                            <input type="text"
                                   className="form-control"
                                   placeholder="0.00"
                                   value={props.purchaseCart.total_discount?props.purchaseCart.total_discount:''}
                                   onChange={(event) => {
                                       event.preventDefault();
                                       props.setTotalDiscount(event.target.value);
                                   }}
                            />
                        </div>
                    </div>
                    <div className="col-xl-12 col-lg-6 col-md-6">
                        <div className="form-group">
                            <label>Shipping Cost</label>
                            <input type="text"
                                   className="form-control"
                                   placeholder="0.00"
                                   value={props.purchaseCart.shipping_cost?props.purchaseCart.shipping_cost:''}
                                   onChange={(event) => {
                                       event.preventDefault();
                                       props.setShippingCost(event.target.value);
                                   }}
                            />
                        </div>
                    </div>
                    {/*<div className="col-xl-12 col-lg-6 col-md-6">*/}
                    {/*    <div className="form-group">*/}
                    {/*        <label>Payment Term</label>*/}
                    {/*        <input type="text" className="form-control"*/}
                    {/*               placeholder="Inreger Only" required/>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                </div>
            </form>
        </>
    );
};

const mapDispatchToProps = (dispatch) => {

    return {
        setPurchaseDate: (purchase_date) => dispatch({type: SET_PURCHASE_DATE, payload: {purchase_date}}),
        setReferenceNo: (reference_no) => dispatch({type: SET_REFERENCE_NO, payload: {reference_no}}),
        setWarehouse: (warehouse_id) => dispatch({type: SET_WAREHOUSE, payload: {warehouse_id}}),
        setStatus: (status) => dispatch({type: SET_STATUS, payload: {status}}),
        setOrderTax: (order_tax) => dispatch({type: SET_ORDER_TAX, payload: {order_tax}}),
        setTotalDiscount: (total_discount) => dispatch({type: SET_DISCOUNT, payload: {total_discount}}),
        setShippingCost: (shipping_cost) => dispatch({type: SET_SHIPPING_COST, payload: {shipping_cost}}),
        uploadDocument: (purchase_document) => dispatch({type: UPLOAD_DOCUMENT, payload: {purchase_document}}),
    }
}
const mapStateToProps = (store) => {
    const {purchaseCart} = store;
    return {purchaseCart};
}

export default connect(mapStateToProps, mapDispatchToProps)(PurchaseLeftBar)