import React from 'react';
import {connect} from "react-redux";
import {
    ADD_DISCOUNT,
    ADD_ITEM_QUANTITY, ADD_SUB_TOTAL,
    ADD_UNIT_COST, PURCHASE_REMOVE,
    SET_EXPIRED_DATE
} from "../../../../store/admin/actions/purchase/purchaseCartActions";

const ItemList = (props) => {
    return (
        <>
            <div className="row">
                <div className="col-sm-12">
                    <div className="table-responsive custom-table mb-2">
                        <table className="table table-bordered table-hover">
                            <thead className="thead-light">
                            <tr>
                                <th scope="col">Image</th>
                                <th scope="col">Product (Code - Name ) <i
                                    className="fas fa-sort-amount-up-alt"/>
                                </th>
                                <th scope="col">Expiry Date *</th>
                                <th scope="col">Quantity *</th>
                                <th scope="col">Unit Cost</th>
                                <th scope="col">SubTotal *</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                props.cart && props.cart.map((product, index) => (
                                    <tr key={index}>
                                        <td>
                                            <img src={product.image} alt=""/>
                                        </td>

                                        <td className="nowrap">{product.name} ({product.code})
                                        </td>

                                        <td>
                                            <input className="customs-date" type="date"
                                                   value={product.expire_date}
                                                   onChange={(event) => {
                                                       event.preventDefault();
                                                       props.setExpiredDate(product.id, event.target.value);
                                                   }}
                                            />
                                        </td>

                                        <td className="text-center">

                                            <input type="text" placeholder="0"
                                                   value={product.quantity>0?product.quantity:''}
                                                   onChange={(event) => {
                                                       event.preventDefault();
                                                       props.addItemQuantity(product.id, event.target.value);
                                                   }}
                                            />

                                        </td>

                                        <td>
                                            <input type="number" placeholder="0.00"
                                                   value={product.unit_price?product.unit_price:''}
                                                   readOnly={true}
                                            />
                                        </td>
                                        <td>
                                            <input type="text" placeholder="0.00"
                                                   value={product.sub_total?product.sub_total:''}
                                                   onChange={(event) => {
                                                       event.preventDefault();
                                                       props.addSubTotal(product.id, event.target.value);
                                                   }}
                                            />
                                        </td>

                                        <td className="actions">
                                            <div className="action-wrap">
                                                {/*<button className="action-edit" href="#"><i*/}
                                                {/*    className="icon-pencil-alt"/>*/}
                                                {/*</button>*/}
                                                <button className="action-delete" href="#"
                                                        onClick={(event) => {
                                                            event.preventDefault();
                                                            props.remove(product.id);
                                                            //alert('Click Here');
                                                        }}
                                                >
                                                    <i className="icon-bin"/></button>
                                            </div>
                                        </td>
                                    </tr>
                                ))
                            }

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </>
    );
};

const mapDispatchToProps = (dispatch) => {

    return {
        setExpiredDate: (id, expire_date) => dispatch({type: SET_EXPIRED_DATE, payload: {id, expire_date}}),
        addItemQuantity: (id, quantity) => dispatch({type: ADD_ITEM_QUANTITY, payload: {id, quantity}}),
        addUnitCost: (id, unitCost) => dispatch({type: ADD_UNIT_COST, payload: {id, unitCost}}),
        addDiscount: (id, discount) => dispatch({type: ADD_DISCOUNT, payload: {id, discount}}),
        addSubTotal: (id, subtotal) => dispatch({type: ADD_SUB_TOTAL, payload: {id, subtotal}}),
        remove: (id) => dispatch({type: PURCHASE_REMOVE, payload: {id}}),
    }
}
const mapStateToProps = (store) => {
    const {cart} = store.purchaseCart;
    return {cart};

}

export default connect(mapStateToProps, mapDispatchToProps)(ItemList)
