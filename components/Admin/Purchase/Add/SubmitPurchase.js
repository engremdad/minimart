import React from 'react';
import axios from "axios";
import {CHECK_VALIDATE, RESET_PURCHASE, SAVE_PURCHASE} from "../../../../store/admin/actions/purchase/purchaseCartActions";
import {connect} from "react-redux";

const SubmitPurchase = (props) => {

    const submitPurchase = async () => {
        props.checkValidate()
        if (props.purchaseCart.purchase_date && props.purchaseCart.reference_no && props.purchaseCart.reference_no && props.purchaseCart.status && props.purchaseCart.supplier_id) {
            let response = await axios.post(`backend/purchases`, props.purchaseCart, {
                headers: {
                    // Overwrite Axios's automatically set Content-Type
                    'Content-Type': 'application/json'
                }
            })
                .then((result) => {
                    return result.data;
                }).catch((error) => {
                    return error;
                });

            if (response.success) {
                props.savePurchase();
            } else {

            }
        }
    }

    return (
        <>
            <div className="row">
                <div className="col-lg-12">
                    <div className="d-flex justify-content-between final-actions">
                        <button className="btn btn-reset"
                                onClick={(event) => {
                                    event.preventDefault();
                                    props.resetPurchase();
                                }}
                        >
                            <i className="icon-cross"/>
                            <span>Reset</span>
                        </button>
                        <button className="btn btn-submit"
                                onClick={(event) => {
                                    event.preventDefault();
                                    submitPurchase();
                                }}
                                disabled={props.purchaseCart.grand_total ? false : true}
                        >
                            <i className="icon-checkmark"/>
                            <span>Submit</span>
                        </button>
                    </div>
                </div>

            </div>
        </>
    );
};


const mapStateToProps = (store) => {
    const {purchaseCart} = store;
    return {purchaseCart};
}

const mapDispatchToProps = (dispatch) => {
    return {
        savePurchase: () => dispatch({type: SAVE_PURCHASE, payload: ''}),
        resetPurchase: () => dispatch({type: RESET_PURCHASE, payload: ''}),
        checkValidate: () => dispatch({type: CHECK_VALIDATE, payload: ''}),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SubmitPurchase);