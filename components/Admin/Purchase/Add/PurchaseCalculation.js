import React from 'react';

import { connect } from "react-redux";
import {RESET_PURCHASE, SET_PURCHASE_NOTE} from "../../../../store/admin/actions/purchase/purchaseCartActions";

const PurchaseCalculation = (props) => {



    return (
        <>
            <div className="pos-form-bottom">
                <div className="cart-summary">
                    <div className="row align-items-center">

                        <div className="col-sm-6">
                            <div className="cs-item-wrap">
                                <span className="cs-items">Items:</span>
                                <span className="cs-items-total">{props.purchaseCart.cart.length}</span>
                            </div>
                        </div>

                        <div className="col-sm-6">
                            <div className="cs-amaount-cal">

                                <div className="csac-total">
                                    <span className="csac-total-title">Total</span>
                                    <span className="csac-total-cal">৳ {props.purchaseCart.sub_total}</span>
                                </div>

                                <div className="csac-coupon">
                                    <span className="csac-coupon-title">Vat/Tax.</span>
                                    <span className="csac-coupon-cal">৳ {props.purchaseCart.total_tax}</span>
                                </div>

                                <div className="csac-shipping">
                                    <span className="csac-shipping-title">Shipping</span>
                                    <span className="csac-shipping-cost">৳ {props.purchaseCart.shipping_cost}</span>
                                </div>
                                <div className="csac-discount">
                                    <span className="csac-discount-title">Discount</span>
                                    <span className="csac-discount-cal">- ৳ {props.purchaseCart.total_discount}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="grand-total">
                                <span className="grand-total-wrap">

                                    <span className="grand-total-title">Grand Total</span>
                                    <span className="grand-total-amount">৳ {props.purchaseCart.grand_total}</span>

                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-12">
                    <div className="form-group">
                        <label>Note (if any)</label>
                        <textarea className="form-control"
                            rows="4"
                                  value={props.purchaseCart.note}
                            onChange={(event) => {
                                event.preventDefault();
                                props.setPurchaseNote(event.target.value);
                            }}
                        />
                    </div>
                </div>
            </div>
        </>
    );
};
const mapStateToProps = (store) => {
    const {purchaseCart} = store;
    return {purchaseCart};
}
const mapDispatchToProps = (dispatch) => {

    return {
        setPurchaseNote: (purchase_note) => dispatch({ type: SET_PURCHASE_NOTE, payload: { purchase_note } }),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PurchaseCalculation)