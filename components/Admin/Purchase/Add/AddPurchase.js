import React, {useState} from 'react';
import ItemList from "./ItemList";
import PurchaseCalculation from "./PurchaseCalculation";
import SupplierProductSearch from "./SupplierProductSearch";
import PurchaseLeftBar from "./PurchaseLeftBar";
import SubmitPurchase from "./SubmitPurchase";


const AddPurchase = (props) => {

    const [purchaseError, setPurchaseError] = useState({
        date_error: "",
        reference_number_error: "",
        status_error: "",
        document_error: "",
        order_tax_error: "",
        total_discount_error: "",
        shipping_cost_error: "",
        supplier_error: "",
    });


    return (
        <>
            <section className="custom-card add-purchase">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="custom-card-box">
                                <div className="custom-card-box-header">
                                    <div className="fbh-title">
                                        <i className="icon-plus-square-regular"/>
                                        <h2>Add Purchase</h2>
                                    </div>
                                    <div className="fbh-info">
                                        <p>Please fill in the information below. The field labels marked with * are
                                            required input fields.
                                        </p>
                                    </div>
                                </div>
                                <div className="custom-card-box-content">
                                    <div className="row">
                                        <div className="col-xl-3 mb-4">
                                            <div className="purchase-options shadow-sm">
                                                <PurchaseLeftBar/>
                                            </div>
                                        </div>
                                        <div className="col-xl-9">
                                            <div className="purchase-details shadow-sm">
                                                <SupplierProductSearch/>
                                                <ItemList/>
                                                <PurchaseCalculation/>

                                                <SubmitPurchase/>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
};

export default AddPurchase;