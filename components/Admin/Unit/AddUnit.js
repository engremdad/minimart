import Success from "../Notification/Success";
import Error from "../Notification/Error";
import process from "../../../next.config";
import React, { useState } from "react";
import axios from "axios";
import Link from "next/link";

const AddUnit = () => {
  const [unit, setUnit] = useState({
    name: "",
    unit_code: "",
    base_unit: "",
    operator: "",
    operation_value: "",
    active: "1",
  });

  const [unitError, setUnitError] = useState({
    name: "",
    name_error: "",
    unit_code: "",
    unit_code_error: "",
    base_unit: "",
    base_unit_error: "",
    operator: "",
    operator_error: "",
    operation_value: "",
    operation_value_error: "",
    active: "",
    active_error: "",
  });

  const [disabledFiled, setDisabledFiled] = useState("");

  const [successMessage, setSuccessMessage] = useState({
    status: false,
    message: "",
  });

  const [errorMessage, setErrorMessage] = useState({
    status: false,
    message: "",
  });

  const handleUnitData = (event) => {
    let name = event.target.name;
    let value = event.target.value;
    if (name === "active") {
      value = event.target.checked ? "1" : "0";
    }
    setUnit({ ...unit, [name]: value });
    setUnitError({ ...unitError, [name]: "", [name + "_error"]: "" });
  };

  const submitUnitData = (event) => {
    event.preventDefault();
    if (unit.name.length === 0) {
      setUnitError({
        ...unitError,
        name: "is-invalid",
        name_error: "Unit name filed is required",
      });
    } else {
      setDisabledFiled("disabled");
      const unitFormData = new FormData();
      unit.name && unitFormData.append("name", unit.name);
      unit.unit_code && unitFormData.append("unit_code", unit.unit_code);
      unit.base_unit && unitFormData.append("base_unit", unit.base_unit);
      unit.operator && unitFormData.append("operator", unit.operator);
      unit.operation_value &&
        unitFormData.append("operation_value", unit.operation_value);
      unitFormData.append("active", unit.active);

      const unitApiUrl = `${process.env.api_backend_url}/units`;
      axios
        .post(unitApiUrl, unitFormData)
        .then((apiResponses) => {
          if (apiResponses.data.success) {
            setDisabledFiled("disabled");
            resetUnitFormData(event);
            setSuccessMessage({
              ...successMessage,
              status: true,
              message: apiResponses.data.message,
            });
          } else {
            setUnitError({
              ...unitError,
              name:
                typeof apiResponses.data.errors.name != "undefined"
                  ? "is-invalid"
                  : "",
              name_error:
                typeof apiResponses.data.errors.name != "undefined"
                  ? apiResponses.data.errors.name.join()
                  : "",
              unit_code:
                typeof apiResponses.data.errors.unit_code != "undefined"
                  ? "is-invalid"
                  : "",
              unit_code_error:
                typeof apiResponses.data.errors.unit_code != "undefined"
                  ? apiResponses.data.errors.unit_code.join()
                  : "",
              base_unit:
                typeof apiResponses.data.errors.base_unit != "undefined"
                  ? "is-invalid"
                  : "",
              base_unit_error:
                typeof apiResponses.data.errors.base_unit != "undefined"
                  ? apiResponses.data.errors.base_unit.join()
                  : "",
              operator:
                typeof apiResponses.data.errors.operator != "undefined"
                  ? "is-invalid"
                  : "",
              operator_error:
                typeof apiResponses.data.errors.operator != "undefined"
                  ? apiResponses.data.errors.operator.join()
                  : "",
              operation_value:
                typeof apiResponses.data.errors.operation_value != "undefined"
                  ? "is-invalid"
                  : "",
              operation_value_error:
                typeof apiResponses.data.errors.operation_value != "undefined"
                  ? apiResponses.data.errors.operation_value.join()
                  : "",
              active:
                typeof apiResponses.data.errors.active != "undefined"
                  ? "is-invalid"
                  : "",
              active_error:
                typeof apiResponses.data.errors.active != "undefined"
                  ? apiResponses.data.errors.active.join()
                  : "",
            });
            setDisabledFiled("");
            setErrorMessage({
              ...errorMessage,
              status: true,
              message: apiResponses.data.message,
            });
            setTimeout(
              () =>
                setErrorMessage({
                  ...errorMessage,
                  status: false,
                  message: "",
                }),
              5000
            );
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  const resetUnitFormData = (event) => {
    event.preventDefault();
    document.getElementById("unitFormData").reset();

    setUnit({
      name: "",
      unit_code: "",
      base_unit: "",
      operation_value: "",
      active: "1",
    });

    setUnitError({
      name: "",
      name_error: "",
      unit_code: "",
      unit_code_error: "",
      base_unit: "",
      base_unit_error: "",
      operation_value: "",
      operation_value_error: "",
      active: "",
      active_error: "",
    });
    setDisabledFiled("");
    setTimeout(
      () =>
        setSuccessMessage({ ...successMessage, status: false, message: "" }),
      5000
    );
  };

  return (
    <>
      {successMessage.status && <Success message={successMessage.message} />}

      {errorMessage.status && <Error message={errorMessage.message} />}
      <section className="custom-card">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-12">
              <div className="custom-card-box">
                <div className="custom-card-box-header">
                  <div className="fbh-title">
                    <i className="far fa-plus-square" />
                    <h2>Add Unit</h2>
                    <Link href={`/admin/unit`}>
                      <a className="fbh-btn" title="Unit List">
                        <i className="icon-list2" />
                      </a>
                    </Link>
                  </div>
                  <div className="fbh-info">
                    <p>
                      Please fill in the information below. The field labels
                      marked with * are required input fields.
                    </p>
                  </div>
                </div>

                <div className="custom-card-box-content">
                  <div className="row">
                    <div className="col-lg-12">
                      <form id="unitFormData" method="POST">
                        <fieldset disabled={disabledFiled ?? ""}>
                          <div className="row">
                            <div className="col-lg-4">
                              <div className="form-group">
                                <label htmlFor="name">
                                  {" "}
                                  Name<span className="text-danger"> *</span>
                                </label>
                                <input
                                  type="text"
                                  className={`form-control ${unitError.name}`}
                                  placeholder=" name"
                                  id="name"
                                  name="name"
                                  onChange={handleUnitData}
                                  required
                                />
                                {unitError.name && (
                                  <div className="invalid-feedback">
                                    {unitError.name_error}
                                  </div>
                                )}
                              </div>
                            </div>

                            <div className="col-lg-4">
                              <div className="form-group">
                                <label htmlFor="unit_code"> Unit code</label>
                                <input
                                  type="text"
                                  className={`form-control ${unitError.unit_code}`}
                                  placeholder=" unit code"
                                  id="unit_code"
                                  name="unit_code"
                                  onChange={handleUnitData}
                                  required
                                />
                                {unitError.unit_code && (
                                  <div className="invalid-feedback">
                                    {unitError.unit_code_error}
                                  </div>
                                )}
                              </div>
                            </div>

                            <div className="col-lg-4">
                              <div className="form-group">
                                <label htmlFor="name"> Base unit</label>
                                <input
                                  type="text"
                                  className={`form-control ${unitError.base_unit}`}
                                  placeholder="base unit"
                                  id="base_unit"
                                  name="base_unit"
                                  onChange={handleUnitData}
                                  required
                                />
                                {unitError.base_unit && (
                                  <div className="invalid-feedback">
                                    {unitError.base_unit_error}
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>

                          <div className="row">
                            <div className="col-lg-6">
                              <div className="form-group">
                                <label htmlFor="operator"> Operator</label>
                                <select
                                  className={`custom-select mr-sm-2 ${unitError.base_unit}`}
                                  id="operator"
                                  name="operator"
                                  onChange={handleUnitData}
                                >
                                  <option>Choose...</option>
                                  <option value="+">Plus (+)</option>
                                  <option value="-">Minus (-)</option>
                                  <option value="*">Multiply (*)</option>
                                  <option value="/">Division (/)</option>
                                </select>
                                {unitError.operator && (
                                  <div className="invalid-feedback">
                                    {unitError.operator_error}
                                  </div>
                                )}
                              </div>
                            </div>

                            <div className="col-lg-6">
                              <div className="form-group">
                                <label htmlFor="operation_value">
                                  {" "}
                                  Operation value
                                </label>
                                <input
                                  type="text"
                                  className={`form-control ${unitError.operation_value}`}
                                  placeholder=" operation value"
                                  id="operation_value"
                                  name="operation_value"
                                  onChange={handleUnitData}
                                  required
                                />
                                {unitError.operation_value && (
                                  <div className="invalid-feedback">
                                    {unitError.operation_value_error}
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>

                          <div className="row">
                            <div className="col-lg-6 text-left">
                              <div className="form-group">
                                <div className="custom-control custom-checkbox">
                                  <input
                                    type="checkbox"
                                    className="custom-control-input"
                                    id="active"
                                    name="active"
                                    defaultChecked={true}
                                    onClick={handleUnitData}
                                  />
                                  <label
                                    className="custom-control-label"
                                    htmlFor="active"
                                  >
                                    Active
                                  </label>
                                  {unitError.active && (
                                    <div className="invalid-feedback">
                                      {unitError.active_error}
                                    </div>
                                  )}
                                </div>
                              </div>
                            </div>
                            <div className="col-lg-6 text-right">
                              <Link href={"/admin/unit"}>
                                <a className="btn btn-info mr-1">
                                  <i className="icon-arrow-left-solid mr-1 mr-1" />
                                  back to list
                                </a>
                              </Link>

                              <button
                                className="btn btn-danger mr-1"
                                onClick={resetUnitFormData}
                              >
                                <i className="icon-reset mr-1 mr-1" />
                                reset
                              </button>
                              <button
                                className="btn btn-primary"
                                onClick={submitUnitData}
                              >
                                {disabledFiled === "disabled" ? (
                                  <>
                                    <span
                                      className="spinner-border spinner-border-sm"
                                      role="status"
                                      aria-hidden="true"
                                    />{" "}
                                    saving...
                                  </>
                                ) : (
                                  <>
                                    <i className="icon-save-regular mr-1" />{" "}
                                    save
                                  </>
                                )}
                              </button>
                            </div>
                          </div>
                        </fieldset>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default AddUnit;
