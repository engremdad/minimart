import React, {useEffect, useState} from 'react';
import axios from "axios";
import {SET_PRODUCTS} from "../../../store/pos/actions/posProductActions";
import {connect} from "react-redux";

const CategorySearch = (props) => {

    const [categories, setCategories] = useState([]);

    const [selectedCategory, setSelectedCategory] = useState('');

    useEffect(() => {
        let mounted = true;
        fetchCategories().then((categoryResponse) => {
            if (mounted) {
                setCategories(categoryResponse.data);
            }
        });
        return () => {
            mounted = false;
        };

    }, []);


    const fetchCategories = async () => {
        return axios.get(`backend/categories`)
            .then((result) => result.data).catch((error) => error);
    }


    const handleChange = (e) => {
        e.preventDefault();
        let category = e.target.value;
        setSelectedCategory(category);

        category && getProductsByCategory(category)
            .then(response => {
                if (response.success) {
                    props.setProducts(response.data);
                }
            }).catch(err => {
                console.log(err);
            })
    }


    const getProductsByCategory = async (category_id) => {


        return await axios.post(`backend/get-product-bycatagory`, {
            category_id: category_id
        }).then(result => {
            console.log(result.data);
            return result.data;
        }).catch((error) => {
            return error;
        });
    }

    return (
        <>
            <div className="col-lg-6">
                <div className="input-group">

                    <select
                        className="custom-select"
                        id="category"
                        onChange={handleChange}
                        defaultValue={selectedCategory}
                        aria-label="Category Select">
                        <option value="">Select Category</option>
                        {
                            categories && categories.length && categories.map((category) => (
                                <option value={category.id} key={category.id}>{category.name}</option>
                            ))
                        }

                    </select>
                </div>
            </div>
        </>
    );
};

const mapDispatchToProps = (dispatch) => {
    return {
        setProducts: (products) => dispatch({type: SET_PRODUCTS, payload: {products}}),
    }
}

export default connect(null, mapDispatchToProps)(CategorySearch);