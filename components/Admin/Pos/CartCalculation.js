import React from 'react';
import {connect} from "react-redux";
import {
    CLEAR_QUICK_CASH_NOTE,
    POS_CHECKOUT,
    SET_PAYMENT_METHOD, SET_PAYMENT_NOTE,
    SET_PAYMENT_STATUS,
    SET_POS_AMOUNT, SET_POS_DISCOUNT, SET_POS_SHIPPING, SET_QUICK_CASH_NOTE, SET_STAFF_NOTE
} from "../../../store/pos/actions/posCartActions";

const CartCalculation = (props) => {
     return (
        <>
            <div className="pos-form-bottom">
                <div className="cart-summary">
                    <div className="row align-items-center">

                        <div className="col-sm-6">
                            <span className="cs-items">Items:</span>
                            <span className="cs-items-total">{props.cart.length}</span>
                        </div>

                        <div className="col-sm-6">
                            <div className="cs-amaount-cal">

                                <div className="csac-total">
                                    <span className="csac-total-title">Total-</span>
                                    <span className="csac-total-cal">৳ {props.sub_total}</span>
                                </div>


                                <div className="csac-shipping">
                                    <span className="csac-shipping-title">Shipping-</span>
                                    <span className="csac-shipping-cost">
                                  <input type="text" placeholder="00.00"
                                         value={props.delivery_charge?props.delivery_charge:''}
                                         onChange={(event) => {
                                             event.preventDefault();
                                             props.setShipping(event.target.value);
                                         }}
                                  />
                                </span>
                                </div>

                                <div className="csac-discount">
                                    <span className="csac-discount-title">Discount-</span>
                                    <span className="csac-discount-cal">
                                  <input type="text" placeholder="00.00"
                                         value={props.discount?props.discount:''}
                                         onChange={(event) => {
                                             event.preventDefault();
                                             props.setDiscount(event.target.value);
                                         }}
                                  />
                                </span>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="grand-total">
                              <span className="grand-total-wrap">

                                <span className="grand-total-title">Grand Total</span>
                                <span className="grand-total-amount">৳ {props.grand_total}</span>

                              </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

const mapStateToProps = (store) => {
    const {cart, sub_total,delivery_charge,discount, grand_total} = store.posCart;
    return {cart, sub_total,delivery_charge, discount, grand_total};
}
const mapDispatchToProps = (dispatch) => {
    return {
        setDiscount: (discount) => dispatch({ type: SET_POS_DISCOUNT,payload: {discount}}),
        setShipping: (delivery_charge) => dispatch({ type: SET_POS_SHIPPING,payload: {delivery_charge}}),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(CartCalculation);