import React from 'react';
import {connect} from "react-redux";
import {POS_DECREASE, POS_INCREASE, POS_REMOVE} from "../../../store/pos/actions/posCartActions";

const CartItem = (props) => {

    return (
        <>
            <tr>
                <td>
                    <div className="cart-product">
                        <img src={props.item.image}
                             alt={props.item.name}/>
                        <span>{props.item.name}</span>
                    </div>
                </td>

                <td>
                    <div className="cart-count input-group">
                        <button className="decrise" onClick={(event) => {
                            event.preventDefault();
                            props.item.quantity > 1 ? props.decrease(props.item.id) : props.remove(props.item.id);
                        }}>
                            <i className="icon-minus-solid"/>
                        </button>

                        <input name="qty" type="text" readOnly value={props.item.quantity}/>

                        <button className="incrise" onClick={(event) => {
                            event.preventDefault();
                            props.increase(props.item.id);
                        }}>
                            <i className="icon-plus-solid"/>
                        </button>
                    </div>
                </td>
                <td className="pos-unit-price">
                    ৳ {props.item.unit_price}
                </td>
                <td className="pos-subtotal">
                    ৳ {props.item.price}
                </td>
                <td className="pos-cart-remove">
                    <button className="btn btn-danger" onClick={(event) => {
                        event.preventDefault();
                        props.remove(props.item.id);
                    }}><i
                        className="icon-trash-alt-regular"/>
                    </button>
                </td>
            </tr>
        </>
    );
};

function mapDispatchToProps(dispatch) {

    return {
        increase: (id) => dispatch({type: POS_INCREASE, payload: {id}}),
        decrease: (id) => dispatch({type: POS_DECREASE, payload: {id}}),
        remove: (id) => dispatch({type: POS_REMOVE, payload: {id}})
    }
}

export default connect(null, mapDispatchToProps)(CartItem);
