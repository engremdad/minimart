import React from 'react';
import CartItem from "./CartItem";
import {connect} from "react-redux";

const CartProducts = ({cart = []}) => {

    return (
        <>
            <div className="pos-form-center">
                <table className="table table-bordered">
                    <thead className="thead-light">
                    <tr>
                        <th scope="col" className="text-center">Product</th>
                        <th scope="col">Qty</th>
                        <th scope="col">Price</th>
                        <th scope="col">Subtotal</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        {cart.length > 0 && cart.map((item) => (<CartItem key={item.id} item={item}/>))}
                    </tbody>
                </table>
            </div>
        </>
    );
};

const mapStateToProps = (store) => {

    const {cart} = store.posCart;

    return {cart};
}

export default connect(mapStateToProps)(CartProducts);
