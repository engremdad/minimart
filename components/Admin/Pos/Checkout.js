import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";
import {POS_CANCEL_ORDER} from "../../../store/pos/actions/posCartActions";

const Checkout = (props) => {
    const [disable, setDisable] = useState(true);

    useEffect(() => {
        (props.posCart.grand_total > 0) ? setDisable(false) : setDisable(true);
        const showModal = event => {
            if (event.code === "NumpadEnter") {
                event.preventDefault();
                $('#checkOutModal').modal('show');
            }
        };
        document.addEventListener("keydown", showModal);
        return () => {
            document.removeEventListener("keydown", showModal);
        };
    }, [props.posCart.grand_total]);

    return (
        <>
            <div className="cart-action shadow-lg">

                <button className="btn btn-cancle"
                        onClick={(event) => {
                            event.preventDefault();
                            props.cancelPosOrder();
                        }}
                >
                    <i className="icon-trash-alt-regular"/>
                    <span>Cancel</span>
                </button>

                {/*<button className="btn btn-draft">*/}
                {/*    <i className="icon-save-regular"/>*/}
                {/*    <span>Save to Draft</span>*/}
                {/*</button>*/}


                <button className="btn btn-cash-pay" data-toggle="modal" data-target="#checkOutModal"
                        disabled={disable}
                >
                    <i className="icon-money-check-alt-solid"/>
                    <span>Cash Payment</span>
                </button>

            </div>
        </>
    );
};

const mapDispatchToProps = (dispatch) => {
    return {
        cancelPosOrder: () => {
            dispatch({type: POS_CANCEL_ORDER})
        }
    };
}

const mapStateToProps = (store) => {

    const posCart = store.posCart;

    return {posCart};
}


export default connect(mapStateToProps, mapDispatchToProps)(Checkout);