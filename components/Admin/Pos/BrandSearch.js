import React, {useEffect, useState} from 'react';
import axios from "axios";
import {connect} from "react-redux";
import {SET_PRODUCTS} from "../../../store/pos/actions/posProductActions";

const BrandSearch = (props) => {

    const [brands, setBrands] = useState([]);

    const [selectedBrand, setSelectedBrand] = useState('');

    useEffect(() => {
        let mounted = true;
        fetchBrands().then((brandResponse) => {
            if (mounted) {
                setBrands(brandResponse.data);
            }
        });
        return () => {
            mounted = false;
        };

    }, [])

    const fetchBrands = async () => {

        return axios.get(`backend/brands`)
            .then((result) => result.data).catch((error) => error);

    }

    const handleChange = async (e) => {
        e.preventDefault();
        let brand_id = e.target.value;

        setSelectedBrand(brand_id);

        brand_id && getProductsByBrand(brand_id).then(response => {
            if (response.success) {
                props.setProducts(response.data);
            }
        })
    }

    const getProductsByBrand = async (brand_id) => {
        return await axios.post(`backend/get-product-bybrand/`, {
            brand_id: brand_id
        }).then(result => {
            console.log(result.data);
            return result.data;
        }).catch((error) => {
            return error;
        });

    }


    return (
        <>
            <div className="col-lg-6">
                <div className="input-group">
                    <select
                        className="custom-select"
                        id="brand"
                        onChange={handleChange}
                        defaultValue={selectedBrand}
                        aria-label="Brand Select">
                        <option value="">Select Brand</option>
                        {
                            brands.length && brands.map((brand, index) => (
                                <option value={brand.id} key={index}>{brand.name}</option>
                            ))
                        }
                    </select>
                </div>
            </div>
        </>
    );
};


const mapDispatchToProps = (dispatch) => {
    return {
        setProducts: (products) => dispatch({type: SET_PRODUCTS, payload: {products}}),
    }
}

export default connect(null, mapDispatchToProps)(BrandSearch);