import React, {useEffect, useState} from 'react';
import axios from "axios";
import {POS_SET_CUSTOMER} from "../../../store/pos/actions/posCartActions";
import {connect} from "react-redux";
import {useRouter} from "next/router";

const Customer = (props) => {
    const [customerEditable, setCustomerEditable] = useState(true);
    const [customers, setCustomers] = useState([]);
    const [selectedCustomer, setSelectedCustomer] = useState('');
    const router = useRouter();

    useEffect(() => {

        let mounted = true;

        getCustomers().then((customerResponse) => {
            if (mounted) {
                customerResponse.data.map((customer) => {
                    if (customer.name === "Walk In Customer") {
                        setSelectedCustomer(customer.id);
                        props.setCustomer(customer.id)
                    }
                })
                setCustomers(customerResponse.data);
            }
        });
        return () => {
            mounted = false;
        };
    }, []);


    const getCustomers = async () => {

        const api_url = await process.env.api_backend_url;

        return axios.get(`${api_url}/users`)
            .then((result) => result.data).catch((error) => error);
    }


    const handleChange = (e) => {
        e.preventDefault();
        setSelectedCustomer(e.currentTarget.value);
        props.setCustomer(e.currentTarget.value);
    }

    return (
        <>
            <div className="pos-form-top">
                <div className="input-group">

                    <select
                        className="custom-select"
                        id="customer"
                        aria-label="Customer select"
                        value={selectedCustomer}
                        onChange={handleChange}
                        disabled={customerEditable}
                    >
                        {
                            customers && customers.map((customer, index) => (
                                <option value={customer.id} key={++index}>{customer.name}</option>
                            ))
                        }

                    </select>

                    <div className="input-group-append" id="button-addon4">
                        <a className="btn btn-outline-secondary" type="button"
                        onClick={(event)=>{
                            event.preventDefault();
                            setCustomerEditable(!customerEditable);
                        }}
                        ><i
                            className="icon-edit-solid"/></a>
                        <a className="btn btn-outline-secondary" type="button">
                            <i className="icon-eye-solid"/></a>
                        <a className="btn btn-outline-secondary"
                           onClick={() => router.push('/admin/user/add/')}
                           type="button">
                            <i className="icon-plus-circle-solid"/></a>
                    </div>
                </div>
            </div>
        </>
    );
};
const mapDispatchToProps = (dispatch) => {
    return {
        setCustomer: (user_id) => dispatch({type: POS_SET_CUSTOMER, payload: {user_id}}),
    };
}
const mapStateToProps = (store) => {

    const posCart = store.posCart;

    return {posCart};
}

export default connect(mapStateToProps, mapDispatchToProps)(Customer);