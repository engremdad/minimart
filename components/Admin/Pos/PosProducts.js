import React, {useEffect,useState} from 'react';
import axios from "axios";
import {connect} from "react-redux";
import {SET_PRODUCTS} from "../../../store/pos/actions/posProductActions";
import SingleProduct from "./SingleProduct";
import Loader from "react-loader";
import {options} from "../../../Helpers/Utils/Utils";

const PosProducts = (props) => {
    const [loaded, setLoaded] = useState(false);

    const api_url = process.env.mode === 'development' ? process.env.api_url_dev : process.env.api_url;


    useEffect(async () => {
        await getPopularProducts();

    }, []);

    const getPopularProducts = async () => {

        let response = await axios.get(`${api_url}/get-popular-products`).then((result) => {
            return result.data;
        }).catch((error) => {
            return error;
        });

        if (response.success) {
            setLoaded('true')
            props.setProducts(response.data);
        }
    }


    return (
        <>
            <Loader loaded={loaded} options={options} className="spinner">
                <div className="row">
                {props.products && props.products.map((product, index) => (
                    <SingleProduct product={product} key={++index}/>
                ))}
                </div>
            </Loader>
        </>
    );
};

const mapStateToProps = (store) => {

    const {products} = store.posProduct;

    return {products};
}


const mapDispatchToProps = (dispatch) => {

    return {
        setProducts: (products) => dispatch({type: SET_PRODUCTS, payload: {products}})
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(PosProducts);