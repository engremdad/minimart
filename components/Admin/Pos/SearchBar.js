import React, {useEffect} from 'react';
import axios from "axios";
import {SET_PRODUCTS} from "../../../store/pos/actions/posProductActions";
import {connect} from "react-redux";
import {POS_ADD_TO_CART, SET_SEARCH_KEYWORDS} from "../../../store/pos/actions/posCartActions";

const SearchBar = (props) => {

    useEffect(() => {
        document.getElementById('search_products').focus();
    }, []);

    const WAIT_INTERVAL = 500;
    const ENTER_KEY = 13;
    let keyword;
    let timer = null;

    const searchProducts = (e) => {
        e.preventDefault();

        clearTimeout(timer);

        keyword = e.target.value;

        timer = setTimeout(getProducts, WAIT_INTERVAL);
    }


    const getProducts = async () => {

        // set search keyword to redux for marking the searching result
        props.setSearchKeywords(keyword);

        let response = await axios.post(`backend/search/products`, {
            search_keyword: keyword
        }).then(result => {
            return result.data;
        }).catch(err => {
            return err;
        });

        if (response.success) {

            let product = response.data && response.data.find(item => item.barcode === keyword);

            // product barcode and search keyword same then directly product will added to cart and clear the search keyword
            if (product) {
                props.posAddToCart(product);
                props.setSearchKeywords('');
            } else {
                props.setProducts(response.data)
            }

        }

    }


    return (
        <>
            <div className="row">
                <div className="col-sm-12">
                    <div className="input-group">

                        <input
                            id="search_products"
                            type="text"
                            className="form-control"
                            onChange={searchProducts}
                            value={keyword}
                            placeholder="Search/Scan product by Name/Barcode"/>

                        <div className="input-group-append" id="button-addon4">
                            <button className="btn btn-outline-secondary"
                                    type="button">
                                <i className="icon-plus-circle-solid"/>
                            </button>
                        </div>


                    </div>
                </div>
            </div>
        </>
    );
};

const mapDispatchToProps = (dispatch) => {
    return {
        setProducts: (products) => dispatch({type: SET_PRODUCTS, payload: {products}}),
        posAddToCart: (product) => {
            dispatch({type: POS_ADD_TO_CART, payload: {product}})
            document.getElementById('search_products').value = '';
        },
        setSearchKeywords: (keyword) => dispatch({type: SET_SEARCH_KEYWORDS, payload: {keyword}})
    }
}

export default connect(null, mapDispatchToProps)(SearchBar);