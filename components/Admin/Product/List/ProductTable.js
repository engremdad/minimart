import React, { useState, useEffect } from "react";
import Link from "next/link";
import axios from "axios";
import ReactPaginate from "react-paginate";
import useDebounce from "../custom-hook/use-debounce";
import MySucces from "../../Notification/MySuccess";
import MyError from "../../Notification/MyError";
import Loader from "react-loader";
import { options } from "../../../../Helpers/Utils/Utils";

const ProductTable = ({ products }) => {
  const [pigantionMeta, setPigantionMeta] = useState(products.meta);
  const [listPerPage, setListPerPage] = useState(products.meta.per_page);
  const [searchingKeyWord, setSearchingKeyWord] = useState("");
  const [productList, setProductList] = useState(products.data);
  const [disabledFiled, setDisabledFiled] = useState("");

  const [successMessage, setSuccessMessage] = useState({
    status: false,
    message: "",
  });

  const [errorMessage, setErrorMessage] = useState({
    status: false,
    message: "",
  });

  const resetSuccesMessage = () => {
    setSuccessMessage({
      ...successMessage,
      status: false,
      message: "",
    });
  };

  const resetErrorMessage = () => {
    setErrorMessage({
      ...errorMessage,
      status: false,
      message: "",
    });
  };

  const debouncedSearch = useDebounce(searchingKeyWord, 1000);

  const [searchingFlag, setSearchingFlag] = useState(false);

  useEffect(() => {
    if (debouncedSearch) {
      setSearchingFlag(false);
      setPigantionMeta("");
      getProductList("", listPerPage);
    }
    if (searchingFlag) {
      setSearchingFlag(false);
      setPigantionMeta("");
      getProductList("", listPerPage);
    }
  }, [debouncedSearch]);

  const getProductList = async (pageNumber, showingNumber) => {
    showingNumber && setListPerPage(showingNumber);
    try {
      const productApiUrl = pageNumber
        ? `${
            process.env.api_backend_url
          }/getAllProduct?page=${pageNumber}&show=${showingNumber}${
            searchingKeyWord && `&searchKeyword=${searchingKeyWord}`
          }`
        : `${process.env.api_backend_url}/getAllProduct?show=${
            showingNumber ?? listPerPage
          }${searchingKeyWord && `&searchKeyword=${searchingKeyWord}`}`;
      const listOfProduct = await axios.get(productApiUrl);
      setProductList(listOfProduct.data.data.data);
      setPigantionMeta(listOfProduct.data.data.meta);
    } catch (error) {
      setProductList("");
      setPigantionMeta("");
    }
  };

  const handaleSearch = (event) => {
    event.preventDefault();
    if (event.target.value.length === 0) {
      setSearchingFlag(true);
    }
    setSearchingKeyWord(event.target.value);
  };

  const handleProductShowPerPage = (event) => {
    event.preventDefault();
    setPigantionMeta("");
    getProductList(null, event.target.value);
  };

  const handlePageChange = (pageNumber) => {
    const selected = parseInt(pageNumber.selected + 1);
    getProductList(selected, listPerPage);
  };

  const postStatusProductApi = async (productId) => {
    setDisabledFiled("disabled");
    const apiProductStatusUrl = `${process.env.api_backend_url}/products/product-active-status/${productId}`;
    return await axios
      .post(apiProductStatusUrl)
      .then((apiResponse) => apiResponse.data)
      .catch((errors) => errors);
  };

  const deleteProductApi = async (productId) => {
    setDisabledFiled("disabled");
    const apiProductStatusUrl = `${process.env.api_backend_url}/products/${productId}`;
    const deleteForm = new FormData();
    deleteForm.append("_method", "DELETE");
    return await axios
      .post(apiProductStatusUrl, deleteForm)
      .then((apiResponse) => apiResponse.data)
      .catch((errors) => errors);
  };

  const deleteProduct = (event, productId, option) => {
    event.preventDefault();
    const postDeleteProductApi = document.getElementById(
      "product_" + productId
    );
    const productStatusUpdate = document.getElementById("status_" + productId);
    const changeButtonName = document.getElementById(`pubUnpub${productId}`);
    const changeButtonIcon = document.getElementById(
      `pubUnpubIcon${productId}`
    );

    if (option === "remove") {
      deleteProductApi(productId).then((returnedData) => {
        if (returnedData.success) {
          setSuccessMessage({
            ...successMessage,
            status: true,
            message: returnedData.message,
          });
          // setTimeout(
          //   () =>
          //     setSuccessMessage({
          //       ...successMessage,
          //       status: false,
          //       message: "",
          //     }),
          //   5000
          // );
          setDisabledFiled("");
          postDeleteProductApi.remove();
        } else {
          setErrorMessage({
            ...errorMessage,
            status: true,
            message: returnedData.message,
          });
          // setTimeout(
          //   () =>
          //     setErrorMessage({ ...errorMessage, status: false, message: "" }),
          //   5000
          // );
          setDisabledFiled("");
        }
      });
    } else if (option === "status") {
      postStatusProductApi(productId).then((returnedData) => {
        if (returnedData.success) {
          setSuccessMessage({
            ...successMessage,
            status: true,
            message: returnedData.message,
          });
          // setTimeout(
          //   () =>
          //     setSuccessMessage({
          //       ...successMessage,
          //       status: false,
          //       message: "",
          //     }),
          //   5000
          // );
          setDisabledFiled("");
          productStatusUpdate.classList.add(
            productStatusUpdate.classList[2] === "text-danger"
              ? "text-success"
              : "text-danger"
          );
          productStatusUpdate.classList.remove(
            productStatusUpdate.classList[2].toString()
          );

          changeButtonName.textContent = returnedData.data
            ? "Unpublish"
            : "Publish";
          changeButtonIcon.className = returnedData.data
            ? "icon-sphere text-danger"
            : "icon-sphere text-success";
        } else {
          setErrorMessage({
            ...errorMessage,
            status: true,
            message: returnedData.message,
          });
          // setTimeout(
          //   () =>
          //     setErrorMessage({ ...errorMessage, status: false, message: "" }),
          //   5000
          // );
          // setDisabledFiled("");
        }
      });
    }
  };

  return (
    <>
      {successMessage.status && (
        <MySucces
          message={successMessage.message}
          resetSuccesMessage={resetSuccesMessage}
        />
      )}

      {errorMessage.status && (
        <MyError
          message={errorMessage.message}
          resetErrorMessage={resetErrorMessage}
        />
      )}

      <section className="custom-card">
        <Loader loaded={!!productList} options={options} className="spinner">
          <div className="container-fluid">
            <div className="row">
              <div className="col-md-12">
                <div className="custom-card-box">
                  <div className="custom-card-box-header">
                    <div className="fbh-title">
                      <i className="icon-list2" />
                      <h2>Product List</h2>
                      <Link href={`/admin/product/add`}>
                        <a className="fbh-btn" title="Add New Product">
                          <i className="icon-plus-square-regular" />
                        </a>
                      </Link>
                    </div>
                  </div>
                  <div className="custom-card-box-content">
                    <div className="row table-top-option mb-3">
                      <div className="col-sm-6">
                        <div className="showing-table-row">
                          <label className="">Show</label>
                          <select
                            className="custom-select"
                            defaultValue={10}
                            onChange={handleProductShowPerPage}
                          >
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                          </select>
                        </div>
                      </div>
                      <div className="col-sm-6">
                        <div className="table-search">
                          <label className="">Search</label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="search product by catgeory/brand/prduct name"
                            onChange={handaleSearch}
                            title="search product by catgeory/brand/prduct name"
                            style={{ width: "300px" }}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-12">
                        <div className="table-responsive custom-table">
                          <fieldset disabled={disabledFiled}>
                            <table className="table table-bordered table-hover table-striped">
                              <thead className="thead-light">
                                <tr>
                                  <th scope="col">SL</th>
                                  <th scope="col">Image</th>
                                  <th scope="col">Name</th>
                                  <th scope="col">Brand</th>
                                  <th scope="col">Purchase unit</th>
                                  <th scope="col">Sell unit</th>
                                  <th scope="col">Sell price</th>
                                  <th scope="col">Purchase quantity</th>
                                  <th scope="col">Sell quantity</th>
                                  <th scope="col">Current stock</th>
                                  <th scope="col">In stock</th>
                                  <th scope="col">Status</th>
                                  <th scope="col">Action</th>
                                </tr>
                              </thead>
                              <tbody>
                                {productList &&
                                  productList.map((product, index) => (
                                    <tr
                                      key={product.id}
                                      id={`product_${product.id}`}
                                      className={
                                        product.stock <= 2 ? "bg-warning" : ""
                                      }
                                    >
                                      <td scope="row">
                                        {pigantionMeta.from
                                          ? index + pigantionMeta.from
                                          : ""}
                                      </td>
                                      <td>
                                        <img
                                          src={product.feature_image}
                                          alt={product.name}
                                        />
                                      </td>
                                      <td className="nowrap">{product.name}</td>
                                      <td className="nowrap">
                                        {product.brand.name}
                                      </td>
                                      <td className="nowrap">
                                        {product.purchase_unit.name}
                                      </td>
                                      <td className="nowrap">
                                        {product.sell_unit.name}
                                      </td>
                                      <td className="nowrap">
                                        {product.selling_price}
                                      </td>
                                      <td className="nowrap">
                                        {product.purchase_qty}
                                      </td>
                                      <td className="nowrap">
                                        {product.selling_qty}
                                      </td>
                                      <td className="nowrap">
                                        {product.stock}
                                      </td>
                                      <td className="text-center">
                                        <i
                                          className={
                                            product.in_stock
                                              ? `icon icon-check-circle-solid text-success`
                                              : `icon icon-check-circle-solid text-danger`
                                          }
                                        />
                                      </td>
                                      <td className="text-center">
                                        <i
                                          className={
                                            product.active
                                              ? `icon icon-check-circle-solid text-success`
                                              : `icon icon-check-circle-solid text-danger`
                                          }
                                          id={`status_${product.id}`}
                                        />
                                      </td>
                                      <td className="text-center">
                                        <div className="dropdown action-btn-group">
                                          <button
                                            className="btn btn-outline-primary rounded-0 btn-sm dropdown-toggle"
                                            type="button"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"
                                          >
                                            Actions
                                          </button>
                                          <div className="dropdown-menu">
                                            <Link
                                              href={`product/edit/?id=${product.id}`}
                                            >
                                              <a className="dropdown-item">
                                                <i className="icon-pencil-alt text-primary" />{" "}
                                                Edit
                                              </a>
                                            </Link>

                                            {/*<a className="dropdown-item" href="#">*/}
                                            {/*  <i className="icon-image" /> View*/}
                                            {/*</a>*/}

                                            <a
                                              className="dropdown-item"
                                              href="#"
                                              onClick={(event) => {
                                                deleteProduct(
                                                  event,
                                                  product.id,
                                                  "remove"
                                                );
                                              }}
                                            >
                                              <i className="icon-trash-alt-regular text-danger" />{" "}
                                              Delete
                                            </a>
                                            <div className="dropdown-divider" />
                                            <a
                                              className="dropdown-item"
                                              href="#"
                                              onClick={(event) => {
                                                deleteProduct(
                                                  event,
                                                  product.id,
                                                  "status"
                                                );
                                              }}
                                            >
                                              <i
                                                id={`pubUnpubIcon${product.id}`}
                                                className={
                                                  product.active
                                                    ? `icon-sphere text-danger`
                                                    : `icon-sphere text-success`
                                                }
                                              />
                                              <span
                                                id={`pubUnpub${product.id}`}
                                              >
                                                {product.active
                                                  ? "Unpublish"
                                                  : "Publish"}
                                              </span>
                                            </a>
                                          </div>
                                        </div>
                                      </td>
                                    </tr>
                                  ))}
                              </tbody>
                            </table>
                          </fieldset>
                        </div>
                      </div>
                    </div>
                  </div>
                  {pigantionMeta && (
                    <nav
                      aria-label="Page navigation example"
                      className="d-flex justify-content-end mr-4"
                    >
                      <ReactPaginate
                        previousLabel={"previous"}
                        nextLabel={"next"}
                        breakLabel={"..."}
                        pageCount={pigantionMeta.last_page}
                        marginPagesDisplayed={2}
                        pageRangeDisplayed={2}
                        onPageChange={handlePageChange}
                        containerClassName={"pagination"}
                        pageClassName={"page-item"}
                        pageLinkClassName={"page-link"}
                        activeClassName={"active"}
                        previousClassName={"page-item"}
                        previousLinkClassName={"page-link"}
                        nextClassName={"page-item"}
                        nextLinkClassName={"page-link"}
                        breakClassName={"page-item"}
                        breakLinkClassName={"page-link"}
                      />
                    </nav>
                  )}
                </div>
              </div>
            </div>
          </div>
        </Loader>
      </section>
    </>
  );
};

export default ProductTable;
