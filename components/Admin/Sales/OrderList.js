import React, {useEffect, useState} from 'react';
import {useRouter} from "next/router";
import axios from "axios";
import {connect} from "react-redux";
import {SET_SALE, SET_SALE_LIST} from "../../../store/admin/actions/sale/saleListAction";
import ReactPaginate from "react-paginate";
import Loader from "react-loader";
import {options} from "../../../Helpers/Utils/Utils";


const OrderList = (props) => {

    const router = useRouter();

    let saleType = router.pathname.includes('pos_sale') ? 'POS' : 'ECOM';

    const [show, setShow] = useState(10);
    const [keywords, setKeywords] = useState('');
    const [meta, setMeta] = useState('');
    const [page, setPage] = useState(0);

    useEffect(() => {

        let mounted = true;
        fetchedSales().then((response) => {
            if (mounted) {
                setMeta(response.meta);
                props.setSaleList(response.data);
            }
        }).finally(() => {
            mounted = false;
        })
    }, [show, keywords, page]);


    const fetchedSales = async () => {

        let url = `backend/sales?page=${page}&show=${show}&keywords=${keywords}&saleType=${saleType}`;

        return await axios.get(url)
            .then((result) => result.data.data).catch((error) => error);
    }


    return (
        <>
            <div className="col-md-7 mb-3">
                <Loader loaded={!!meta} options={options} className="spinner">
                    <div className="custom-card-box">
                    <div className="custom-card-box-header">
                        <div className="fbh-title">
                            <i className="icon-list2"/>
                            <h2>Order List</h2>
                        </div>
                    </div>

                    <div className="custom-card-box-content p-md-3 p-2">
                        <div className="row table-top-option mb-md-3 mb-2">
                            <div className="col-6">
                                <div className="showing-table-row">
                                    <label className="">Show</label>
                                    <select
                                        className="custom-select"
                                        defaultValue={`10`}
                                        onChange={(e) => {
                                            setMeta('');
                                            setShow(e.target.value);
                                            setPage(1);
                                        }}>
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                            </div>
                            <div className="col-6">
                                <div className="table-search">
                                    <label className="">Search</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        placeholder="Search here ..."
                                        onChange={(e) => {
                                            setTimeout(() => {
                                                setMeta('');
                                                setKeywords(e.target.value);
                                                setPage(1);
                                            }, 500);
                                        }}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="table-responsive custom-table custom-hight-500">
                                    <table className="table table-bordered table-hover mb-0 carsour-pointer">
                                        <thead className="thead-light">
                                        <tr>
                                            <th scope="col">SL</th>
                                            <th scope="col">Invoice</th>
                                            <th scope="col">Total</th>
                                            <th scope="col">Payment Status</th>
                                            <th scope="col">Sale Type</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            props.saleList.length > 0 ? (props.saleList.map((item, index) =>
                                                (
                                                    <tr
                                                        key={item.id}
                                                        onClick={(e) => {
                                                            e.preventDefault();
                                                            props.setSale(item);
                                                        }}
                                                        className={`text-center ${(props.sale ? props.sale.id === item.id : '') ? 'bg-warning' : ''}`}>
                                                        <td className="nowrap text-center ">{meta.from ? index + meta.from : ''}</td>

                                                        <td className="nowrap text-center">
                                                            {item.order_number}
                                                        </td>

                                                        <td className="text-center text-nowrap">
                                                            ৳ {item.grand_total}
                                                        </td>

                                                        <td className="text-center">
                                                            {item.payment_status}
                                                        </td>
                                                        <td className="text-center">
                                                            {item.order_status}
                                                        </td>

                                                    </tr>
                                                )
                                            )) : (
                                                <tr>
                                                    <td colSpan="5" className="text-center">No Order Found</td>
                                                </tr>
                                            )
                                        }

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    {meta && (
                        <nav
                            aria-label="Page navigation example"
                            className="d-flex justify-content-center">
                            <ReactPaginate
                                previousLabel={"Previous"}
                                nextLabel={"Next"}
                                breakLabel={"..."}
                                pageCount={meta.last_page}
                                marginPagesDisplayed={3}
                                pageRangeDisplayed={3}
                                onPageChange={(pageNumber) => {
                                    setPage((pageNumber.selected + 1))
                                }}
                                containerClassName={"pagination"}
                                pageClassName={"page-item"}
                                pageLinkClassName={"page-link"}
                                activeClassName={"active"}
                                previousClassName={"page-item"}
                                previousLinkClassName={"page-link"}
                                nextClassName={"page-item"}
                                nextLinkClassName={"page-link"}
                                breakClassName={"page-item"}
                                breakLinkClassName={"page-link"}/>
                        </nav>
                    )}

                </div>
                </Loader>
            </div>
        </>
    );
};

const mapDispatchToProps = (dispatch) => {
    return {
        setSaleList: (saleList) => dispatch({type: SET_SALE_LIST, payload: {saleList}}),
        setSale: (sale) => dispatch({type: SET_SALE, payload: {sale}})
    }
}

const mapStateToProps = (store) => {

    const {saleList, sale} = store.saleListStore;

    return {saleList, sale};
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderList);