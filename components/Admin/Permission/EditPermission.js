import React, { useState, useEffect } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import process from "../../../next.config";
import { editPermissionApiAllRequest } from "./Utils/Utils";
import Link from "next/link";
import TextInput from "./Form/TextInput";
import CheckBox from "./Form/CheckBox";
import TextArea from "./Form/TextArea";
import SelectOption from "./Form/SelectOption";
import Success from "../Notification/Success";
import Error from "../Notification/Error";

const EditPermission = () => {
  const permissionUrlId = useRouter();

  const [permission, setPermission] = useState({
    name: "",
    display_name: "",
    description: "",
    permission_type: "",
    module_id: "",
    active: "1",
  });

  const [permissionError, setPermissionError] = useState({
    name: "",
    name_error: "",

    display_name: "",
    display_name_error: "",

    description: "",
    description_error: "",

    permission_type: "",
    permission_type_error: "",

    module_id: "",
    module_id_error: "",
  });

  const [disabledFiled, setDisabledFiled] = useState("");

  const [moduleOptions, setModuleOptions] = useState("");

  const permissionsType = [
    { value: "add", label: "Add" },
    { value: "edit", label: "Edit" },
    { value: "delete", label: "Delete" },
    { value: "view", label: "View" },
    { value: "miscellaneous", label: "Miscellaneous" },
  ];

  const [successMessage, setSuccessMessage] = useState({
    status: false,
    message: "",
  });

  const [errorMessage, setErrorMessage] = useState({
    status: false,
    message: "",
  });

  const inputHandler = (event) => {
    const name = event.target.name;
    let value = event.target.value;
    if (name === "active") {
      value = event.target.checked ? "1" : "0";
    }
    setPermission({ ...permission, [name]: value });
    setPermissionError({
      ...permissionError,
      [name]: "",
      [name + "_error"]: "",
    });
  };

  const resetFormData = async (event) => {
    event.preventDefault();
    document.getElementById("permissionFormData").reset();
    setPermission({
      ...permission,
      name: "",
      display_name: "",
      description: "",
      permission_type: "",
      module_id: "",
      active: "1",
    });

    setPermissionError({
      ...permissionError,
      name: "",
      name_error: "",

      display_name: "",
      display_name_error: "",

      description: "",
      description_error: "",

      permission_type: "",
      permission_type_error: "",

      module_id: "",
      module_id_error: "",
    });
  };

  const submitHandler = async (event) => {
    event.preventDefault();
    const checkRequiredFiled =
      !permission.name ||
      !permission.permission_type ||
      !permission.module_id ||
      !permission.display_name;
    if (checkRequiredFiled) {
      setPermissionError((permissionError) => ({
        ...permissionError,
        name: !permission.name ? "is-invalid" : "",
        name_error: !permission.name ? "Name filed is required" : "",

        display_name: !permission.display_name ? "is-invalid" : "",
        display_name_error: !permission.display_name
          ? "Permission display name filed is required"
          : "",

        permission_type: !permission.permission_type ? "is-invalid" : "",
        permission_type_error: !permission.permission_type
          ? "Permission option filed is required"
          : "",

        module_id: !permission.module_id ? "is-invalid" : "",
        module_id_error: !permission.module_id
          ? "Module option filed is required"
          : "",
      }));
    } else {
      setDisabledFiled("disabled");

      const permissionsData = new FormData();
      permission.name && permissionsData.append("name", permission.name);
      permission.display_name &&
        permissionsData.append("display_name", permission.display_name);
      permission.description &&
        permissionsData.append("description", permission.description);
      permission.module_id &&
        permissionsData.append("module_id", permission.module_id);
      permission.permission_type &&
        permissionsData.append("permission_type", permission.permission_type);
      permission.active && permissionsData.append("active", permission.active);
      permissionsData.append("_method", "PUT");

      const permissionApiUrl = `${process.env.api_backend_url}/permissions/${permissionUrlId.query.edit}`;
      const config = { headers: { "content-type": "application/json" } };

      await axios
        .post(permissionApiUrl, permissionsData, config)
        .then((responses) => {
          if (responses.data.success) {
            setTimeout(
              () =>
                setSuccessMessage({
                  ...successMessage,
                  status: false,
                  message: "",
                }),
              5000
            );
            setSuccessMessage({
              ...successMessage,
              status: true,
              message: responses.data.message,
            });
          } else {
            Object.keys(responses.data.errors).map((name) => {
              setPermissionError((permissionError) => ({
                ...permissionError,
                [name]: "is-invalid",
                [name + "_error"]: responses.data.errors[name].join(),
              }));
            });

            setErrorMessage({
              ...errorMessage,
              status: true,
              message: responses.data.message,
            });
            setTimeout(
              () =>
                setErrorMessage({
                  ...errorMessage,
                  status: false,
                  message: "",
                }),
              5000
            );
          }
        })
        .catch((errors) => errors)
        .finally(() => {
          setTimeout(() => setDisabledFiled(""), 4000);
        });
    }
  };

  useEffect(() => {
    let mounted = true;

    const abrotCtl = new AbortController();

    if (permissionUrlId.asPath !== permissionUrlId.route) {
      editPermissionApiAllRequest(permissionUrlId.query.edit).then(
        (responses) => {
          if (responses[0]) {
            if (responses[0].data.success) {
              let modules = responses[0].data.data.map(({ id, name }) => ({
                value: id,
                label: name,
              }));
              setModuleOptions(modules);
            } else {
              setModuleOptions("");
            }
          }

          if (responses[1]) {
            if (responses[1].data.success) {
              setPermission((permission) => ({
                ...permission,
                name: responses[1].data.data.name ?? "",
                display_name: responses[1].data.data.display_name ?? "",
                description: responses[1].data.data.description ?? "",
                permission_type: responses[1].data.data.permission_type ?? "",
                module_id: responses[1].data.data.module.id.toString() ?? "",
                active: responses[1].data.data.status === "Active" ? "1" : "0",
              }));
            } else {
              setModuleOptions("");
            }
          }
        }
      );
    }
    return () => {
      return abrotCtl.abort();
    };
  }, [permissionUrlId]);

  return (
    <>
      {successMessage.status && <Success message={successMessage.message} />}
      {errorMessage.status && <Error message={errorMessage.message} />}
      <section className="custom-card">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-12">
              <div className="custom-card-box">
                <div className="custom-card-box-header">
                  <div className="fbh-title">
                    <i className="far fa-plus-square" />
                    <h2>Add Permission</h2>
                    <Link href={`/admin/permission`}>
                      <a className="fbh-btn" title="Permission list">
                        <i className="icon-list2"></i>
                      </a>
                    </Link>
                  </div>
                  <div className="fbh-info">
                    <p>
                      Please fill in the information below. The field labels
                      marked with * are required input fields.
                    </p>
                  </div>
                </div>

                <div className="custom-card-box-content">
                  <div className="row">
                    <div className="col-lg-12">
                      <form id="permissionFormData">
                        <fieldset disabled={disabledFiled}>
                          <div className="row">
                            <div className="col-lg-6">
                              <SelectOption
                                name={`module_id`}
                                options={moduleOptions}
                                value={permission.module_id}
                                eventHandler={inputHandler}
                                required={"required"}
                                label={`Module`}
                                error={permissionError.module_id}
                                errorMessage={permissionError.module_id_error}
                              />
                            </div>

                            <div className="col-lg-6">
                              <SelectOption
                                name={`permission_type`}
                                options={permissionsType}
                                value={permission.permission_type}
                                eventHandler={inputHandler}
                                required={"required"}
                                label={`Permission type`}
                                error={permissionError.permission_type}
                                errorMessage={
                                  permissionError.permission_type_error
                                }
                              />
                            </div>

                            <div className="col-lg-6">
                              <TextInput
                                name={`name`}
                                value={permission.name}
                                eventHandler={inputHandler}
                                required={`required`}
                                label={"Name"}
                                error={permissionError.name}
                                errorMessage={permissionError.name_error}
                              />
                            </div>

                            <div className="col-lg-6">
                              <TextInput
                                name={`display_name`}
                                value={permission.display_name}
                                eventHandler={inputHandler}
                                label={"Display name"}
                                required={`required`}
                                error={permissionError.display_name}
                                errorMessage={
                                  permissionError.display_name_error
                                }
                              />
                            </div>

                            <div className="col-lg-12">
                              <TextArea
                                name={`description`}
                                row={`6`}
                                value={permission.description}
                                eventHandler={inputHandler}
                                label={"Description"}
                                error={permissionError.description}
                                errorMessage={permissionError.description_error}
                              />
                            </div>

                            <div className="col-lg-12">
                              <div className="row">
                                <div className="col-lg-6 text-left">
                                  <CheckBox
                                    name={"active"}
                                    value={permission.active === "1"}
                                    eventHandler={inputHandler}
                                    required={""}
                                    label={`Active`}
                                  />
                                </div>

                                <div className="col-lg-6 text-right">
                                  <Link href={"/admin/permission"}>
                                    <a className="btn btn-info mr-1">
                                      <i className="icon-arrow-left-solid mr-1" />
                                      back to list
                                    </a>
                                  </Link>

                                  <button
                                    type="button"
                                    className="btn btn-danger mr-1"
                                    onClick={resetFormData}
                                  >
                                    <i className="icon-reset mr-1" /> Reset
                                  </button>

                                  <button
                                    type="submit"
                                    className="btn btn-primary"
                                    onClick={submitHandler}
                                  >
                                    {disabledFiled === "disabled" ? (
                                      <>
                                        <span
                                          className="spinner-border spinner-border-sm"
                                          role="status"
                                          aria-hidden="true"
                                        />{" "}
                                        saving...
                                      </>
                                    ) : (
                                      <>
                                        <i className="icon-save-regular mr-1" />{" "}
                                        save
                                      </>
                                    )}
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </fieldset>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default EditPermission;
