import axios from "axios";
import process from "../../../../next.config";

export const moduleApi = async () => {
    const apiUrl = await `${process.env.api_backend_url}/modules`;
    return await axios.get(apiUrl).then((responses) => responses.data).catch((errors) => (errors));
}

export const editPermissionApiAllRequest = async (permissionId) => {
    const moduleApiUrl = await `${process.env.api_backend_url}/modules`;
    const permissionApiUrl = await `${process.env.api_backend_url}/permissions/${permissionId}`;

    const moduleApiRequest = await axios.get(moduleApiUrl);
    const permissionApiRequest = await axios.get(permissionApiUrl);

    return await axios.all([moduleApiRequest, permissionApiRequest])
        .then(axios.spread((...responses) => responses ))
        .catch(errors => errors)
}