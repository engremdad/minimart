import React from "react";
import Link from "next/link";

const PermissionRow = (props) => {
  const onClickPublish = async ({ id }) => {
    // console.log(event.target);
    // const permissionStatusDomNode = document.getElementById(
    //     `permission_status_${id}`
    //   ),
    //   permissionPublishLinkDomNode = document.getElementById(
    //     `permission_publish_${id}`
    //   );
    // permissionStatusDomNode.classList.add(
    //   permissionStatusDomNode.classList[2] === "text-danger"
    //     ? "text-success"
    //     : "text-danger"
    // );
    // permissionStatusDomNode.classList.remove(
    //   permissionStatusDomNode.classList[2].toString()
    // );
    // permissionPublishLinkDomNode.classList.add(
    //   permissionPublishLinkDomNode.classList[1] === "text-danger"
    //     ? "text-success"
    //     : "text-danger"
    // );
    // permissionPublishLinkDomNode.classList.remove(
    //   permissionPublishLinkDomNode.classList[1].toString()
    // );
  };

  const onClickDelete = async (index) => {
    console.log(index);
    props.permissions.splice(index, 1);
    // setPermissions(permissions);
  };

  return (
    <>
      {props.permissions &&
        props.permissions.map((permission, index) => (
          <tr key={index} id={`permission_${permission.id}`}>
            <td scope="row">{index}</td>
            <td className="nowrap">{permission.name ?? "-"}</td>
            <td className="nowrap">{permission.display_name ?? "-"}</td>
            <td className="nowrap">{permission.description ?? "-"}</td>
            <td className="nowrap">{permission.module.name ?? "-"}</td>
            <td className="nowrap">{permission.created_at ?? "-"}</td>

            <td className="text-center">
              <i
                className={
                  permission.status === "Active"
                    ? `fas fa-circle text-success`
                    : `fas fa-circle text-danger`
                }
                id={`permission_status_${permission.id}`}
              />
            </td>
            <td className="text-center">
              <div className="dropdown action-btn-group">
                <button
                  className="btn btn-outline-primary rounded-0 btn-sm dropdown-toggle"
                  type="button"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Actions
                </button>
                <div className="dropdown-menu">
                  <Link href={`permission/edit/${permission.id}`}>
                    <a className="dropdown-item">
                      <i className="icon-pencil-alt text-primary" /> Edit
                    </a>
                  </Link>

                  <a
                    key={permission.id}
                    className="dropdown-item"
                    href="#"
                    onClick={(event) => {
                      event.preventDefault();
                    }}
                  >
                    <i className="icon-image text-info" />
                    View
                  </a>

                  <a
                    className="dropdown-item"
                    href="#"
                    onClick={(event) => {
                      event.preventDefault();
                      onClickDelete(index);
                    }}
                  >
                    <i className="icon-trash-alt-regular text-danger" />
                    Delete
                  </a>
                  <div className="dropdown-divider" />
                  <a
                    className="dropdown-item"
                    href="#"
                    onClick={(event) => {
                      event.preventDefault();
                      onClickPublish(index);
                    }}
                  >
                    <i
                      className={`icon-sphere ${
                        permission.status === "Active"
                          ? "text-danger"
                          : "text-success"
                      }`}
                      id={`permission_publish_${permission.id}`}
                    />
                    {permission.status === "Active" ? "Unpublish" : "Publish"}
                  </a>
                </div>
              </div>
            </td>
          </tr>
        ))}
    </>
  );
};

export default PermissionRow;
