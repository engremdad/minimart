import React, { useState, useEffect } from "react";
import axios from "axios";
import Link from "next/link";
import Success from "../Notification/Success";
import Error from "../Notification/Error";

const PermissionTable = () => {
  const [permissions, setPermissions] = useState(null);

  const [successMessage, setSuccessMessage] = useState({
    status: false,
    message: "",
  });

  const [errorMessage, setErrorMessage] = useState({
    status: false,
    message: "",
  });

  const [pagination, setPagination] = useState(null);

  useEffect(() => {
    let mounted = true;
    fetchPermissionsApi().then((PermissionsData) => {
      if (mounted) {
        setPermissions(PermissionsData.data.data);
        setPagination(PermissionsData.data.meta.last_page);
      }
    });
    return () => {
      mounted = false;
    };
  }, []);

  const fetchPermissionsApi = async () => {
    const permissionsApiUrl = await `${process.env.api_backend_url}/permissions`;
    return await axios
      .get(permissionsApiUrl)
      .then((apiResponse) => apiResponse.data)
      .catch((errors) => errors);
  };

  // onclick permission publish or unpublish
  const onClickPublish = async (index, permissionId) => {
    const permissionDeleteApiUrl = `${process.env.api_backend_url}/permissions/permission-active-status/${permissionId}`;
    await axios
      .post(permissionDeleteApiUrl)
      .then((apiResponses) => {
        if (apiResponses.data.success) {
          const copyOfPermissionsList = JSON.parse(JSON.stringify(permissions));
          copyOfPermissionsList[index].status = apiResponses.data.data.status;
          setPermissions(copyOfPermissionsList);
          setTimeout(
            () =>
              setSuccessMessage({
                ...successMessage,
                status: false,
                message: "",
              }),
            2000
          );
          setSuccessMessage({
            ...successMessage,
            status: true,
            message: apiResponses.data.message,
          });
        } else {
          setErrorMessage({
            ...errorMessage,
            status: true,
            message: apiResponses.data.message,
          });
          setTimeout(
            () =>
              setErrorMessage({ ...errorMessage, status: false, message: "" }),
            2000
          );
        }
      })
      .catch((errors) => {
        setErrorMessage({
          ...errorMessage,
          status: true,
          message: errors,
        });
        setTimeout(
          () =>
            setErrorMessage({ ...errorMessage, status: false, message: "" }),
          2000
        );
      });
  };

  // onclick delete permission
  const onClickDelete = async (index, permissionId) => {
    const permissionDeleteApiUrl = `${process.env.api_backend_url}/permissions/${permissionId}`;
    const config = { headers: { "content-type": "application/json" } };
    const permissionsData = new FormData();
    permissionsData.append("_method", "DELETE");

    await axios
      .post(permissionDeleteApiUrl, permissionsData, config)
      .then((apiResponses) => {
        if (apiResponses.data.success) {
          const copyOfPermissionsList = JSON.parse(JSON.stringify(permissions));
          copyOfPermissionsList.splice(index, 1);
          setPermissions(copyOfPermissionsList);
          setTimeout(
            () =>
              setSuccessMessage({
                ...successMessage,
                status: false,
                message: "",
              }),
            2000
          );
          setSuccessMessage({
            ...successMessage,
            status: true,
            message: apiResponses.data.message,
          });
        } else {
          setErrorMessage({
            ...errorMessage,
            status: true,
            message: apiResponses.data.message,
          });
          setTimeout(
            () =>
              setErrorMessage({ ...errorMessage, status: false, message: "" }),
            2000
          );
        }
      })
      .catch((errors) => {
        setErrorMessage({
          ...errorMessage,
          status: true,
          message: errors,
        });
        setTimeout(
          () =>
            setErrorMessage({ ...errorMessage, status: false, message: "" }),
          2000
        );
      });
  };

  const onclikPiganteDataFetching = async (pageNumber) => {
    const permissionsApiUrl = await `${process.env.api_backend_url}/permissions?page=${pageNumber}`;
    return await axios
      .get(permissionsApiUrl)
      .then((apiResponse) => {
        setPermissions(apiResponse.data.data.data);
        setPagination(apiResponse.data.data.meta.last_page);
      })
      .catch((errors) => errors);
  };

  return (
    <>
      {successMessage.status && <Success message={successMessage.message} />}
      {errorMessage.status && <Error message={errorMessage.message} />}
      <section className="custom-card">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-12">
              <div className="custom-card-box">
                <div className="custom-card-box-header">
                  <div className="fbh-title">
                    <i className="icon-list2" />
                    <h2>Permission List</h2>
                    <Link href={`/admin/permission/add`}>
                      <a className="fbh-btn" title="Add new permisssion">
                        <i className="icon-plus-square-regular"></i>
                      </a>
                    </Link>
                  </div>
                </div>
                <div className="custom-card-box-content">
                  <div className="row table-top-option mb-3">
                    <div className="col-sm-6">
                      <div className="showing-table-row">
                        <label className="">Show</label>
                        <select className="custom-select" defaultValue={`25`}>
                          <option value="10">10</option>
                          <option value="25">25</option>
                          <option value="50">50</option>
                          <option value="100">100</option>
                        </select>
                      </div>
                    </div>
                    <div className="col-sm-6">
                      <div className="table-search">
                        <label className="">Search</label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="type your need"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="table-responsive custom-table">
                        <table className="table table-bordered table-hover table-striped">
                          <thead className="thead-light">
                            <tr>
                              <th scope="col">SL</th>
                              <th scope="col">Name</th>
                              <th scope="col">Display Name</th>
                              <th scope="col">Description</th>
                              <th scope="col">Module name</th>
                              <th scope="col">Created at</th>
                              <th scope="col">Status</th>
                              <th scope="col">Action</th>
                            </tr>
                          </thead>
                          <tbody id="table">
                            {permissions &&
                              permissions.map((permission, index) => (
                                <tr key={index}>
                                  <td scope="row">{index + 1}</td>
                                  <td className="nowrap">
                                    {permission.name ?? "-"}
                                  </td>
                                  <td className="nowrap">
                                    {permission.display_name ?? "-"}
                                  </td>
                                  <td className="nowrap">
                                    {permission.description ?? "-"}
                                  </td>
                                  <td className="nowrap">
                                    {permission.module.name ?? "-"}
                                  </td>
                                  <td className="nowrap">
                                    {permission.created_at ?? "-"}
                                  </td>

                                  <td className="text-center">
                                    <i
                                      className={
                                        permission.status === "Active"
                                          ? `fas fa-circle text-success`
                                          : `fas fa-circle text-danger`
                                      }
                                      id={`permission_status_${permission.id}`}
                                    />
                                  </td>
                                  <td className="text-center">
                                    <div className="dropdown action-btn-group">
                                      <button
                                        className="btn btn-outline-primary rounded-0 btn-sm dropdown-toggle"
                                        type="button"
                                        data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="false"
                                      >
                                        Actions
                                      </button>
                                      <div className="dropdown-menu">
                                        <Link
                                          href={`permission/edit/${permission.id}`}
                                        >
                                          <a className="dropdown-item">
                                            <i className="icon-pencil-alt text-primary" />{" "}
                                            Edit
                                          </a>
                                        </Link>

                                        <a
                                          className="dropdown-item"
                                          href="#"
                                          onClick={(event) => {
                                            event.preventDefault();
                                          }}
                                        >
                                          <i className="icon-image text-info" />
                                          View
                                        </a>

                                        <a
                                          className="dropdown-item"
                                          href="#"
                                          onClick={(event) => {
                                            event.preventDefault();
                                            onClickDelete(index, permission.id);
                                          }}
                                        >
                                          <i className="icon-trash-alt-regular text-danger" />
                                          Delete
                                        </a>
                                        <div className="dropdown-divider" />
                                        <a
                                          className="dropdown-item"
                                          href="#"
                                          onClick={(event) => {
                                            event.preventDefault();
                                            onClickPublish(
                                              index,
                                              permission.id
                                            );
                                          }}
                                        >
                                          <i
                                            className={`icon-sphere ${
                                              permission.status === "Active"
                                                ? "text-danger"
                                                : "text-success"
                                            }`}
                                          />
                                          {permission.status === "Active"
                                            ? "Unpublish"
                                            : "Publish"}
                                        </a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                              ))}
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div className="col-lg-12">
                      <nav aria-label="Page navigation example">
                        <ul className="pagination justify-content-end">
                          <li className="page-item disabled">
                            <a
                              className="page-link"
                              href="#"
                              tabIndex="-1"
                              aria-disabled="true"
                            >
                              Previous
                            </a>
                          </li>

                          {[...Array(pagination)].map((empty, i) => (
                            <li key={++i} className="page-item">
                              <a
                                className="page-link"
                                href="#"
                                onClick={(event) => {
                                  event.preventDefault();
                                  onclikPiganteDataFetching(i);
                                }}
                              >
                                {i + 1}
                              </a>
                            </li>
                          ))}
                          <li className="page-item">
                            <a className="page-link" href="#">
                              Next
                            </a>
                          </li>
                        </ul>
                      </nav>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default PermissionTable;
