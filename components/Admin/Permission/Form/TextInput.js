import React from "react";

const TextInput = (props) => {
    const {name, value, eventHandler, required, label, error, errorMessage} = props;
    return (
        <div className="form-group">
            <label htmlFor={name}>{label}{required && <span className="text-danger"> *</span>}</label>
            <input type={'text'}
                   className={`form-control ${error}`}
                   placeholder={label.toLowerCase()}
                   id={name}
                   value={value}
                   name={name}
                   onChange={eventHandler}
                   required={required ?? ''}/>
            {error &&
            <div className="invalid-feedback">
                {errorMessage}
            </div>}
        </div>
    )
};

export default TextInput;