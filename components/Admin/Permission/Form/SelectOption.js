import React from "react";

const SelectOption = (props) => {
    const {name,value,options,eventHandler,required,label,error,errorMessage} = props;
    return (
        <div className="form-group">
            <label htmlFor={name}>{` `}{label}{required && <span className="text-danger"> *</span>}</label>
            <select className={`custom-select ${error}`}
                    id={name}
                    name={name}
                    value={value}
                    required={required}
                    onChange={eventHandler}>
                <option value="">Choose...</option>
                {options && options.map(({value,label})=>(
                    <option value={value.toString().toLowerCase()} key={value}>{label}</option>
                ))}
            </select>
            {error &&
            <div className="invalid-feedback">
                {errorMessage}
            </div>}
        </div>
    )
}

export default SelectOption;