import React from "react";

const TextArea = (props) => {
    const {name, row, value, eventHandler, required, label, error, errorMessage} = props;
    return (
        <div className="form-group">
            <label htmlFor={name}>{" "}{label}{required && <span className="text-danger"> *</span>}</label>
            <textarea
                className={`form-control ${error}`}
                placeholder={label.toLowerCase()}
                rows={row}
                id={name}
                name={name}
                value={value}
                onChange={eventHandler}
                required={required ?? ''}/>
            {error &&
            <div className="invalid-feedback">
                {errorMessage ?? ''}
            </div>}
        </div>
    )
};

export default TextArea;