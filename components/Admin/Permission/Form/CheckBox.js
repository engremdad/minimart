import React from "react";

const CheckBox = (props) => {
    const {name, value, eventHandler, required, label} = props;
    return (
        <div className="form-group">
            <div className="custom-control custom-checkbox">
                <input type={`checkbox`}
                       className={`custom-control-input`}
                       placeholder={name.toLowerCase()}
                       id={name}
                       checked={value}
                       name={name}
                       onChange={eventHandler}
                       required={required ?? ''}/>

                <label className="custom-control-label" htmlFor={name}>{` `}{label}</label>
            </div>
        </div>
    )
};

export default CheckBox;