import React, {useEffect, useState} from 'react';
import axios from "axios";
import Error from "../Notification/Error";
import Success from "../Notification/Success";
import Link from "next/link";

const AddUser = () => {

    let initialUserData = {
        first_name: '',
        last_name: '',
        username: '',
        email: '',
        phone: '',
        roles: '',
        avatar: '',
        password: '',
        password_confirmation: ''
    }

    let initialValidationErrors = {
        first_name_error: '',
        last_name_error: '',
        username_error: '',
        password_error: '',
        email_error: '',
        avatar_error: '',
        roles_error: ''
    }

    let initialMessage = {success: '', error: ''}

    const [userData, setUserData] = useState(initialUserData);

    const [userValidation, setUserValidation] = useState(initialValidationErrors);

    const [message, setMessage] = useState(initialMessage);

    const [loading, setLoading] = useState(false);

    const [avatarImage, setAvatarImage] = useState('');

    const [roles, setRoles] = useState([]);

    useEffect(() => {

        let mounted = true;

        fetchedRoles().then((response) => {
            if (mounted) {
                setRoles(response.data);
            }
        }).finally(() => {
            mounted = false;
        });

    }, []);


    const fetchedRoles = async () => {
        return await axios.get('backend/roles')
            .then((result) => result.data).catch((error) => error);
    }


    const handleUserData = (event) => {

        setLoading(false);

        let name = event.target.name;
        let value = event.target.value;

        if (name === 'avatar') {
            value = event.target.files[0];
            setAvatarImage(URL.createObjectURL(value));
        }

        setUserData({...userData, [name]: value});
        setUserValidation({...userValidation, [name + "_error"]: ""});
        setMessage(initialMessage);
    }


    const saveUserData = async (e) => {
        e.preventDefault();

        setLoading(true);

        const userFormData = new FormData();
        userFormData.append('first_name', userData.first_name);
        userFormData.append('last_name', userData.last_name);
        userFormData.append('username', userData.username);
        userFormData.append('email', userData.email);
        userFormData.append('phone', userData.phone);
        userFormData.append('roles', userData.roles);
        userFormData.append('avatar', userData.avatar);
        userFormData.append('password', userData.password);
        userFormData.append('password_confirmation', userData.password_confirmation);

        const config = {
            headers: {
                'content-type': 'multipart/form-data;'
            }
        }

        let response = await axios.post(`/backend/users`, userFormData, config)
            .then((result) => (result.data))
            .catch((errors) => (errors));

        if (response.success) {
            // Need to redirect list page
            await resetUserData(e);

            setMessage({...message, success: response.message});

        } else {
            setLoading(false);

            setMessage({...message, error: response.message});

            setUserValidation(response.errors);
        }

        setTimeout(() => {
            setMessage(initialMessage);
        }, 1000);

    }

    const resetUserData = async (e) => {
        e.preventDefault();

        setUserData(initialUserData);

        setUserValidation(initialValidationErrors);

        setAvatarImage('');

        setLoading(false);
    }


    return (
        <>
            {message.success && <Success message={message.success}/>}
            {message.error && <Error message={message.error}/>}

            <section className="custom-card">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="custom-card-box">
                                <div className="custom-card-box-header">
                                    <div className="fbh-title">
                                        <i className="far fa-plus-square"></i>
                                        <h2>Add User</h2>
                                    </div>
                                    <div className="fbh-info">
                                        <p>Please fill in the information below.
                                            The field labels marked with
                                            <span className="text-danger"> * </span>
                                            are required input fields.</p>
                                    </div>
                                </div>

                                <div className="custom-card-box-content">
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <form>

                                                <div className="row">

                                                    <div className="col-lg-6">
                                                        <div className="row">
                                                            <div className="col-md-12">
                                                                <div className="form-group">

                                                                    <label htmlFor="first_name">
                                                                        First Name
                                                                        <span className="text-danger"> * </span>
                                                                    </label>

                                                                    <input
                                                                        type="text"
                                                                        className={`form-control ${userValidation.first_name_error ? `is-invalid` : ``}`}
                                                                        id="first_name"
                                                                        name="first_name"
                                                                        value={userData.first_name}
                                                                        onChange={handleUserData}
                                                                        placeholder="First Name"
                                                                        required/>

                                                                    <div
                                                                        className={userValidation.first_name_error ? `invalid-feedback` : `d-none`}>
                                                                        {userValidation.first_name_error}
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div className="col-12">
                                                                <div className="form-group">

                                                                    <label htmlFor="phone">
                                                                        Phone Number
                                                                    </label>

                                                                    <input
                                                                        type="phone"
                                                                        className={`form-control`}
                                                                        id="phone"
                                                                        name="phone"
                                                                        value={userData.phone}
                                                                        onChange={handleUserData}
                                                                        placeholder="Phone Number"/>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="col-lg-6">

                                                        <div className="row">
                                                            <div className="col-md-12">
                                                                <div className="form-group">

                                                                    <label htmlFor="last_name">
                                                                        Last Name
                                                                        <span className="text-danger"> * </span>
                                                                    </label>

                                                                    <input
                                                                        type="text"
                                                                        className={`form-control ${userValidation.last_name_error ? `is-invalid` : ``}`}
                                                                        id="last_name"
                                                                        name="last_name"
                                                                        value={userData.last_name}
                                                                        onChange={handleUserData}
                                                                        placeholder="Last Name"
                                                                        required/>

                                                                    <div
                                                                        className={userValidation.last_name_error ? `invalid-feedback` : `d-none`}>
                                                                        {userValidation.last_name_error}
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>


                                                        <div className="row">
                                                            <div className="col-md-12">
                                                                <div className="form-group">
                                                                    <label htmlFor="roles">
                                                                        Select Role
                                                                        <span className="text-danger"> * </span>
                                                                    </label>

                                                                    <div
                                                                        className={`input-group ${userValidation.roles_error ? `is-invalid` : ``}`}>

                                                                        <select
                                                                            className="custom-select"
                                                                            id="roles"
                                                                            name="roles"
                                                                            onChange={handleUserData}
                                                                            aria-label="Role Select">
                                                                            <option value="">Select Role</option>
                                                                            {
                                                                                roles && roles.length && roles.map((role, index) => (
                                                                                    <option value={role.id}
                                                                                            key={index}>{role.name}</option>
                                                                                ))
                                                                            }
                                                                        </select>
                                                                    </div>

                                                                    <div
                                                                        className={userValidation.roles_error ? `invalid-feedback` : `d-none`}>
                                                                        {userValidation.roles_error}
                                                                    </div>

                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>


                                                </div>

                                                <div className="row">

                                                    <div className="col-lg-6">

                                                        <div className="row">

                                                            <div className="col-12">
                                                                <div className="form-group">

                                                                    <label htmlFor="username">
                                                                        Username
                                                                        <span className="text-danger"> * </span>
                                                                    </label>

                                                                    <input
                                                                        type="text"
                                                                        className={`form-control ${userValidation.username_error ? `is-invalid` : ``}`}
                                                                        id="username"
                                                                        name="username"
                                                                        value={userData.username}
                                                                        onChange={handleUserData}
                                                                        placeholder="Username"
                                                                        required/>

                                                                    <div
                                                                        className={userValidation.username_error ? `invalid-feedback` : `d-none`}>
                                                                        {userValidation.username_error}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div className="row">

                                                            <div className="col-12">
                                                                <div className="form-group">

                                                                    <label htmlFor="email">
                                                                        Email
                                                                        <span className="text-danger"> * </span>
                                                                    </label>

                                                                    <input
                                                                        type="email"
                                                                        className={`form-control ${userValidation.email_error ? `is-invalid` : ``}`}
                                                                        id="email"
                                                                        name="email"
                                                                        value={userData.email}
                                                                        onChange={handleUserData}
                                                                        placeholder="Email"
                                                                        required/>

                                                                    <div
                                                                        className={userValidation.email_error ? `invalid-feedback` : `d-none`}>
                                                                        {userValidation.email_error}
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div className="row">

                                                            <div className="col-12">
                                                                <div className="form-group">

                                                                    <label htmlFor="password">
                                                                        Password
                                                                        <span className="text-danger"> * </span>
                                                                    </label>

                                                                    <input
                                                                        type="password"
                                                                        className={`form-control ${userValidation.password_error ? `is-invalid` : ``}`}
                                                                        id="password"
                                                                        name="password"
                                                                        value={userData.password}
                                                                        onChange={handleUserData}
                                                                        placeholder="Password"
                                                                        required/>

                                                                    <div
                                                                        className={userValidation.password_error ? `invalid-feedback` : `d-none`}>
                                                                        {userValidation.password_error}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div className="row">

                                                            <div className="col-12">
                                                                <div className="form-group">

                                                                    <label htmlFor="confirm_password">
                                                                        Confirm Password
                                                                        <span className="text-danger"> * </span>
                                                                    </label>

                                                                    <input
                                                                        type="password"
                                                                        className="form-control"
                                                                        id="confirm_password"
                                                                        name="password_confirmation"
                                                                        value={userData.password_confirmation}
                                                                        onChange={handleUserData}
                                                                        placeholder="Confirm Password"
                                                                        required/>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>

                                                    <div className="col-lg-6">

                                                        <div className="row">
                                                            <div className="col-12">

                                                                <div className="card mb-3">

                                                                    <div className="card-header">
                                                                        <div className="fbh-title">
                                                                            <h6>Profile image</h6>
                                                                        </div>
                                                                    </div>

                                                                    <div className="card-body">
                                                                        <div className="form-group mt-1">
                                                                            <div className="custom-file">

                                                                                <input
                                                                                    type="file"
                                                                                    onChange={handleUserData}
                                                                                    className={`custom-file-input ${userValidation.avatar_error ? `is-invalid` : ``}`}
                                                                                    name={`avatar`}
                                                                                    id="avatar"
                                                                                />

                                                                                <label
                                                                                    className="custom-file-label cfl-left-align"
                                                                                    htmlFor="avatar">
                                                                                    Choose Image
                                                                                </label>

                                                                                <div
                                                                                    className={userValidation.avatar_error ? `invalid-feedback` : ``}>
                                                                                    {userValidation.avatar_error}
                                                                                </div>

                                                                            </div>
                                                                        </div>

                                                                        <div className="container border"
                                                                             style={{height: '180px'}}>

                                                                            {
                                                                                avatarImage &&
                                                                                <img className="mx-auto d-flex p-2"
                                                                                     id="avatar"
                                                                                     src={avatarImage}
                                                                                     style={{
                                                                                         width: '100%',
                                                                                         height: '100%'
                                                                                     }}/>}
                                                                        </div>

                                                                    </div>

                                                                </div>


                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>


                                                <div className="row">
                                                    <div className="col-lg-12 text-right">
                                                        <div className="form-group">

                                                            <Link href={'/admin/user'}>
                                                                <a className="btn btn-info mr-2">
                                                                    <i className="icon-arrow-left-solid mr-1 mr-1"/>
                                                                    Back
                                                                </a>
                                                            </Link>

                                                            <button
                                                                type="submit"
                                                                onClick={resetUserData}
                                                                className="btn btn-danger mr-2">
                                                                <i className="icon-reset mr-1"/>Reset
                                                            </button>

                                                            <button
                                                                type="submit"
                                                                onClick={saveUserData}
                                                                className={`btn btn-success ${loading ? 'disabled' : ''}`}>
                                                                <i className={loading ? `spinner-border spinner-border-sm mr-1` : `icon-save-regular mr-1`}/>
                                                                {loading ? `Saving..` : `Save`}
                                                            </button>

                                                        </div>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </>

    );
}

export default AddUser;