import React, { useEffect, useState } from "react";
import axios from "axios";
import Link from "next/link";
import { useRouter } from "next/router";
import CustomStyles from "./css/user_avatar.module.css";
import Error from "../Notification/Error";
import Success from "../Notification/Success";

const EditUser = ({ userEditData }) => {
  const [user, setUser] = useState({
    username: userEditData.username ?? "",
    last_name: userEditData.last_name ?? "",
    first_name: userEditData.first_name ?? "",
    phone: userEditData.phone ?? "",
    email: userEditData.email ?? "",
    password: "",
    roles: userEditData.roles ? userEditData.roles.id : "",
    confirm_password: "",
    avatar: userEditData.image ?? "",
    active: userEditData.status ? "1" : "0",
  });

  const [userErrors, setUserErrors] = useState({
    username: "",
    username_error: "",
    last_name: "",
    last_name_error: "",
    first_name: "",
    first_name_error: "",
    email: "",
    email_error: "",
    password: "",
    password_error: "",
    confirm_password: "",
    confirm_password_error: "",
    roles: "",
    roles_error: "",
    roles_error_error: "",
  });

  const [avatarImagePreView, setAvatarImagePreView] = useState(
    userEditData.image
  );
  const [filedDisabled, setFiledDisable] = useState("");
  const [successMessage, setSuccessMessage] = useState({
    status: false,
    message: "",
  });
  const [errorMessage, setErrorMessage] = useState({
    status: false,
    message: "",
  });

  const router = useRouter();

  useEffect(() => {
    // boostrap 4.5 file input field value preview
    const bsCustomFileInput = require("bs-custom-file-input");
    bsCustomFileInput.init();
  }, []);

  const [roles, setRoles] = useState([]);

  const fetchedRoles = async () => {
    return await axios
      .get("backend/roles")
      .then((result) => result.data)
      .catch((error) => error);
  };

  useEffect(() => {
    let mounted = true;

    fetchedRoles()
      .then((response) => {
        if (mounted) {
          setRoles(response.data);
        }
      })
      .finally(() => {
        mounted = false;
      });
  }, []);

  const handleUserData = (event) => {
    const name = event.target.name;
    let value = event.target.value;
    let errorClass = "";
    let errorMessage = "";
    name === "active" && (value = event.target.checked ? "1" : "0");
    name === "avatar" &&
      ((value = event.target.files[0]),
      setAvatarImagePreView(URL.createObjectURL(value)));
    if (name === "password") {
      if (value.length > 0 && value.length < 8) {
        errorClass = "is-invalid";
        errorMessage = "Password filed must be 8 length";
      }
    }

    if (name === "confirm_password") {
      if (value.length > 0 && value.length < 8) {
        errorClass = "is-invalid";
        errorMessage = "Confirm password filed must be 8 length";
      } else if (value.length >= 8 && user.password !== value) {
        errorClass = "is-invalid";
        errorMessage = "Confirm password filed not matched with password";
      }
    }

    setUser({ ...user, [name]: value });
    setUserErrors({
      ...userErrors,
      [name]: errorClass,
      [name + "_error"]: errorMessage,
    });
    return false;
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    let emptyFiledChecking =
      !user.username || !user.first_name || !user.last_name || !user.email;

    if (emptyFiledChecking) {
      setUserErrors({
        ...userErrors,
        username: !user.username ? "is-invalid" : "",
        username_error: !user.username ? "Username filed is required" : "",

        last_name: !user.last_name ? "is-invalid" : "",
        last_name_error: !user.last_name ? "Last name filed is required" : "",

        first_name: !user.first_name ? "is-invalid" : "",
        first_name_error: !user.first_name
          ? "First name filed is required"
          : "",

        email: !user.email ? "is-invalid" : "",
        email_error: !user.email ? "Email filed is required" : "",
      });
    } else {
      if (
        (user.password && !user.confirm_password) ||
        (!user.password && user.confirm_password) ||
        user.password !== user.confirm_password
      ) {
        let passwordErrorMessage = "";
        let confirmErrorPasswordMessage = "";
        if (user.password && !user.confirm_password) {
          confirmErrorPasswordMessage = "Confirm password filed is required";
          passwordErrorMessage = "";
        } else if (!user.password && user.confirm_password) {
          confirmErrorPasswordMessage = "";
          passwordErrorMessage = "Password filed is required";
        } else if (user.password !== user.confirm_password) {
          confirmErrorPasswordMessage = "Password not matched";
          passwordErrorMessage = "";
        }

        setUserErrors({
          ...userErrors,
          password: passwordErrorMessage ? "is-invalid" : "",
          password_error: passwordErrorMessage,

          confirm_password: confirmErrorPasswordMessage ? "is-invalid" : "",
          confirm_password_error: confirmErrorPasswordMessage,
        });
      } else {
        setFiledDisable("disabled");

        const apiUserUrl = `backend/users/${userEditData.slug}`;
        const userFormData = new FormData();
        user.username && userFormData.append("username", user.username);
        user.first_name && userFormData.append("first_name", user.first_name);
        user.last_name && userFormData.append("last_name", user.last_name);
        user.email && userFormData.append("email", user.email);
        user.password && userFormData.append("password", user.password);
        user.confirm_password &&
          userFormData.append("confirm_password", user.confirm_password);
        typeof user.avatar === "object" &&
          userFormData.append("avatar", user.avatar);
        userFormData.append("active", user.active);
        user.roles && userFormData.append("roles", user.roles);
        user.phone && userFormData.append("phone", user.phone);
        userFormData.append("_method", "PUT");

        const config = {
          headers: {
            "content-type": "multipart/form-data",
          },
        };
        await axios
          .post(apiUserUrl, userFormData, config)
          .then((apiResponses) => {
            if (apiResponses.data.success) {
              if (userEditData.slug !== apiResponses.data.data.slug) {
                router.replace(apiResponses.data.data.slug);
              }
              setSuccessMessage({
                ...successMessage,
                status: true,
                message: apiResponses.data.message,
              });
              setFiledDisable("");
              setTimeout(
                () =>
                  setSuccessMessage({
                    ...successMessage,
                    status: false,
                    message: "",
                  }),
                5000
              );
            } else {
              Object.keys(apiResponses.data.errors).map((name) => {
                setUserErrors((userErrors) => ({
                  ...userErrors,
                  [name]: "is-invalid",
                  [name + "_error"]: apiResponses.data.errors[name],
                }));
              });
              setFiledDisable("");
              setErrorMessage({
                ...errorMessage,
                status: true,
                message: apiResponses.data.message,
              });
              setTimeout(
                () =>
                  setErrorMessage({
                    ...errorMessage,
                    status: false,
                    message: "",
                  }),
                5000
              );
            }
          })
          .catch((errors) => {
            console.log(errors);
          });
      }
    }
  };

  const resetFormData = (event) => {
    event.preventDefault();
    document.getElementById("userFormData").reset();
    setUser({
      ...user,
      username: "",
      last_name: "",
      first_name: "",
      email: "",
      password: "",
      confirm_password: "",
      phone: "",
      avatar: "",
      roles: "",
    });

    setUserErrors({
      ...userErrors,
      username: "",
      username_error: "",
      last_name: "",
      last_name_error: "",
      first_name: "",
      first_name_error: "",
      email: "",
      email_error: "",
      password: "",
      password_error: "",
      confirm_password: "",
      confirm_password_error: "",
      roles_error: "",
      roles_error_error: "",
    });

    setFiledDisable("");
  };

  return (
    <>
      {successMessage.status && <Success message={successMessage.message} />}

      {errorMessage.status && <Error message={errorMessage.message} />}

      <section className="custom-card">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-12">
              <div className="custom-card-box">
                <div className="custom-card-box-header">
                  <div className="fbh-title">
                    <i className="far fa-plus-square" />
                    <h2>Edit User</h2>
                  </div>
                  <div className="fbh-info">
                    <p>
                      Please fill in the information below. The field labels
                      marked with * are required input fields.
                    </p>
                  </div>
                </div>

                <div className="custom-card-box-content">
                  <div className="row">
                    <div className="col-lg-12">
                      <form id="userFormData">
                        <fieldset disabled={filedDisabled ?? ""}>
                          <div className="row">
                            <div className="col-lg-6">
                              <div className="form-group">
                                <label htmlFor="name">
                                  {" "}
                                  First Name
                                  <span className="text-danger"> *</span>
                                </label>
                                <input
                                  type="text"
                                  className={`form-control ${userErrors.first_name}`}
                                  placeholder="first name"
                                  id="firstName"
                                  value={user.first_name}
                                  name="first_name"
                                  onChange={handleUserData}
                                />
                                {userErrors.first_name && (
                                  <div className="invalid-feedback">
                                    {userErrors.first_name_error}
                                  </div>
                                )}
                              </div>
                            </div>

                            <div className="col-lg-6">
                              <div className="form-group">
                                <label htmlFor="name">
                                  {" "}
                                  Last Name
                                  <span className="text-danger"> *</span>
                                </label>
                                <input
                                  type="text"
                                  className={`form-control ${userErrors.last_name}`}
                                  placeholder="last name"
                                  id="lastName"
                                  value={user.last_name}
                                  name="last_name"
                                  onChange={handleUserData}
                                />
                                {userErrors.last_name && (
                                  <div className="invalid-feedback">
                                    {userErrors.last_name_error}
                                  </div>
                                )}
                              </div>
                            </div>
                          </div>

                          <div className="row">
                            <div className="col-lg-6">
                              <div className="row">
                                <div className="col-lg-12">
                                  <div className="form-group">
                                    <label htmlFor="userName">
                                      {" "}
                                      Username
                                      <span className="text-danger"> *</span>
                                    </label>
                                    <input
                                      type="text"
                                      className={`form-control ${userErrors.username}`}
                                      placeholder="username"
                                      id="userName"
                                      value={user.username}
                                      name="username"
                                      onChange={handleUserData}
                                    />
                                    {userErrors.username && (
                                      <div className="invalid-feedback">
                                        {userErrors.username_error}
                                      </div>
                                    )}
                                  </div>
                                </div>
                                <div className="col-lg-12">
                                  <div className="form-group">
                                    <label htmlFor="userEmail">
                                      {" "}
                                      Email
                                      <span className="text-danger"> *</span>
                                    </label>
                                    <input
                                      type="email"
                                      className={`form-control ${userErrors.email}`}
                                      placeholder="email"
                                      id="userEmail"
                                      value={user.email}
                                      name="email"
                                      onChange={handleUserData}
                                    />
                                    {userErrors.email && (
                                      <div className="invalid-feedback">
                                        {userErrors.email_error}
                                      </div>
                                    )}
                                  </div>
                                </div>
                                <div className="col-lg-12">
                                  <div className="form-group">
                                    <label htmlFor="userPassword">
                                      {" "}
                                      Password
                                    </label>
                                    <input
                                      type="password"
                                      className={`form-control ${userErrors.password}`}
                                      placeholder="password"
                                      id="userPassword"
                                      value={user.password}
                                      name="password"
                                      onChange={handleUserData}
                                    />
                                    {userErrors.password && (
                                      <div className="invalid-feedback">
                                        {userErrors.password_error}
                                      </div>
                                    )}
                                  </div>
                                </div>
                                <div className="col-lg-12">
                                  <div className="form-group">
                                    <label htmlFor="confirmPassword">
                                      Confirm Password
                                    </label>

                                    <input
                                      type="password"
                                      className={`form-control ${userErrors.confirm_password}`}
                                      placeholder="confirm password"
                                      id="confirmPassword"
                                      value={user.confirm_password}
                                      name="confirm_password"
                                      onChange={handleUserData}
                                    />
                                    {userErrors.confirm_password && (
                                      <div className="invalid-feedback">
                                        {userErrors.confirm_password_error}
                                      </div>
                                    )}
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div className="col-lg-6">
                              <div className="row">
                                <div className="col-lg-12">
                                  <div className="form-group">
                                    <label htmlFor="phone">Phone Number</label>

                                    <input
                                      type="phone"
                                      className={`form-control`}
                                      id="phone"
                                      name="phone"
                                      value={user.phone}
                                      onChange={handleUserData}
                                      placeholder="Phone Number"
                                    />
                                  </div>
                                </div>
                                <div className="col-md-12">
                                  <div className="form-group">
                                    <label htmlFor="roles">
                                      Select Role
                                      <span className="text-danger"> * </span>
                                    </label>

                                    <div className={`input-group`}>
                                      <select
                                        className={`custom-select ${userErrors.roles_error}`}
                                        id="roles"
                                        name="roles"
                                        value={user.roles}
                                        onChange={handleUserData}
                                        aria-label="Role Select"
                                      >
                                        <option value="">Select Role</option>
                                        {roles &&
                                          roles.length &&
                                          roles.map((role, index) => (
                                            <option value={role.id} key={index}>
                                              {role.name}
                                            </option>
                                          ))}
                                      </select>
                                      {userErrors.roles_error && (
                                        <div className="invalid-feedback">
                                          {userErrors.roles_error_error}
                                        </div>
                                      )}
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div className="card mb-3">
                                <div className="card-header">
                                  <div className="fbh-title">
                                    <h6> Profile image</h6>
                                  </div>
                                </div>
                                <div className="card-body">
                                  <div className="form-group mt-1">
                                    <div className="custom-file">
                                      <input
                                        type="file"
                                        className={`custom-file-input`}
                                        name="avatar"
                                        id="avatar"
                                        onChange={handleUserData}
                                      />
                                      <label
                                        className="custom-file-label cfl-left-align"
                                        htmlFor="avatar"
                                      >
                                        {" "}
                                        Choose Image
                                      </label>
                                    </div>
                                  </div>

                                  <div
                                    className={`container border ${CustomStyles.avatarImage}`}
                                  >
                                    {avatarImagePreView && (
                                      <img
                                        src={avatarImagePreView}
                                        alt={user.username}
                                      />
                                    )}
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div className="row">
                            <div className="col-lg-6 text-left">
                              <div className="custom-control custom-checkbox">
                                <input
                                  type="checkbox"
                                  className="custom-control-input"
                                  defaultChecked={user.active === "1"}
                                  name="active"
                                  onChange={handleUserData}
                                  id="active"
                                />
                                <label
                                  className="custom-control-label"
                                  htmlFor="active"
                                >
                                  Active
                                </label>
                              </div>
                            </div>
                            <div className="col-lg-6 text-right">
                              <Link href={"/admin/user"}>
                                <a className="btn btn-info mr-1">
                                  <i className="icon-arrow-left-solid mr-1 mr-1" />
                                  back to list
                                </a>
                              </Link>

                              <button
                                type="button"
                                className="btn btn-danger mr-2"
                                onClick={resetFormData}
                              >
                                <i className="icon-reset mr-1" /> Reset
                              </button>

                              <button
                                type="submit"
                                className="btn btn-primary mr-2"
                                onClick={handleSubmit}
                              >
                                {filedDisabled === "disabled" ? (
                                  <>
                                    <span
                                      className="spinner-border spinner-border-sm"
                                      role="status"
                                      aria-hidden="true"
                                    />{" "}
                                    updating...
                                  </>
                                ) : (
                                  <>
                                    <i className="icon-save-regular mr-1" />{" "}
                                    update
                                  </>
                                )}
                              </button>
                            </div>
                          </div>
                        </fieldset>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default EditUser;
