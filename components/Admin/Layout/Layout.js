import Footer from "../Footer/Footer";
import React, { useEffect, useState } from "react";
import LeftSideBar from "../Sidebar/LeftSideBar";
import TopBar from "../Header/TopBar";
import Router from "next/router";
import axios from "axios";
import Storage from "../../../Helpers/Auth/Storage";
import { Provider } from "react-redux";
import adminStore from "../../../store/admin/adminStore";

const Layout = (props) => {
  // Set global token
  useEffect(() => {
    // axios.defaults.baseURL = process.env.api_url;
    // axios.defaults.headers.Authorization = `Bearer ${Storage.getToken()}`;

    if (!Storage.isLoggedIn()) {
      Router.push("/auth/login");
    }
  });

  const [toggleButton, setToggleButton] = useState(false);

  const toggleChange = () => {
    setToggleButton(!toggleButton);
  };

  return (
    <>
      <Provider store={adminStore}>
        <div className="wrapper">
          <TopBar toggleButton={toggleButton} toggleChange={toggleChange} />
          <LeftSideBar toggleChange={toggleButton} />

          <div id="content" className={toggleButton ? "active" : ""}>
            {props.children}

            <Footer></Footer>
          </div>
        </div>
      </Provider>
    </>
  );
};

export default Layout;
