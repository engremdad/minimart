import React, { useEffect, useState } from "react";
import TopBar from "../Header/TopBar";
import Router from "next/router";
import axios from "axios";
import { Provider } from "react-redux";
import posStore from "../../../store/pos/posStore";
import Storage from "../../../Helpers/Auth/Storage";

const LayoutPos = (props) => {
  // Set global token
  useEffect(() => {
    axios.defaults.baseURL = process.env.api_url;
    axios.defaults.headers.Authorization = `Bearer ${Storage.getToken()}`;

    if (!Storage.isLoggedIn()) {
      Router.push("/auth/login");
    }
  });

  const [toggleButton, setToggleButton] = useState(false);

  const toggleChange = () => {
    setToggleButton(!toggleButton);
  };

  return (
    <>
      <Provider store={posStore}>
        <div className="wrapper">
          <TopBar toggleButton={toggleButton} toggleChange={toggleChange} />

          <div id="content" className="content-pos">
            {props.children}
          </div>
        </div>
      </Provider>
    </>
  );
};

export default LayoutPos;
