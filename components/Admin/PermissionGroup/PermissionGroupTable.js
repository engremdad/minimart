import React, { useState, useEffect } from "react";
import Link from "next/link";

const PermissionGroupTable = () => {
  return (
    <>
      <section className="custom-card">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-12">
              <div className="custom-card-box">
                <div className="custom-card-box-header">
                  <div className="fbh-title">
                    <i className="icon-list2" />
                    <h2>Permission Gropu List</h2>
                    <Link href={`/admin/permissiongroup/add`}>
                      <a className="fbh-btn" title="Add new permisssion group">
                        <i className="icon-plus-square-regular"></i>
                      </a>
                    </Link>
                  </div>
                </div>
                <div className="custom-card-box-content">
                  <div className="row table-top-option mb-3">
                    <div className="col-sm-6">
                      <div className="showing-table-row">
                        <label className="">Show</label>
                        <select className="custom-select" defaultValue={`25`}>
                          <option value="10">10</option>
                          <option value="25">25</option>
                          <option value="50">50</option>
                          <option value="100">100</option>
                        </select>
                      </div>
                    </div>
                    <div className="col-sm-6">
                      <div className="table-search">
                        <label className="">Search</label>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="type your need"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="table-responsive custom-table">
                        <table className="table table-bordered table-hover table-striped">
                          <thead className="thead-light">
                            <tr>
                              <th scope="col">SL</th>
                              <th scope="col">Name</th>
                              <th scope="col">Display Name</th>
                              <th scope="col">Description</th>
                              <th scope="col">Created at</th>
                              <th scope="col">Status</th>
                              <th scope="col">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            {/* {permissions &&
                              permissions.map((permission, index) => (
                                <tr key={index}>
                                  <td scope="row">{index + 1}</td>
                                  <td className="nowrap">
                                    {permission.name ?? "-"}
                                  </td>
                                  <td className="nowrap">
                                    {permission.display_name ?? "-"}
                                  </td>
                                  <td className="nowrap">
                                    {permission.description ?? "-"}
                                  </td>
                                  <td className="nowrap">
                                    {permission.module.name ?? "-"}
                                  </td>
                                  <td className="nowrap">
                                    {permission.created_at ?? "-"}
                                  </td>

                                  <td className="text-center">
                                    <i
                                      className={
                                        permission.status === "Active"
                                          ? `fas fa-circle text-success`
                                          : `fas fa-circle text-danger`
                                      }
                                      id={`permission_status_${permission.id}`}
                                    />
                                  </td>
                                  <td className="text-center">
                                    <div className="dropdown action-btn-group">
                                      <button
                                        className="btn btn-outline-primary rounded-0 btn-sm dropdown-toggle"
                                        type="button"
                                        data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="false"
                                      >
                                        Actions
                                      </button>
                                      <div className="dropdown-menu">
                                        <Link
                                          href={`permission/edit/${permission.id}`}
                                        >
                                          <a className="dropdown-item">
                                            <i className="icon-pencil-alt text-primary" />{" "}
                                            Edit
                                          </a>
                                        </Link>

                                        <a
                                          className="dropdown-item"
                                          href="#"
                                          onClick={(event) => {
                                            event.preventDefault();
                                          }}
                                        >
                                          <i className="icon-image text-info" />
                                          View
                                        </a>

                                        <a
                                          className="dropdown-item"
                                          href="#"
                                          onClick={(event) => {
                                            event.preventDefault();
                                            onClickDelete(index, permission.id);
                                          }}
                                        >
                                          <i className="icon-trash-alt-regular text-danger" />
                                          Delete
                                        </a>
                                        <div className="dropdown-divider" />
                                        <a
                                          className="dropdown-item"
                                          href="#"
                                          onClick={(event) => {
                                            event.preventDefault();
                                            onClickPublish(
                                              index,
                                              permission.id
                                            );
                                          }}
                                        >
                                          <i
                                            className={`icon-sphere ${
                                              permission.status === "Active"
                                                ? "text-danger"
                                                : "text-success"
                                            }`}
                                          />
                                          {permission.status === "Active"
                                            ? "Unpublish"
                                            : "Publish"}
                                        </a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                              ))} */}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default PermissionGroupTable;
