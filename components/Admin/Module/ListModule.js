import React from 'react';
import {connect} from "react-redux";

const ListModule = (props) => {

    return (
        <>
            <section className="custom-card">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="custom-card-box">
                                <div className="custom-card-box-header">
                                    <div className="fbh-title">
                                        <i className="far fa-plus-square" onClick={(event => {
                                            alert('come')
                                        })}></i>
                                        <h2>Module List</h2>
                                    </div>
                                </div>

                                <div className="custom-card-box-content">

                                    <div className="row table-top-option mb-3">
                                        <div className="col-sm-6">
                                            <div className="showing-table-row">
                                                <label className="">Show</label>
                                                <select className="custom-select">
                                                    <option selected>10</option>
                                                    <option value="25">25</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div className="col-sm-6">
                                            <div className="table-search">
                                                <label className="">Search</label>
                                                <input type="text" className="form-control"
                                                       placeholder="Type your need"/>
                                            </div>
                                        </div>
                                    </div>


                                    <div className="row">
                                        <div className="col-lg-12">
                                            <div className="table-responsive custom-table">
                                                <table className="table table-bordered table-hover table-striped">

                                                    <thead className="thead-light">

                                                    <tr>
                                                        <th scope="col">SL</th>
                                                        <th scope="col">Image</th>
                                                        <th scope="col">Name</th>
                                                        <th scope="col">Status</th>
                                                        <th scope="col">Action</th>
                                                    </tr>

                                                    </thead>

                                                    <tbody>

                                                    <tr>
                                                        <td scope="row">1</td>
                                                        <td>
                                                            <h1>Image</h1>
                                                        </td>
                                                        <td className="nowrap">Module Name</td>

                                                        <td className="text-center"><i
                                                            className={`fas fa-circle text-success`}/>
                                                        </td>

                                                        <td className="text-center">
                                                            <div className="dropdown action-btn-group">
                                                                <button
                                                                    className="btn btn-outline-primary rounded-0 btn-sm dropdown-toggle"
                                                                    type="button"
                                                                    data-toggle="dropdown" aria-haspopup="true"
                                                                    aria-expanded="false">
                                                                    Actions
                                                                </button>
                                                                <div className="dropdown-menu">
                                                                    <a className="dropdown-item" href="#"><i
                                                                        className="icon-pencil-alt"/> Edit
                                                                    </a>
                                                                    <a className="dropdown-item" href="#"><i
                                                                        className="icon-image"/> View</a>
                                                                    <a className="dropdown-item" href="#"><i
                                                                        className="icon-trash-alt-regular"/> Delete</a>
                                                                    <div className="dropdown-divider"/>
                                                                    <a className="dropdown-item" href="#"><i
                                                                        className="icon-trash-alt-regular"/> publish</a>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    </tbody>

                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
};

const mapStateToProps = (store) => {
    return store;
}

export default connect(mapStateToProps)(ListModule);