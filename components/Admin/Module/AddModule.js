import React, {useState, useEffect} from 'react';
import axios from "axios";
import Success from "../Notification/Success";
import Error from "../Notification/Error";

const AddModule = () => {

    const [errors, setErrors] = useState([]);

    const [message, setMessage] = useState('');

    const [isError, setIsError] = useState(true);

    const [loading, setLoading] = useState(false);

    const [moduleData, setModuleData] = useState({
        name: '',
        description: '',
        active: false
    });

    const handleModuleData = (event) => {
        setIsError(false);
        setErrors([]);
        setMessage('');
        setLoading(false);

        const name = event.target.name;
        const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
        setModuleData({...moduleData, [name]: value});
    }

    const saveModuleData = async (e) => {
        e.preventDefault();

        setLoading(true);

        let response = await axios.post(`${process.env.api_backend_url}/modules`, moduleData).then((result) => {
            return result.data;
        }).catch((exception) => {
            return exception;
        });

        if (response.success) {
            // Need to redirect list page
            setLoading(false);
            setIsError(false);
            setMessage(response.message);
        } else {
            setLoading(false);
            setIsError(true);
            setErrors(response.errors);
            setMessage(response.message);
        }
    }

    const resetModuleData = async (e) => {
        e.preventDefault();

        setModuleData({
            name: '',
            description: '',
            active: false
        });

        setLoading(false);
        setIsError(false);
        setErrors([]);
        setMessage('');

    }




    return (
        <>
            <div className={message ? '' : 'd-none'}>
                {isError ? <Error message={message}/> : <Success message={message}/>}
            </div>

            <section className="custom-card">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="custom-card-box">
                                <div className="custom-card-box-header">
                                    <div className="fbh-title">
                                        <i className="far fa-plus-square"></i>
                                        <h2>Add Module</h2>
                                    </div>
                                    <div className="fbh-info">
                                        <p>Please fill in the information below.
                                            The field labels marked with
                                            <span className="text-danger"> * </span>
                                            are required input fields.</p>
                                    </div>
                                </div>

                                <div className="custom-card-box-content">
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <form>
                                                <div className="row">

                                                    <div className="col-lg-6">
                                                        <div className="form-group">

                                                            <label htmlFor="name">
                                                                Module Name
                                                                <span className="text-danger"> * </span>
                                                            </label>

                                                            <input
                                                                type="text"
                                                                className={`form-control ${isError && errors ? errors.name ? 'is-invalid' : '' : ''}`}
                                                                id="name"
                                                                name="name"
                                                                value={moduleData.name}
                                                                onChange={handleModuleData}
                                                                placeholder="Module Name"
                                                                required/>

                                                            <div
                                                                className={isError && errors ? errors.name ? `invalid-feedback` : `d-none` : `d-none`}>
                                                                {isError && errors ? errors.name ? errors.name : '' : ''}
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div className="col-lg-6">
                                                        <div className="form-group">

                                                            <label htmlFor="description">
                                                                Description
                                                            </label>

                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                id="description"
                                                                name="description"
                                                                value={moduleData.description}
                                                                onChange={handleModuleData}
                                                                placeholder="Description"/>
                                                        </div>
                                                    </div>

                                                </div>


                                                <div className="row">


                                                    <div className="col-lg-6">

                                                        <div className="custom-control custom-checkbox">

                                                            <input
                                                                type="checkbox"
                                                                className="custom-control-input"
                                                                checked={moduleData.active}
                                                                onChange={handleModuleData}
                                                                name="active"
                                                                id="active"/>

                                                            <label
                                                                className="custom-control-label"
                                                                htmlFor="active">
                                                                Active
                                                            </label>
                                                        </div>

                                                    </div>


                                                    <div className="col-lg-6 text-right">
                                                        <div className="form-group">

                                                            <button
                                                                type="submit"
                                                                onClick={saveModuleData}
                                                                className={`btn btn-success mr-2 ${loading ? 'disabled' : ''}`}>
                                                                <i className={loading ? `spinner-border spinner-border-sm mr-1` : `icon-save-regular mr-1`}/>
                                                                {loading ? `Saving..` : `Save`}

                                                            </button>

                                                            <button
                                                                type="submit"
                                                                onClick={resetModuleData}
                                                                className="btn btn-danger">
                                                                <i className="fas fa-redo-alt mr-1"/>Reset
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </>

    );
}
export default AddModule;