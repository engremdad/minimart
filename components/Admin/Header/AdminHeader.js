import Head from "next/head";

const AdminHeader = (props)=>{

    return(
       <>
           <Head>
               <title>{props.children ? props.children : 'Ecommerce'}</title>
           </Head>
       </>
    );
}
export default AdminHeader;
