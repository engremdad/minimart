import Router, {useRouter} from "next/router";
import {useEffect, useState} from "react";
import Link from "next/link";
import Storage from "../../../Helpers/Auth/Storage";

const LeftSideBar = (props) => {
    const router = useRouter();

    const [user, setUser] = useState("");

    const menu_active = 'menu_active';

    const logout = () => {
        Storage.clear();

        if (!Storage.isLoggedIn()) {
            Router.push("/auth/login");
        }
    };

    useEffect(() => {
        Storage.isLoggedIn() ? setUser(Storage.getUser()) : "";
    }, []);

    return (
        <>
            <nav
                id="sidebar"
                className={props.toggleChange ? "shadow-sm active" : "shadow-sm"}>
                <div className="sidebar-wrap">
                    <div className="client-login-area">
                        <a href="#">
                            <img src="/img/user.png" alt="User Login"/>
                        </a>
                        <div className="client-login-detail">
                            <p className="welcome-text">Welcome</p>

                            <h4 className="cla-name" title={user}>
                                <a href="#">{user}</a>
                            </h4>

                            <p
                                onClick={(event) => {
                                    event.preventDefault();
                                    logout();
                                }}
                                className="cla-log-in-out"
                            >
                                <a href="#">Logout</a>
                            </p>
                        </div>
                    </div>

                    <div className="accordion settings-menu" id="sidebarMainMenu">
                        <span className="section-tag">Products</span>

                        {/* Product Menu*/}
                        <div className="card">
                            <div
                                className={`card-header ${[
                                    "/admin/product",
                                    "/admin/product/add",
                                    "/admin/product/edit/[edit]",
                                    "/admin/adjustment",
                                    "/admin/adjustment/add",
                                    "/admin/product/print_barcode"
                                ].includes(router.pathname) ? menu_active : ''}`}
                                id="productMenu">

                                <h2 className="mb-0">
                                    <a
                                        className={`nav-link has-sub-menu ${[
                                            "/admin/product",
                                            "/admin/product/add",
                                            "/admin/product/edit/[edit]",
                                            "/admin/adjustment",
                                            "/admin/adjustment/add",
                                            "/admin/product/print_barcode"
                                        ].includes(router.pathname) ? '' : 'collapsed'}`}
                                        data-toggle="collapse"
                                        data-target="#product"
                                        aria-expanded={[
                                            "/admin/product",
                                            "/admin/product/add",
                                            "/admin/product/edit/[edit]",
                                            "/admin/adjustment",
                                            "/admin/adjustment/add",
                                            "/admin/product/print_barcode"
                                        ].includes(router.pathname) ? "true" : "false"}
                                        aria-controls="product"
                                        href="#">
                                        <img src="/admin_assets/images/menu_icon/baby_product.png" alt="Module Image"/>
                                        <span>Product</span>
                                    </a>
                                </h2>
                            </div>

                            <div
                                id="product"
                                className={`collapse ${[
                                    "/admin/product",
                                    "/admin/product/add",
                                    "/admin/product/edit/[edit]",
                                    "/admin/adjustment",
                                    "/admin/adjustment/add",
                                    "/admin/product/print_barcode"
                                ].includes(router.pathname) ? "show" : ""}`}
                                aria-labelledby="productMenu"
                                data-parent="#sidebarMainMenu">

                                <div className="card-body">
                                    <div
                                        className="panel-collapse collapse show"
                                        aria-expanded="true">
                                        <ul className="">
                                            <li
                                                className={`list-group-item ${
                                                    router.pathname === "/admin/product" ? menu_active : ''}`}>
                                                <Link href="/admin/product">
                                                    <a className="nav-link">Product List</a>
                                                </Link>
                                            </li>

                                            <li
                                                className={`list-group-item ${
                                                    router.pathname === "/admin/product/add" ? menu_active : ''}`}>
                                                <Link href="/admin/product/add">
                                                    <a className="nav-link">Add Product</a>
                                                </Link>
                                            </li>

                                            <li
                                                className={`list-group-item ${
                                                    router.pathname === "/admin/adjustment" ? menu_active : ''}`}>
                                                <Link href="/admin/adjustment">
                                                    <a className="nav-link">Adjustment List</a>
                                                </Link>
                                            </li>

                                            <li
                                                className={`list-group-item ${
                                                    router.pathname === "/admin/adjustment/add" ? menu_active : ''}`}>
                                                <Link href="/admin/adjustment/add">
                                                    <a className="nav-link">Add Adjustment</a>
                                                </Link>
                                            </li>

                                            <li
                                                className={`list-group-item ${
                                                    router.pathname === "/admin/product/print_barcode" ? menu_active : ''}`}>
                                                <Link href="/admin/product/print_barcode">
                                                    <a className="nav-link">Print Barcode</a>
                                                </Link>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/*End Product Menu*/}

                        {/*Third Category Menu */}
                        <div className="card">

                            <div className={`card-header ${[
                                "/admin/category",
                                "/admin/category/add"
                            ].includes(router.pathname) ? menu_active : ''}`} id="categoryMenu">

                                <h2 className="mb-0">
                                    <a
                                        className={`nav-link has-sub-menu ${[
                                            "/admin/category",
                                            "/admin/category/add"
                                        ].includes(router.pathname) ? '' : 'collapsed'}`}
                                        data-toggle="collapse"
                                        data-target="#category"
                                        aria-expanded={[
                                            "/admin/category",
                                            "/admin/category/add"
                                        ].includes(router.pathname) ? "true" : "false"}
                                        aria-controls="category"
                                        href="#">
                                        <img
                                            src="/admin_assets/images/menu_icon/category.png"
                                            alt="Food and Beverage"/>
                                        <span>Category</span>
                                    </a>
                                </h2>
                            </div>

                            <div
                                id="category"
                                className={`collapse ${[
                                    "/admin/category",
                                    "/admin/category/add"
                                ].includes(router.pathname) ? "show" : ""}`}
                                aria-labelledby="categoryMenu"
                                data-parent="#sidebarMainMenu">
                                <div className="card-body">
                                    <div
                                        className="panel-collapse collapse show"
                                        aria-expanded="true">
                                        <ul className="">
                                            <li className={`list-group-item ${
                                                router.pathname === "/admin/category" ? menu_active : ''}`}>
                                                <Link href="/admin/category">
                                                    <a className="nav-link">Category List</a>
                                                </Link>
                                            </li>
                                            <li className={`list-group-item ${
                                                router.pathname === "/admin/category/add" ? menu_active : ''}`}>
                                                <Link href="/admin/category/add">
                                                    <a className="nav-link">Add Category</a>
                                                </Link>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/*End Category Menu*/}

                        {/*Brand Menu */}
                        <div className="card">
                            <div className={`card-header ${[
                                "/admin/brand",
                                "/admin/brand/add"
                            ].includes(router.pathname) ? menu_active : ''}`} id="brandMenu">

                                <h2 className="mb-0">
                                    <a
                                        className={`nav-link has-sub-menu ${[
                                            "/admin/brand",
                                            "/admin/brand/add"
                                        ].includes(router.pathname) ? '' : 'collapsed'}`}
                                        data-toggle="collapse"
                                        data-target="#brand"
                                        aria-expanded={[
                                            "/admin/brand",
                                            "/admin/brand/add"
                                        ].includes(router.pathname) ? "true" : "false"}
                                        aria-controls="brand"
                                        href="#">
                                        <img
                                            src="/admin_assets/images/menu_icon/brand.png"
                                            alt="Food and Beverage"/>
                                        <span>Brand</span>
                                    </a>
                                </h2>
                            </div>

                            <div
                                id="brand"
                                className={`collapse ${[
                                    "/admin/brand",
                                    "/admin/brand/add"
                                ].includes(router.pathname) ? "show" : ""}`}
                                aria-labelledby="brandMenu"
                                data-parent="#sidebarMainMenu">

                                <div className="card-body">

                                    <div
                                        className="panel-collapse collapse show"
                                        aria-expanded="true">

                                        <ul className="">
                                            <li className={`list-group-item ${
                                                router.pathname === "/admin/brand" ? menu_active : ''}`}>
                                                <Link href="/admin/brand">
                                                    <a className="nav-link">Brand List</a>
                                                </Link>
                                            </li>
                                            <li className={`list-group-item ${
                                                router.pathname === "/admin/brand/add" ? menu_active : ''}`}>
                                                <Link href="/admin/brand/add">
                                                    <a className="nav-link">Add Brand</a>
                                                </Link>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/*End Brand Menu*/}

                        {/*Third Unit Menu */}
                        <div className="card">
                            <div className={`card-header ${[
                                "/admin/unit",
                                "/admin/unit/add"
                            ].includes(router.pathname) ? menu_active : ''}`} id="unitMenu">

                                <h2 className="mb-0">
                                    <a
                                        className={`nav-link has-sub-menu ${[
                                            "/admin/unit",
                                            "/admin/unit/add"
                                        ].includes(router.pathname) ? '' : 'collapsed'}`}
                                        data-toggle="collapse"
                                        data-target="#unit"
                                        aria-expanded={[
                                            "/admin/unit",
                                            "/admin/unit/add"
                                        ].includes(router.pathname) ? "true" : "false"}
                                        aria-controls="unit"
                                        href="#">
                                        <img
                                            src="/admin_assets/images/menu_icon/unit.png"
                                            alt="Food and Beverage"/>
                                        <span>Unit</span>
                                    </a>
                                </h2>
                            </div>

                            <div
                                id="unit"
                                className={`collapse ${[
                                    "/admin/unit",
                                    "/admin/unit/add"
                                ].includes(router.pathname) ? "show" : ""}`}
                                aria-labelledby="unitMenu"
                                data-parent="#sidebarMainMenu">

                                <div className="card-body">
                                    <div
                                        className="panel-collapse collapse show"
                                        aria-expanded="true">
                                        <ul className="">
                                            <li className={`list-group-item ${
                                                router.pathname === "/admin/unit" ? menu_active : ''}`}>
                                                <Link href="/admin/unit">
                                                    <a className="nav-link">Unit List</a>
                                                </Link>
                                            </li>
                                            <li className={`list-group-item ${
                                                router.pathname === "/admin/unit/add" ? menu_active : ''}`}>
                                                <Link href="/admin/unit/add">
                                                    <a className="nav-link">Add Unit</a>
                                                </Link>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/*End Unit Menu*/}

                        {/*Start Product Purchase */}
                        <div className="card">
                            <div className={`card-header ${[
                                "/admin/purchase",
                                "/admin/purchase/add"
                            ].includes(router.pathname) ? menu_active : ''}`} id="productPurchaseMenu">
                                <h2 className="mb-0">
                                    <a
                                        className={`nav-link has-sub-menu ${[
                                            "/admin/purchase",
                                            "/admin/purchase/add"
                                        ].includes(router.pathname) ? '' : 'collapsed'}`}
                                        data-toggle="collapse"
                                        data-target="#purchase"
                                        aria-expanded={[
                                            "/admin/purchase",
                                            "/admin/purchase/add"
                                        ].includes(router.pathname) ? "true" : "false"}
                                        aria-controls="purchase"
                                        href="#"
                                    >
                                        <img src="/admin_assets/images/menu_icon/purchase.png" alt="Module Image"/>
                                        <span>Purchase</span>
                                    </a>
                                </h2>
                            </div>

                            <div
                                id="purchase"
                                className={`collapse ${[
                                    "/admin/purchase",
                                    "/admin/purchase/add"
                                ].includes(router.pathname) ? "show" : ""}`}
                                aria-labelledby="productPurchaseMenu"
                                data-parent="#sidebarMainMenu">
                                <div className="card-body">
                                    <div
                                        className="panel-collapse collapse show"
                                        aria-expanded="true">
                                        <ul className="">
                                            <li className={`list-group-item ${
                                                router.pathname === "/admin/purchase" ? menu_active : ''}`}>
                                                <Link href="/admin/purchase">
                                                    <a className="nav-link">Purchase List</a>
                                                </Link>
                                            </li>
                                            <li className={`list-group-item ${
                                                router.pathname === "/admin/purchase/add" ? menu_active : ''}`}>
                                                <Link href="/admin/purchase/add">
                                                    <a className="nav-link">Add Purchase</a>
                                                </Link>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/*End Product Purchase */}

                        {/*Start Order */}
                        <div className="card">
                            <div className={`card-header ${[
                                "/admin/sales",
                                "/admin/sales/pos_sale"
                            ].includes(router.pathname) ? menu_active : ''}`} id="productSalesMenu">

                                <h2 className="mb-0">
                                    <a
                                        className={`nav-link has-sub-menu ${[
                                            "/admin/sales",
                                            "/admin/sales/pos_sale"
                                        ].includes(router.pathname) ? '' : 'collapsed'}`}
                                        data-toggle="collapse"
                                        data-target="#sales"
                                        aria-expanded={[
                                            "/admin/sales",
                                            "/admin/sales/pos_sale"
                                        ].includes(router.pathname) ? "true" : "false"}
                                        aria-controls="sales"
                                        href="#">
                                        <img src="/admin_assets/images/menu_icon/sales.png" alt="Module Image"/>
                                        <span>Sales</span>
                                    </a>
                                </h2>
                            </div>

                            <div
                                id="sales"
                                className={`collapse ${[
                                    "/admin/sales",
                                    "/admin/sales/pos_sale"
                                ].includes(router.pathname) ? "show" : ""}`}
                                aria-labelledby="productSalesMenu"
                                data-parent="#sidebarMainMenu">

                                <div className="card-body">
                                    <div
                                        className="panel-collapse collapse show"
                                        aria-expanded="true">
                                        <ul className="">
                                            {/*<li className={`list-group-item ${*/}
                                            {/*    router.pathname === "/admin/sales" ? menu_active : ''}`}>*/}
                                            {/*    <Link href="/admin/sales">*/}
                                            {/*        <a className="nav-link">Sales List</a>*/}
                                            {/*    </Link>*/}
                                            {/*</li>*/}
                                            <li className={`list-group-item ${
                                                router.pathname === "/admin/sales/pos_sale" ? menu_active : ''}`}>
                                                <Link href="/admin/sales/pos_sale">
                                                    <a className="nav-link">POS Sale</a>
                                                </Link>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/*End Order */}


                        {/*Start Return */}
                        <div className="card">
                            <div className={`card-header ${[
                                "/admin/return/sale",
                                "/admin/return/sale/add",
                                "/admin/return/purchase",
                                "/admin/return/purchase/add"
                            ].includes(router.pathname) ? menu_active : ''}`} id="productReturnMenu">

                                <h2 className="mb-0">
                                    <a
                                        className={`nav-link has-sub-menu ${[
                                            "/admin/return/sale",
                                            "/admin/return/sale/add",
                                            "/admin/return/purchase",
                                            "/admin/return/purchase/add"
                                        ].includes(router.pathname) ? '' : 'collapsed'}`}
                                        data-toggle="collapse"
                                        data-target="#productReturn"
                                        aria-expanded={[
                                            "/admin/return/sale",
                                            "/admin/return/sale/add",
                                            "/admin/return/purchase",
                                            "/admin/return/purchase/add"
                                        ].includes(router.pathname) ? "true" : "false"}
                                        aria-controls="productReturn"
                                        href="#">
                                        <img src="/admin_assets/images/menu_icon/return.png" alt="Module Image"/>
                                        <span>Return</span>
                                    </a>
                                </h2>
                            </div>

                            <div
                                id="productReturn"
                                className={`collapse ${[
                                    "/admin/return/sale",
                                    "/admin/return/sale/add",
                                    "/admin/return/purchase",
                                    "/admin/return/purchase/add"
                                ].includes(router.pathname) ? "show" : ""}`}
                                aria-labelledby="productReturnMenu"
                                data-parent="#sidebarMainMenu">

                                <div className="card-body">
                                    <div
                                        className="panel-collapse collapse show"
                                        aria-expanded="true">
                                        <ul className="">
                                            <li className={`list-group-item ${
                                                router.pathname === "/admin/return/sale" ? menu_active : ''}`}>
                                                <Link href="/admin/return/sale">
                                                    <a className="nav-link">Sales Return</a>
                                                </Link>
                                            </li>
                                            <li className={`list-group-item ${
                                                router.pathname === "/admin/return/purchase" ? menu_active : ''}`}>
                                                <Link href="/admin/return/purchase">
                                                    <a className="nav-link">Purchase Return</a>
                                                </Link>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/*End Return */}


                    </div>

                    {/*Start Settings Menu */}
                    <div className="accordion settings-menu" id="sidebarMainMenu">
                        <span className="section-tag">Settings</span>

                        {/*Start User Menu */}
                        <div className="card">
                            <div className={`card-header ${[
                                "/admin/user",
                                "/admin/user/add"
                            ].includes(router.pathname) ? menu_active : ''}`} id="userHeading">

                                <h2 className="mb-0">
                                    <a
                                        className={`nav-link has-sub-menu ${[
                                            "/admin/user",
                                            "/admin/user/add"
                                        ].includes(router.pathname) ? '' : 'collapsed'}`}
                                        data-toggle="collapse"
                                        data-target="#userItem"
                                        aria-expanded={[
                                            "/admin/user",
                                            "/admin/user/add"
                                        ].includes(router.pathname) ? "true" : "false"}
                                        aria-controls="userItem"
                                        href="#">
                                        <img
                                            src="/admin_assets/images/menu_icon/user.png"
                                            alt="Food and Beverage"/>
                                        <span>Users</span>
                                    </a>
                                </h2>
                            </div>

                            <div
                                id="userItem"
                                className={`collapse ${[
                                    "/admin/user",
                                    "/admin/user/add"
                                ].includes(router.pathname) ? "show" : ""}`}
                                aria-labelledby="userHeading"
                                data-parent="#sidebarMainMenu">

                                <div className="card-body">
                                    <div
                                        className="panel-collapse collapse show"
                                        aria-expanded="true">
                                        <ul className="">

                                            <li className={`list-group-item ${
                                                router.pathname === "/admin/user" ? menu_active : ''}`}>
                                                <Link href="/admin/user">
                                                    <a className="nav-link">User List</a>
                                                </Link>
                                            </li>

                                            <li className={`list-group-item ${
                                                router.pathname === "/admin/user/add" ? menu_active : ''}`}>
                                                <Link href="/admin/user/add">
                                                    <a className="nav-link">Add User</a>
                                                </Link>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/*End User Menu*/}

                        {/* Module Menu*/}
                        {/*<div className="card">*/}
                        {/*    <div className={`card-header ${[*/}
                        {/*        "/admin/module",*/}
                        {/*        "/admin/module/add"*/}
                        {/*    ].includes(router.pathname) ? menu_active : ''}`} id="moduleHeading">*/}

                        {/*        <h2 className="mb-0">*/}
                        {/*            <a*/}
                        {/*                className={`nav-link has-sub-menu ${[*/}
                        {/*                    "/admin/module",*/}
                        {/*                    "/admin/module/add"*/}
                        {/*                ].includes(router.pathname) ? '' : 'collapsed'}`}*/}
                        {/*                data-toggle="collapse"*/}
                        {/*                data-target="#moduleItem"*/}
                        {/*                aria-expanded={[*/}
                        {/*                    "/admin/user",*/}
                        {/*                    "/admin/user/add"*/}
                        {/*                ].includes(router.pathname) ? "true" : "false"}*/}
                        {/*                aria-controls="moduleItem"*/}
                        {/*                href="#">*/}
                        {/*                <img src="/admin_assets/images/menu_icon/modules.png" alt="Module Image"/>*/}
                        {/*                <span>Modules</span>*/}
                        {/*            </a>*/}
                        {/*        </h2>*/}
                        {/*    </div>*/}

                        {/*    <div*/}
                        {/*        id="moduleItem"*/}
                        {/*        className={`collapse ${[*/}
                        {/*            "/admin/module",*/}
                        {/*            "/admin/module/add"*/}
                        {/*        ].includes(router.pathname) ? "show" : ""}`}*/}
                        {/*        aria-labelledby="moduleHeading"*/}
                        {/*        data-parent="#sidebarMainMenu">*/}
                        {/*        <div className="card-body">*/}
                        {/*            <div*/}
                        {/*                className="panel-collapse collapse show"*/}
                        {/*                aria-expanded="true">*/}
                        {/*                <ul className="">*/}

                        {/*                    <li className={`list-group-item ${*/}
                        {/*                        router.pathname === "/admin/module" ? menu_active : ''}`}>*/}
                        {/*                        <Link href="/admin/module">*/}
                        {/*                            <a className="nav-link">Module List</a>*/}
                        {/*                        </Link>*/}
                        {/*                    </li>*/}

                        {/*                    <li className={`list-group-item ${*/}
                        {/*                        router.pathname === "/admin/module/add" ? menu_active : ''}`}>*/}
                        {/*                        <Link href="/admin/module/add">*/}
                        {/*                            <a className="nav-link">Add Module</a>*/}
                        {/*                        </Link>*/}
                        {/*                    </li>*/}
                        {/*                </ul>*/}
                        {/*            </div>*/}
                        {/*        </div>*/}
                        {/*    </div>*/}
                        {/*</div>*/}
                        {/*End First Label Menu*/}

                        {/*Start Permission Menu */}
                        {/*<div className="card">*/}
                        {/*    <div className={`card-header ${[*/}
                        {/*        "/admin/permission",*/}
                        {/*        "/admin/permission/add"*/}
                        {/*    ].includes(router.pathname) ? menu_active : ''}`} id="permission">*/}

                        {/*        <h2 className="mb-0">*/}
                        {/*            <a*/}
                        {/*                className={`nav-link has-sub-menu ${[*/}
                        {/*                    "/admin/permission",*/}
                        {/*                    "/admin/permission/add"*/}
                        {/*                ].includes(router.pathname) ? '' : 'collapsed'}`}*/}
                        {/*                data-toggle="collapse"*/}
                        {/*                data-target="#permissionItem"*/}
                        {/*                aria-expanded={[*/}
                        {/*                    "/admin/permission",*/}
                        {/*                    "/admin/permission/add"*/}
                        {/*                ].includes(router.pathname) ? "true" : "false"}*/}
                        {/*                aria-controls="permissionItem"*/}
                        {/*                href="#">*/}
                        {/*                <img src={`/admin_assets/images/menu_icon/permission.png`} alt="Permission"/>*/}
                        {/*                <span>Permissions</span>*/}
                        {/*            </a>*/}
                        {/*        </h2>*/}
                        {/*    </div>*/}

                        {/*    <div*/}
                        {/*        id="permissionItem"*/}
                        {/*        className={`collapse ${[*/}
                        {/*            "/admin/permission",*/}
                        {/*            "/admin/permission/add"*/}
                        {/*        ].includes(router.pathname) ? "show" : ""}`}*/}
                        {/*        aria-labelledby="permission"*/}
                        {/*        data-parent="#sidebarMainMenu">*/}
                        {/*        <div className="card-body">*/}
                        {/*            <div*/}
                        {/*                className="panel-collapse collapse show"*/}
                        {/*                aria-expanded="true">*/}

                        {/*                <ul className="">*/}
                        {/*                    <li className={`list-group-item ${*/}
                        {/*                        router.pathname === "/admin/permission" ? menu_active : ''}`}>*/}
                        {/*                        <Link href={"/admin/permission"}>*/}
                        {/*                            <a className="nav-link">Permission List</a>*/}
                        {/*                        </Link>*/}
                        {/*                    </li>*/}
                        {/*                    <li className={`list-group-item ${*/}
                        {/*                        router.pathname === "/admin/permission/add" ? menu_active : ''}`}>*/}
                        {/*                        <Link href={`/admin/permission/add`}>*/}
                        {/*                            <a className="nav-link">Add Permission</a>*/}
                        {/*                        </Link>*/}
                        {/*                    </li>*/}
                        {/*                </ul>*/}
                        {/*            </div>*/}
                        {/*        </div>*/}
                        {/*    </div>*/}
                        {/*</div>*/}
                        {/*End Permission Menu*/}

                        {/*Start Permission Group Menu */}
                        {/*<div className="card">*/}
                        {/*    <div className={`card-header ${[*/}
                        {/*        "/admin/permissiongroup",*/}
                        {/*        "/admin/permissiongroup/add"*/}
                        {/*    ].includes(router.pathname) ? menu_active : ''}`} id="role">*/}
                        {/*        <h2 className="mb-0">*/}

                        {/*            <a*/}
                        {/*                className={`nav-link has-sub-menu ${[*/}
                        {/*                    "/admin/permissiongroup",*/}
                        {/*                    "/admin/permissiongroup/add"*/}
                        {/*                ].includes(router.pathname) ? '' : 'collapsed'}`}*/}

                        {/*                data-toggle="collapse"*/}
                        {/*                data-target="#roleItem"*/}
                        {/*                aria-expanded={[*/}
                        {/*                    "/admin/permissiongroup",*/}
                        {/*                    "/admin/permissiongroup/add"*/}
                        {/*                ].includes(router.pathname) ? "true" : "false"}*/}
                        {/*                aria-controls="roleItem"*/}
                        {/*                href="#"*/}
                        {/*            >*/}
                        {/*                <img src={`/admin_assets/images/menu_icon/group_permission.png`} alt="Permission"/>*/}
                        {/*                <span>Permission Group</span>*/}
                        {/*            </a>*/}
                        {/*        </h2>*/}
                        {/*    </div>*/}

                        {/*    <div*/}
                        {/*        id="roleItem"*/}
                        {/*        className={`collapse ${[*/}
                        {/*            "/admin/permissiongroup",*/}
                        {/*            "/admin/permissiongroup/add"*/}
                        {/*        ].includes(router.pathname) ? "show" : ""}`}*/}
                        {/*        aria-labelledby="role"*/}
                        {/*        data-parent="#sidebarMainMenu">*/}

                        {/*        <div className="card-body">*/}
                        {/*            <div*/}
                        {/*                className="panel-collapse collapse show"*/}
                        {/*                aria-expanded="true">*/}

                        {/*                <ul className="">*/}
                        {/*                    <li className={`list-group-item ${*/}
                        {/*                        router.pathname === "/admin/permissiongroup" ? menu_active : ''}`}>*/}
                        {/*                        <Link href={"/admin/permissiongroup"}>*/}
                        {/*                            <a className="nav-link"> Permission Group List</a>*/}
                        {/*                        </Link>*/}
                        {/*                    </li>*/}

                        {/*                    <li className={`list-group-item ${*/}
                        {/*                        router.pathname === "/admin/permissiongroup/add" ? menu_active : ''}`}>*/}
                        {/*                        <Link href={`/admin/permissiongroup/add`}>*/}
                        {/*                            <a className="nav-link"> Add Permission Group</a>*/}
                        {/*                        </Link>*/}
                        {/*                    </li>*/}
                        {/*                </ul>*/}
                        {/*            </div>*/}
                        {/*        </div>*/}
                        {/*    </div>*/}
                        {/*</div>*/}
                        {/*End Permission Group Menu*/}

                        <div className="card">
                            <div className={`card-header ${[
                                "/admin/reports/lowquantity",
                                "/admin/reports/expired-products"
                            ].includes(router.pathname) ? menu_active : ''}`} id="role">
                                <h2 className="mb-0">
                                    <a
                                        className={`nav-link has-sub-menu ${[

                                            "/admin/reports/lowquantity",
                                            "/admin/reports/expired-products"
                                        ].includes(router.pathname) ? '' : 'collapsed'}`}

                                        data-toggle="collapse"
                                        data-target="#all_reports"
                                        aria-expanded={[
                                            "/admin/reports/lowquantity",
                                            "/admin/reports/expired-products"
                                        ].includes(router.pathname) ? "true" : "false"}
                                        aria-controls="all_reports"
                                        href="#"
                                    >
                                        <img src={`/admin_assets/images/menu_icon/reports.png`} alt="Reports"/>
                                        <span>Reports</span>
                                    </a>
                                </h2>
                            </div>

                            <div
                                id="all_reports"
                                className={`collapse ${[
                                    "/admin/reports/lowquantity",
                                    "/admin/reports/expired-products"
                                ].includes(router.pathname) ? "show" : ""}`}
                                aria-labelledby="role"
                                data-parent="#sidebarMainMenu">

                                <div className="card-body">
                                    <div
                                        className="panel-collapse collapse show"
                                        aria-expanded="true">

                                        <ul className="">
                                            <li className={`list-group-item ${
                                                router.pathname === "/admin/reports/lowquantity" ? menu_active : ''}`}>
                                                <Link href={"/admin/reports/lowquantity"}>
                                                    <a className="nav-link"> Low Quantity Product List</a>
                                                </Link>
                                            </li>

                                            <li className={`list-group-item ${
                                                router.pathname === "/admin/reports/expired-products" ? menu_active : ''}`}>
                                                <Link href={`/admin/reports/expired-products`}>
                                                    <a className="nav-link"> Expired Product List</a>
                                                </Link>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/*<div className="accordion settings-menu" id="sidebarMainMenu">*/}
                    {/*    <span className="section-tag">Report</span>*/}
                    {/*    <div className="card">*/}
                    {/*        <div className="card-header">*/}
                    {/*            <h2 className="mb-0">*/}
                    {/*                <a href="#" className="nav-link">*/}
                    {/*                    <img*/}
                    {/*                        src="/img/product-category/baby-products.png"*/}
                    {/*                        alt="Baby Products"*/}
                    {/*                    />*/}
                    {/*                    <span>Sales Report</span>*/}
                    {/*                </a>*/}
                    {/*            </h2>*/}
                    {/*        </div>*/}
                    {/*    </div>*/}

                    {/*    <div className="card">*/}
                    {/*        <div className="card-header">*/}
                    {/*            <h2 className="mb-0">*/}
                    {/*                <a href="#" className="nav-link">*/}
                    {/*                    <img*/}
                    {/*                        src="/img/product-category/baby-products.png"*/}
                    {/*                        alt="Baby Products"*/}
                    {/*                    />*/}
                    {/*                    <span>Daily Report</span>*/}
                    {/*                </a>*/}
                    {/*            </h2>*/}
                    {/*        </div>*/}
                    {/*    </div>*/}
                    {/*    <div className="card">*/}
                    {/*        <div className="card-header">*/}
                    {/*            <h2 className="mb-0">*/}
                    {/*                <a href="#" className="nav-link">*/}
                    {/*                    <img*/}
                    {/*                        src="/img/product-category/baby-products.png"*/}
                    {/*                        alt="Baby Products"*/}
                    {/*                    />*/}
                    {/*                    <span>Weekly Report</span>*/}
                    {/*                </a>*/}
                    {/*            </h2>*/}
                    {/*        </div>*/}
                    {/*    </div>*/}
                    {/*    <div className="card">*/}
                    {/*        <div className="card-header">*/}
                    {/*            <h2 className="mb-0">*/}
                    {/*                <a href="#" className="nav-link">*/}
                    {/*                    <img*/}
                    {/*                        src="/img/product-category/baby-products.png"*/}
                    {/*                        alt="Baby Products"*/}
                    {/*                    />*/}
                    {/*                    <span>Monthly Report</span>*/}
                    {/*                </a>*/}
                    {/*            </h2>*/}
                    {/*        </div>*/}
                    {/*    </div>*/}
                    {/*    <div className="card">*/}
                    {/*        <div className="card-header">*/}
                    {/*            <h2 className="mb-0">*/}
                    {/*                <a href="#" className="nav-link">*/}
                    {/*                    <img*/}
                    {/*                        src="/img/product-category/baby-products.png"*/}
                    {/*                        alt="Baby Products"*/}
                    {/*                    />*/}
                    {/*                    <span>Yearly Report</span>*/}
                    {/*                </a>*/}
                    {/*            </h2>*/}
                    {/*        </div>*/}
                    {/*    </div>*/}
                    {/*</div>*/}

                    {/*End of Settings menu*/}

                    {/*<div className="sd-support-policy">*/}
                    {/*    <span className="section-tag">Help & Support</span>*/}
                    {/*    <div className="sdsp-info">*/}
                    {/*        <a href="tel:+8801718659502" className="sdsp-info-box">*/}
                    {/*            <img src="/img/help-line.png" alt="Hotline"/>*/}
                    {/*            <p>*/}
                    {/*                <span className="sdsp-title">Hotline Number</span>*/}
                    {/*                <span className="sdsp-details">+880 1913 800 800</span>*/}
                    {/*            </p>*/}
                    {/*        </a>*/}
                    {/*        <a href="mailto:info@onushorit.com" className="sdsp-info-box">*/}
                    {/*            <img src="/img/email.png" alt="Hotline"/>*/}
                    {/*            <p>*/}
                    {/*                <span className="sdsp-title">Email Us</span>*/}
                    {/*                <span className="sdsp-details">support@desktopit.com.bd</span>*/}
                    {/*            </p>*/}
                    {/*        </a>*/}
                    {/*        <a href="#" className="sdsp-info-box">*/}
                    {/*            <img src="/img/power1.png" alt="DESKTOP IT"/>*/}
                    {/*            <p>*/}
                    {/*                <span className="sdsp-title">Powered By</span>*/}
                    {/*                <span className="sdsp-details">desktopit.com.bd</span>*/}
                    {/*            </p>*/}
                    {/*        </a>*/}
                    {/*    </div>*/}
                    {/*    <div className="sdsp-link">*/}
                    {/*        <span className="section-tag">Help & Support</span>*/}
                    {/*        <p>*/}
                    {/*            <a href="#">About us</a>,<a href="#">Contact us</a>,*/}
                    {/*            <a href="#">Privacy policy</a>,<a href="#">Purchase policy</a>,*/}
                    {/*            <a href="#">Cookie policy</a>,<a href="#">Terms & conditions</a>*/}
                    {/*            ,<a href="#">Returns policy</a>*/}
                    {/*        </p>*/}
                    {/*    </div>*/}
                    {/*    <div className="sdsp-copyright">*/}
                    {/*        <p>*/}
                    {/*            <i className="far fa-copyright"></i> Deskto Shop 2020*/}
                    {/*        </p>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                </div>
            </nav>
        </>
    );
};
export default LeftSideBar;
