import {applyMiddleware, createStore} from 'redux';
import posRootReducer from "./reducers/posRootReducer";
import thunk from "redux-thunk";
import {composeWithDevTools} from "redux-devtools-extension";


const posStore = createStore(posRootReducer, composeWithDevTools(applyMiddleware(thunk)));

export default posStore;