import {
    CLEAR_QUICK_CASH_NOTE,
    POS_ADD_TO_CART,
    POS_CANCEL_ORDER,
    POS_CHECKOUT,
    POS_DECREASE,
    POS_INCREASE,
    POS_REMOVE,
    POS_SET_CUSTOMER,
    SET_PAYMENT_METHOD,
    SET_PAYMENT_NOTE,
    SET_PAYMENT_STATUS,
    SET_POS_AMOUNT,
    SET_POS_DISCOUNT,
    SET_POS_SHIPPING,
    SET_QUICK_CASH_NOTE,
    SET_STAFF_NOTE
} from "../actions/posCartActions";
import PosStorage from "../../../Helpers/Pos/PosStorage";

let quick_cash_note = [
    {
        note_amount: 5,
        counter: 0
    },
    {
        note_amount: 10,
        counter: 0
    },

    {
        note_amount: 20,
        counter: 0
    },
    {
        note_amount: 50,
        counter: 0
    },

    {
        note_amount: 100,
        counter: 0
    },
    {
        note_amount: 200,
        counter: 0
    },

    {
        note_amount: 500,
        counter: 0
    },
    {
        note_amount: 1000,
        counter: 0
    },
]
let initialState = {
    cart: [],
    sale_type: 'POS',
    shop_id: 2,
    user_id: 1,
    payment_method: 'Cash',
    payment_status: 'Paid',
    staff_note: '',
    payment_note: '',
    quick_cash_amount: 0.00,
    quick_cash_note,
    pos_amount: '',
    change_return: 0,
    delivery_charge: 0,
    sub_total: 0,
    discount: 0,
    coupon_discount: 0,
    grand_total: 0
}
let temp_init_state = initialState;
let sub_total = 0;
let grand_total = 0;

let posStore = (posState) => {
    PosStorage.setCartItems(posState);
}

let getInitialState = async () => {
    if (!PosStorage.hasCartItems()) {
        PosStorage.setCartItems(initialState);
    } else {
        initialState = PosStorage.getCartItems();
    }
}

getInitialState().then(res => {
    //console.log(res);
}).catch((error) => {
    //console.log(error);
});


const posCartReducers = (state = initialState, action) => {


    sub_total = state.sub_total;
    switch (action.type) {

        case POS_SET_CUSTOMER: {

            let user_id = action.payload.user_id

            return {
                ...state,
                user_id
            }
        }

        case POS_ADD_TO_CART: {

            //if (action.payload.product.stock > 0) {
                let id = action.payload.product.id;
                let name = action.payload.product.name;
                let image = action.payload.product.feature_image;
                let unit_price = action.payload.product.selling_price;
                let stock_quantity = action.payload.product.stock;
                let weight = action.payload.product.weight;
                let sale_unit = action.payload.product.sale_unit;
                let net_discount = 0;
                let quantity = 1;
                let discount = net_discount * quantity;
                let price = unit_price * quantity - discount;

                let product = {
                    id,
                    name,
                    image,
                    unit_price,
                    net_discount,
                    discount,
                    price,
                    stock_quantity,
                    weight,
                    sale_unit,
                    quantity,

                }

                let itemExist = false;

                let tempCart = state.cart.map(cartItem => {

                    if (cartItem.id === product.id) {
                        itemExist = true;
                        if (cartItem.stock_quantity > cartItem.quantity) {
                            let quantity = cartItem.quantity + 1;
                            let net_price = cartItem.unit_price * quantity;
                            let discount = (cartItem.net_discount * quantity).toFixed(2);
                            let price = (net_price - discount).toFixed(2);

                            sub_total = sub_total + parseFloat(cartItem.unit_price - cartItem.net_discount);

                            cartItem = {...cartItem, price, discount, quantity};
                        } else {
                            alert('Out of Stock');
                            //return false;
                        }
                    }
                    return cartItem;
                });

                let temp_grand_total = sub_total + state.delivery_charge - state.discount;
                let stateData = (itemExist) ? {
                        ...state,
                        cart: tempCart,
                        sub_total,
                        grand_total: temp_grand_total,
                        quick_cash_note,
                        pos_amount: temp_grand_total,
                        change_return: 0

                    } :
                    {
                        ...state,
                        cart: [...state.cart, product],
                        sub_total: (sub_total + product.price),
                        grand_total: temp_grand_total + product.price,
                        quick_cash_note,
                        pos_amount: temp_grand_total + product.price,
                        change_return: 0
                    };

                //To store in localstorage
                posStore(stateData);

                return stateData;
            // } else {
            //     alert('Out of Stock');
            //     return stateData;
            // }

        }

        case POS_INCREASE: {

            let tempCart = state.cart.map(cartItem => {
                //sub_total =state.sub_total;
                if (cartItem.id === action.payload.id) {
                    if (cartItem.stock_quantity > cartItem.quantity) {
                        let quantity = cartItem.quantity + 1;
                        let net_price = cartItem.unit_price * quantity;
                        let discount = (cartItem.net_discount * quantity).toFixed(2);
                        let price = (net_price - discount).toFixed(2);
                        sub_total = sub_total + parseFloat(cartItem.unit_price - cartItem.net_discount);
                        //console.log(grand_total);
                        cartItem = {...cartItem, price, discount, quantity};
                    } else {
                        alert('Out of Stock');
                    }
                }
                return cartItem;
            });
            let temp_grand_total = sub_total + state.delivery_charge - state.discount;
            let stateData = {
                ...state,
                cart: tempCart,
                sub_total,
                grand_total: temp_grand_total,
                quick_cash_note,
                pos_amount: temp_grand_total,
                change_return: 0
            };

            //To store in localstorage
            posStore(stateData);

            return stateData;
        }

        case POS_DECREASE: {
            let tempCart = state.cart.map(cartItem => {
                if (cartItem.id === action.payload.id) {
                    let quantity = ((cartItem.quantity - 1) > 0) ? cartItem.quantity - 1 : 1;
                    let net_price = cartItem.unit_price * quantity;
                    let discount = (cartItem.net_discount * quantity).toFixed(2);
                    let price = (net_price - discount).toFixed(2);

                    sub_total = ((cartItem.quantity - 1) > 0) ? (sub_total - parseFloat(cartItem.unit_price - cartItem.net_discount)) : sub_total;
                    cartItem = {...cartItem, price, discount, quantity};
                }
                return cartItem;
            });
            let temp_grand_total = sub_total + state.delivery_charge - state.discount;
            let stateData = {
                ...state,
                cart: tempCart,
                sub_total,
                grand_total: temp_grand_total,
                quick_cash_note,
                pos_amount: temp_grand_total,
                change_return: 0
            };

            //To store in localstorage
            posStore(stateData);

            return stateData;
        }

        case  POS_REMOVE: {
            state.cart.map(cartItem => {
                sub_total = sub_total - ((cartItem.id === action.payload.id) ? parseFloat((cartItem.unit_price - cartItem.net_discount) * cartItem.quantity) : 0);
            });
            let temp_grand_total = sub_total + state.delivery_charge - state.discount;
            let stateData = {
                ...state,
                cart: state.cart.filter(cartItem => cartItem.id !== action.payload.id),
                sub_total,
                grand_total: temp_grand_total,
                quick_cash_note,
                pos_amount: temp_grand_total,
                change_return: 0
            };

            //To store in localstorage
            posStore(stateData);

            return stateData;
        }

        case SET_POS_SHIPPING: {

            let delivery_charge = action.payload.delivery_charge > 0 ? Math.abs(parseFloat(action.payload.delivery_charge)) : 0.00;
            let stateDate = {
                ...state,
                delivery_charge,
                grand_total: state.sub_total + delivery_charge - state.discount,
                quick_cash_note,
                pos_amount: state.sub_total + delivery_charge - state.discount,
                change_return: 0

            }
            posStore(stateDate);

            return stateDate;

        }

        case SET_POS_DISCOUNT: {


            let discount = action.payload.discount > 0 ? Math.abs(parseFloat(action.payload.discount)) : 0.00;
            let stateDate = {
                ...state,
                discount,
                grand_total: state.sub_total + state.delivery_charge - discount,
                quick_cash_note,
                pos_amount: state.sub_total + state.delivery_charge - discount,
                change_return: 0

            }
            posStore(stateDate);

            return stateDate;

        }

        case SET_POS_AMOUNT: {

            let pos_amount = action.payload.pos_amount > 0 ? Math.abs(parseFloat(action.payload.pos_amount)) : 0.00;

            let stateDate = {
                ...state,
                pos_amount: pos_amount,
                quick_cash_note,
                change_return: state.grand_total - pos_amount

            };
            posStore(stateDate);

            return stateDate;
        }

        case SET_PAYMENT_METHOD : {
            let stateDate = {
                ...state,
                payment_method: action.payload.payment_method
            }
            posStore(stateDate);

            return stateDate;
        }

        case SET_PAYMENT_STATUS : {
            let stateDate = {
                ...state,
                payment_status: action.payload.payment_status
            }
            posStore(stateDate);

            return stateDate;
        }

        case SET_STAFF_NOTE : {
            let stateDate = {
                ...state,
                staff_note: action.payload.staff_note
            }
            posStore(stateDate);

            return stateDate;
        }

        case SET_PAYMENT_NOTE : {

            let stateDate = {
                ...state,
                payment_note: action.payload.payment_note
            }

            posStore(stateDate);

            return stateDate;
        }

        case CLEAR_QUICK_CASH_NOTE: {
            let stateDate = {
                ...state,
                quick_cash_note,
                pos_amount: 0,
                change_return: 0
            }

            posStore(stateDate);

            return stateDate;

        }

        case SET_QUICK_CASH_NOTE : {

            let note_amount = action.payload.note_amount;
            let counter = 1;
            let cash_note = {
                note_amount,
                counter
            }
            let itemExist = false;
            let pos_amount = 0;
            let tempNote = state.quick_cash_note.map(quick_note => {
                pos_amount = pos_amount + (quick_note.note_amount * quick_note.counter);

                if (quick_note.note_amount === note_amount) {
                    pos_amount = pos_amount + note_amount;
                    itemExist = true;
                    quick_note = {...quick_note, counter: quick_note.counter + 1};
                }
                return quick_note;
            });

            let stateData = (itemExist) ? {
                    ...state,
                    quick_cash_note: tempNote,
                    pos_amount,
                    change_return: state.grand_total - pos_amount
                } :
                {
                    ...state,
                    quick_cash_note: [...state.quick_cash_note, cash_note],
                    pos_amount,
                    change_return: state.grand_total - pos_amount
                }

            posStore(stateData);

            return stateData;
        }

        case POS_CANCEL_ORDER : {
            // Remove PosItem From localStorage
            PosStorage.removeCartItem();

            return temp_init_state;
        }
        case POS_CHECKOUT : {
            // Remove PosItem From localStorage
            PosStorage.removeCartItem();
            //posStore(initialState);
            return temp_init_state;
        }

        default:
            return state;
    }


}

export default posCartReducers;