import {combineReducers} from "redux";
import posCartReducers from "./posCartReducers";
import posProductReducer from "./posProductReducer";

const posRootReducer = combineReducers({
    posCart: posCartReducers,
    posProduct: posProductReducer
});

export default posRootReducer;
