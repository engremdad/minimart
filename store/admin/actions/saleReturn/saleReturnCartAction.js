export const SALE_RETURN_CART = 'SALE_RETURN_CART';
export const SALE_RETURN_SET_CUSTOMER = 'SALE_RETURN_SET_CUSTOMER';
export const SALE_RETURN_ADD_TO_CART = 'SALE_RETURN_ADD_TO_CART';
export const SALE_RETURN_ADD_QUANTITY = 'SALE_RETURN_ADD_QUANTITY';
export const SALE_RETURN_REMOVE = 'SALE_RETURN_REMOVE';
export const SET_SALE_RETURN_DATE = 'SET_SALE_RETURN_DATE';
export const SET_SALE_RETURN_TOTAL_TAX = 'SET_SALE_RETURN_TOTAL_TAX';
export const SET_SALE_RETURN_TOTAL_DISCOUNT = 'SET_SALE_RETURN_TOTAL_DISCOUNT';
export const SET_SALE_RETURN_SHIPPING_COST = 'SET_SALE_RETURN_SHIPPING_COST';
export const SET_SALE_RETURN_REFERENCE_NO = 'SET_SALE_RETURN_REFERENCE_NO';
export const SET_SALE_RETURN_STATUS = 'SET_SALE_RETURN_STATUS';
export const ATTACHED_SALE_RETURN_DOCUMENT = 'ATTACHED_SALE_RETURN_DOCUMENT';
export const RESET_SALE_RETURN = 'RESET_SALE_RETURN';
export const SET_SALE_RETURN_NOTE = 'SET_SALE_RETURN_NOTE';