import {applyMiddleware, createStore} from 'redux';
import thunk from "redux-thunk";
import {composeWithDevTools} from "redux-devtools-extension";
import adminRootReducer from "./reducers/adminRootReducer";


const adminStore = createStore(adminRootReducer, composeWithDevTools(applyMiddleware(thunk)));

export default adminStore;