import {SET_SALE, SET_SALE_LIST} from "../../actions/sale/saleListAction";

let initialState = {
    saleList: '',
    sale: ''
}

const saleListReducer = (state = initialState, action) => {

    let {type, payload} = action;

    switch (type) {
        case SET_SALE_LIST: {

            let saleList = payload.saleList;

            return {
                ...state,
                saleList: saleList,
                sale: (saleList.length > 0) ? saleList[0] : ''
            }
        }

        case SET_SALE: {
            let sale = payload.sale;
            return {
                ...state,
                sale: sale
            }
        }

        default :
            return state;
    }

}

export default saleListReducer;

