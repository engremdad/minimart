import {
    ADD_DISCOUNT,
    ADD_ITEM_QUANTITY,
    ADD_SUB_TOTAL,
    ADD_UNIT_COST,
    CHECK_VALIDATE,
    EDIT_PURCHASE,
    PURCHASE_ADD_TO_CART,
    PURCHASE_REMOVE,
    RESET_PURCHASE,
    SAVE_PURCHASE,
    SET_DISCOUNT,
    SET_EXPIRED_DATE,
    SET_ORDER_TAX,
    SET_PURCHASE_DATE,
    SET_PURCHASE_NOTE,
    SET_REFERENCE_NO,
    SET_SHIPPING_COST,
    SET_STATUS,
    SET_SUPPLIER_ID,
    SET_WAREHOUSE,
    UPLOAD_DOCUMENT
} from "../../actions/purchase/purchaseCartActions";

let initialState = {
    cart: [],
    errorMessage: {
        purchaseDate: "",
        referenceNo : "",
        status : "",
        supplier : ""
    },
    edit_purchase: [],
    shop_id: 1,
    reference_no: '',
    warehouse_id: 1,
    supplier_id: '',
    sub_total: 0.00,
    total_tax: 0.00,
    total_discount: 0.00,
    shipping_cost: 0.00,
    grand_total: 0.00,
    paid_amount: 0.00,
    document: '',
    status: '',
    payment_status: '',
    note: '',
    purchase_date: new Date().toISOString().slice(0, 10),
}

const purchaseCartReducers = (state = initialState, action) => {

    switch (action.type) {

        case PURCHASE_ADD_TO_CART: {
            let id = action.payload.product.id;
            let name = action.payload.product.name;
            let code = action.payload.product.barcode;
            let image = action.payload.product.feature_image;
            let expire_date = new Date().toISOString().slice(0, 10);
            let quantity = 1;
            let unit_price = 0.00;
            let sub_total = 0.00;
            let product = {
                id,
                name,
                code,
                image,
                unit_price,
                quantity,
                expire_date,
                sub_total

            }

            let itemExist = false;

            let tempCart = state.cart.map(cartItem => {

                if (cartItem.id === product.id) {
                    itemExist = true;
                }

                return cartItem;
            });

            return (itemExist) ? {
                    ...state,
                    cart: tempCart
                } :
                {
                    ...state,
                    cart: [...state.cart, product],
                    sub_total: (state.sub_total + product.sub_total),
                    grand_total: state.sub_total + product.sub_total + state.total_tax + state.shipping_cost - state.total_discount
                };

        }

        case SET_EXPIRED_DATE: {
            let cartData = state.cart.map(cartItem => {

                if (cartItem.id === action.payload.id) {
                    let expire_date = action.payload.expire_date;
                    if(expire_date < state.purchase_date){
                        alert("your product expire date is less then purchase date ")
                        return cartItem
                    }
                    cartItem = {...cartItem, expire_date}
                }
                return cartItem
            })

            return {...state, cart: cartData};

        }

        case ADD_ITEM_QUANTITY: {
            let cartData = state.cart.map(cartItem => {
                if (cartItem.id === action.payload.id) {
                    let quantity = Math.abs(action.payload.quantity) > 0 ? Math.abs(action.payload.quantity) : 1;
                    let unit_price = (cartItem.sub_total > 0) ? (cartItem.sub_total / quantity) : 0;
                    cartItem = {...cartItem, quantity, unit_price}
                }
                return cartItem
            })


            return {
                ...state,
                cart: cartData
            };

        }

        case ADD_UNIT_COST: {
            let cartData = state.cart.map(cartItem => {

                if (cartItem.id === action.payload.id) {
                    let unit_price = action.payload.unitCost;
                    let sub_total = unit_price * cartItem.quantity;
                    cartItem = {...cartItem, unit_price, sub_total}
                }
                return cartItem
            })

            return {
                ...state,
                cart: cartData
            };

        }

        case ADD_DISCOUNT: {
            let cartData = state.cart.map(cartItem => {

                if (cartItem.id === action.payload.id) {
                    let discount = action.payload.discount;
                    let total = cartItem.sub_total - discount;
                    let unit_price = (total > 0) ? (total / cartItem.quantity) : 0;
                    cartItem = {...cartItem, unit_price, discount, total}
                }
                return cartItem
            })

            return {...state, cart: cartData};

        }

        case ADD_SUB_TOTAL: {

            let temp_sub_total = 0;

            let cartData = state.cart.map(cartItem => {

                if (cartItem.id === action.payload.id) {

                    let sub_total = Math.abs(parseFloat(action.payload.subtotal)) > 0 ? Math.abs(parseFloat(action.payload.subtotal)) : 0.00;

                    let unit_price = (sub_total > 0) ? parseFloat(sub_total / cartItem.quantity) : 0.00;

                    cartItem = {...cartItem, unit_price, sub_total}
                }

                temp_sub_total = temp_sub_total + cartItem.sub_total;


                return cartItem
            });


            return {
                ...state,
                cart: cartData,
                sub_total: temp_sub_total,
                grand_total: parseFloat(temp_sub_total) + parseFloat(state.total_tax) + parseFloat(state.shipping_cost) - parseFloat(state.total_discount)
            };

        }
        case  PURCHASE_REMOVE: {
            let sub_total=state.sub_total;
            state.cart.map(cartItem => {
                sub_total = sub_total - ((cartItem.id === action.payload.id) ? cartItem.sub_total : 0);
            });

            let stateData = {
                ...state,
                cart: state.cart.filter(cartItem => cartItem.id !== action.payload.id),
                sub_total,
                grand_total: parseFloat(sub_total) + parseFloat(state.total_tax) + parseFloat(state.shipping_cost) - parseFloat(state.total_discount)
            };
            return stateData;
        }
        case SET_PURCHASE_DATE: {
            let errorMessage = {
                ...state.errorMessage,purchaseDate:'',
            }
            return {
                ...state,
                purchase_date: action.payload.purchase_date,
                errorMessage,
            };
        }

        case SET_REFERENCE_NO: {
            let errorMessage = {
                ...state.errorMessage,referenceNo:'',
            }
            return {
                ...state,
                reference_no: action.payload.reference_no,
                errorMessage,

            };
        }

        case SET_STATUS: {
            let errorMessage = {
                ...state.errorMessage,status:'',
            }
            return {
                ...state,
                status: action.payload.status,
                errorMessage,

            };
        }

        case SET_ORDER_TAX: {

            let temp_total_tax = Math.abs(parseFloat(action.payload.order_tax)) > 0 ? Math.abs(parseFloat(action.payload.order_tax)) : 0.00;

            let temp_grand_total = temp_total_tax + parseFloat(state.sub_total) + parseFloat(state.shipping_cost) - parseFloat(state.total_discount);

            return {
                ...state,
                total_tax: temp_total_tax,
                grand_total: temp_grand_total

            };

        }

        case SET_DISCOUNT: {

            let temp_total_discount = Math.abs(parseFloat(action.payload.total_discount)) > 0 ? Math.abs(parseFloat(action.payload.total_discount)) : 0.00;

            let temp_grand_total = parseFloat(state.total_tax) + parseFloat(state.sub_total) + parseFloat(state.shipping_cost) - temp_total_discount;


            return {
                ...state,
                total_discount: temp_total_discount,
                grand_total: temp_grand_total

            };
        }

        case SET_SHIPPING_COST: {

            let temp_shipping_cost = Math.abs(parseFloat(action.payload.shipping_cost)) > 0 ? Math.abs(parseFloat(action.payload.shipping_cost)) : 0.00;

            let temp_grand_total = temp_shipping_cost + parseFloat(state.sub_total) + parseFloat(state.total_tax) - parseFloat(state.total_discount);


            return {
                ...state,
                shipping_cost: temp_shipping_cost,
                grand_total: temp_grand_total

            };
        }

        case SET_WAREHOUSE: {
            return {
                ...state,
                warehouse_id: action.payload.warehouse_id

            };
        }

        case UPLOAD_DOCUMENT: {
            return {
                ...state,
                document: action.payload.purchase_document

            };
        }

        case SET_PURCHASE_NOTE: {

            return {
                ...state,
                note: action.payload.purchase_note

            };
        }

        case SET_SUPPLIER_ID : {
            let errorMessage = {
                ...state.errorMessage,supplier:'',
            }
            return {
                ...state,
                supplier_id: action.payload.supplier_id,
                errorMessage
            }
        }

        case EDIT_PURCHASE: {
            let reference_no = action.payload.purchase.data.reference_no
            let total_tax = action.payload.purchase.data.total_tax
            let total_discount = action.payload.purchase.data.total_discount
            let purchase_date = action.payload.purchase.data.purchase_date
            let status = action.payload.purchase.data.status
            let shipping_cost = action.payload.purchase.data.shipping_cost
            let grand_total = action.payload.purchase.data.grand_total
            let supplier_id = action.payload.purchase.data.supplier_id
            let note = action.payload.purchase.data.note
            let temp_sub_total = 0



            let product = action.payload.purchase.data.product_purchases
            let cart=[]
            console.log('uzzal',action.payload.purchase.data)
            product.map( (cartItem,key) =>{
                let id = cartItem.id
                let name =cartItem.product.name
                let code = cartItem.product.barcode
                let image = cartItem.product.feature_image
                let expire_date =new Date(cartItem.expiry_date).toISOString().slice(0, 10)
                let quantity = cartItem.purchase_quantity
                let unit_price = cartItem.purchase_price
                let sub_total = cartItem.sub_total
                temp_sub_total = temp_sub_total+sub_total
                cart[key]={
                    id,
                    name,
                    code,
                    image,
                    unit_price,
                    quantity,
                    expire_date,
                    sub_total

                }
            });

            return {
                ...state,
                cart:cart,
                reference_no,
                total_tax,
                total_discount,
                purchase_date,
                status,
                shipping_cost,
                grand_total,
                supplier_id,
                note,
                sub_total:temp_sub_total,
                edit_purchase: action.payload.purchase
            }
        }

        case SAVE_PURCHASE: {
            return initialState;
        }

        case RESET_PURCHASE: {
            return initialState;
        }
        case CHECK_VALIDATE: {
            let purchaseDate= ""
            let referenceNo = ""
            let status = ""
            let supplier = ""
            if(state.purchase_date === ""){
                purchaseDate = "Purchase Date can not be null"
            }
            if(state.reference_no === ""){
                referenceNo = "Reference can not be null"
            }
            if(state.status === ""){
                status = "Please select one option "
            }
            if(state.supplier_id === ""){
                supplier = "Please select supplier "
            }

            return {
                ...state,
                errorMessage: {
                    purchaseDate:purchaseDate,
                    referenceNo : referenceNo,
                    status      : status,
                    supplier    : supplier
                }
            };
        }


        default:
            return state;
    }


}

export default purchaseCartReducers;