import {
    ADD_PURCHASE_PAYMENT,
    DELETE_PURCHASE,
    SET_PAYMENT_AMOUNT,
    SET_PAYMENT_DATE,
    SET_PAYMENT_REFERENCE_NO,
    SET_PURCHASE,
    SET_PURCHASE_LIST,
    SET_PURCHASE_PAYMENT_DOCUMENT,
    SET_PURCHASE_PAYMENT_METHOD,
    SET_PURCHASE_PAYMENT_NOTE
} from "../../actions/purchase/purchaseListAction";

let initialState = {
    purchaseList: '',
    purchase: '',
    purchase_id: '',
    payment_date: new Date().toISOString().slice(0, 10),
    reference_no: '',
    amount: 0.00,
    payment_method: 'Cash',
    attachment: '',
    note: ''
}

const purchaseListReducer = (state = initialState, action) => {
    let {type, payload} = action;

    switch (type) {

        case SET_PURCHASE_LIST: {
            let purchaseList = payload.purchaseList;
            let payable = (purchaseList && purchaseList.length > 0) ? (purchaseList[0].grand_total - purchaseList[0].paid_amount) : 0.00;
            return {
                ...state,
                purchaseList,
                purchase: (purchaseList.length > 0) ? purchaseList[0] : [],
                purchase_id: (purchaseList.length > 0) ? purchaseList[0].id : '',
                amount: (payable > 0) ? payable : 0.00,
            }
        }

        case SET_PURCHASE: {
            let payable = (payload.purchase.grand_total) ? (payload.purchase.grand_total - payload.purchase.paid_amount) : 0.00;
            return {
                ...state,
                purchase: payload.purchase,
                purchase_id: (payload.purchase.id) ? payload.purchase.id : '',
                amount: (payable > 0) ? payable : 0.00,
            }
        }
        case SET_PAYMENT_DATE: {

            return {
                ...state, payment_date: action.payload.payment_date
            };
        }
        case SET_PAYMENT_REFERENCE_NO: {
            return {
                ...state, reference_no: action.payload.payment_reference_no
            };
        }
        case SET_PAYMENT_AMOUNT: {

            let amount = action.payload.payment_amount ? Math.abs(parseFloat(action.payload.payment_amount)) : 0;
            amount = amount > state.purchase.grand_total ? state.purchase.grand_total : amount;
            return {
                ...state, amount

            };
        }
        case SET_PURCHASE_PAYMENT_METHOD: {
            return {
                ...state, payment_method: action.payload.purchase_payment_method

            };
        }
        case SET_PURCHASE_PAYMENT_DOCUMENT: {
            return {
                ...state,
                attachment: action.payload.payment_document

            };
        }
        case SET_PURCHASE_PAYMENT_NOTE: {
            return {
                ...state,
                note: action.payload.payment_note

            };
        }
        case ADD_PURCHASE_PAYMENT : {
            return state;
        }


        case DELETE_PURCHASE : {
            let purchaseList = state.purchaseList.filter(purchase => purchase.id !== action.payload.purchase_id);
            let payable = (purchaseList.length > 0) ? (purchaseList[0].grand_total - purchaseList[0].paid_amount) : 0.00;

            return {
                ...state,
                purchaseList: purchaseList,
                purchase: (purchaseList.length > 0) ? purchaseList[0] : [],
                purchase_id: (purchaseList.length > 0) ? purchaseList[0].id : '',
                amount: (payable > 0) ? payable : 0.00,
            };
        }

        default:
            return state;
    }

}

export default purchaseListReducer;