import {combineReducers} from "redux";
import purchaseCartReducers from "./purchase/purchaseCartReducers";
import adjustmentReducer from "./adjustment/adjustmentReducer";
import adjustmentListReducer from "./adjustment/adjustmentListReducer";
import purchaseListReducer from "./purchase/purchaseListReducer";
import printBarcodeReducer from "./barcode/printBarcodeReducer";
import saleListReducer from "./sale/saleListReducer";
import purchaseReturnListReducer from "./purchaseReturn/purchaseReturnListReducer";
import saleReturnListReducer from "./saleReturn/saleReturnListReducer";
import saleReturnCartReducer from "./saleReturn/saleReturnCartReducer";
import purchaseReturnReducer from "./purchaseReturn/purchaseReturnReducer";


const adminRootReducer = combineReducers({
    purchaseCart: purchaseCartReducers,
    purchaseListStore: purchaseListReducer,
    adjustmentProduct: adjustmentReducer,
    adjustmentListStore: adjustmentListReducer,
    printBarcodeStore: printBarcodeReducer,
    saleListStore: saleListReducer,
    purchaseReturnListStore: purchaseReturnListReducer,
    saleReturnListStore: saleReturnListReducer,
    saleReturnCart: saleReturnCartReducer,
    purchaseReturnStore: purchaseReturnReducer
});

export default adminRootReducer;