export const SET_CUSTOMER_ALL_ADDRESSES = 'SET_CUSTOMER_ALL_ADDRESSES';
export const SET_CUSTOMER_SHIPPING_ADDRESS = 'SET_CUSTOMER_SHIPPING_ADDRESS';
export const EDIT_CUSTOMER_SHIPPING_ADDRESS = 'EDIT_CUSTOMER_SHIPPING_ADDRESS';
export const SET_LOCATIONS = 'SET_LOCATIONS';
export const SET_AREAS = 'SET_AREAS';
export const SET_ALL_AREAS = 'SET_ALL_AREAS';
export const ADD_CUSTOMER_SHIPPING_ADDRESS = 'ADD_CUSTOMER_SHIPPING_ADDRESS';
export const UPDATE_CUSTOMER_SHIPPING_ADDRESS = 'UPDATE_CUSTOMER_SHIPPING_ADDRESS';
export const SET_AUTH_USER = 'SET_AUTH_USER';
export const CHANGE_AUTH_USER = 'CHANGE_AUTH_USER';
export const EXPAND_ADD_DELIVERY = 'EXPAND_ADD_DELIVERY';
export const SET_DELIVERY_TYPE = 'SET_DELIVERY_TYPE';

