import {
    ADD_SHIPPING_PRICE_BY_ADDRESS,
    ADD_SHIPPING_PRICE_BY_DATE,
    ADD_TO_CART,
    CART_CHECK_OUT,
    CHECK_BOX_STATUS,
    DECREASE,
    INCREASE,
    REMOVE,
    SET_DELIVERY_DATE
} from '../actions/cartAction';

import CartStorage from "../../../Helpers/Cart/CartStorage";

let initialStore = {
    cart: [],
    order_type: 'ECOM',
    shop_id: 1,
    user_id: 1,
    coupon_discount: 0,
    shipping_price_by_date: 0,
    shipping_price_by_address: 0,
    sub_total: 0,
    grand_total: 0,
    delivery_date: new Date().toISOString().slice(0, 10)
}

let sub_total = 0;
let grand_total = 0;

let ecommerceStore = (ecommerceState) => {
    CartStorage.setCartItems(ecommerceState);
}

let getInitialState = async () => {
    if (!CartStorage.hasCartItems()) {
        CartStorage.setCartItems(initialStore);
    } else {
        initialStore = CartStorage.getCartItems();
    }
}

getInitialState().then(result => {
    // console.log(result);
}).catch(error => {
    // console.log(error);
})


const cartReducer = (state = initialStore, action) => {

    sub_total = state.sub_total;

    switch (action.type) {

        // Add item to cart
        case ADD_TO_CART: {
            let id = action.payload.product.id;
            let name = action.payload.product.name;
            let feature_image = action.payload.product.feature_image;
            let unit_price = action.payload.product.selling_price;
            let stock_quantity = action.payload.product.product_quantity;
            let weight = action.payload.product.weight;
            let sale_unit = action.payload.product.sale_unit;
            let net_discount = 0;
            let quantity = action.payload.qty ? action.payload.qty : 1;
            let discount = net_discount * quantity;
            let price = unit_price * quantity - discount;

            let product = {
                id,
                name,
                feature_image,
                unit_price,
                net_discount,
                discount,
                price,
                stock_quantity,
                weight,
                sale_unit,
                quantity,
                checked: true
            }

            let itemExist = false;
            let tempCart = state.cart.map(cartItem => {
                if (cartItem.id === product.id) {
                    itemExist = true;
                    let qty = action.payload.qty ? action.payload.qty : 1;
                    let quantity = cartItem.quantity + qty;
                    let net_price = cartItem.unit_price * quantity;
                    let discount = (cartItem.net_discount * quantity).toFixed(2);
                    let price = (net_price - discount).toFixed(2);
                    if (cartItem.checked)
                        sub_total = sub_total + parseFloat(cartItem.unit_price - cartItem.net_discount) * qty;

                    cartItem = {...cartItem, price, discount, quantity};
                }
                return cartItem;
            });

            let stateData = (itemExist) ? {
                    ...state,
                    cart: tempCart, sub_total, grand_total: sub_total - state.coupon_discount
                } :
                {

                    ...state,
                    cart: [...state.cart, product],
                    sub_total: (sub_total + product.price),
                    grand_total: sub_total + product.price - state.coupon_discount
                };

            // Save Pricing Data to localstorage
            ecommerceStore(stateData);

            return stateData;


        }

        // Check and uncheck a cart item
        case CHECK_BOX_STATUS: {

            let tempCart = state.cart.map(cartItem => {
                if (cartItem.id === action.payload.id) {
                    cartItem = {...cartItem, checked: action.payload.checkedValue};
                    sub_total = (action.payload.checkedValue) ? (sub_total + parseFloat((cartItem.unit_price - cartItem.net_discount) * cartItem.quantity)) : (sub_total - parseFloat((cartItem.unit_price - cartItem.net_discount) * cartItem.quantity));

                }
                return cartItem;
            });

            let stateData = {...state, cart: tempCart, sub_total, grand_total: sub_total - state.coupon_discount};

            // Save Pricing Data to localstorage
            ecommerceStore(stateData);

            return stateData;
        }

        // Increase the cart quantity
        case INCREASE: {

            let tempCart = state.cart.map(cartItem => {
                if (cartItem.id === action.payload.id) {
                    let quantity = cartItem.quantity + 1;
                    let net_price = cartItem.unit_price * quantity;
                    let discount = (cartItem.net_discount * quantity).toFixed(2);
                    let price = (net_price - discount).toFixed(2);
                    if (cartItem.checked)
                        sub_total = sub_total + parseFloat(cartItem.unit_price - cartItem.net_discount);
                    cartItem = {...cartItem, price, discount, quantity};
                }
                return cartItem;
            });

            let stateData = {...state, cart: tempCart, sub_total, grand_total: sub_total - state.coupon_discount};

            // Save Pricing Data to localstorage
            ecommerceStore(stateData);

            return stateData;
        }

        // Decrease the quantity
        case DECREASE: {

            let tempCart = state.cart.map(cartItem => {
                if (cartItem.id === action.payload.id) {
                    let quantity = ((cartItem.quantity - 1) > 0) ? cartItem.quantity - 1 : 1;
                    let net_price = cartItem.unit_price * quantity;
                    let discount = (cartItem.net_discount * quantity).toFixed(2);
                    let price = (net_price - discount).toFixed(2);
                    if (cartItem.checked)
                        sub_total = ((cartItem.quantity - 1) > 0) ? (sub_total - parseFloat(cartItem.unit_price - cartItem.net_discount)) : sub_total;
                    cartItem = {...cartItem, price, discount, quantity};
                }
                return cartItem;
            });

            let stateData = {...state, cart: tempCart, sub_total, grand_total: sub_total - state.coupon_discount};

            // Save Pricing Data to localstorage
            ecommerceStore(stateData);

            return stateData;
        }


        // Remove a cart items
        case REMOVE: {
            state.cart.map(cartItem => {
                sub_total = sub_total - ((cartItem.id === action.payload.id && cartItem.checked) ? parseFloat((cartItem.unit_price - cartItem.net_discount) * cartItem.quantity) : 0);
            });

            let stateData = {
                ...state,
                cart: state.cart.filter(cartItem => cartItem.id !== action.payload.id),
                sub_total, grand_total: sub_total - state.coupon_discount
            };

            // Save Pricing Data to localstorage
            ecommerceStore(stateData);

            return stateData;
        }

        // Shipping Price Calculation by address distance
        case ADD_SHIPPING_PRICE_BY_ADDRESS: {

            let stateData = '';

            if (state.cart.length) {
                let address = action.payload.address;
                let shipping_price = address.area ? address.area.shipping_price : 0;
                let user_id = address.user ? address.user.id : 1;

                stateData = {
                    ...state,
                    shipping_price_by_address: shipping_price,
                    user_id: user_id,
                }

                // Save Pricing Data to localstorage
                ecommerceStore(stateData);
            } else {
                stateData = state;
            }

            return stateData;
        }

        // Shipping Price Calculation For Delivery
        case ADD_SHIPPING_PRICE_BY_DATE: {

            let stateData = '';

            if (state.cart.length) {

                let shipping_price = action.payload.shipping_price;

                stateData = {
                    ...state,
                    shipping_price_by_date: shipping_price,
                }

                // Save Pricing Data to localstorage
                ecommerceStore(stateData);

            } else {
                stateData = state;
            }

            return stateData;
        }

        case SET_DELIVERY_DATE : {

            let stateData = '';

            if (state.cart.length) {

                let delivery_date = action.payload.delivery_date;

                stateData = {
                    ...state,
                    delivery_date: delivery_date
                }

                // Save current state to localStorage
                ecommerceStore(stateData);
            } else {
                stateData = state;
            }

            return stateData;
        }


        // After Checkout need to reset state
        case CART_CHECK_OUT: {
            return initialStore;
        }

        default:
            return state;
    }
}
export default cartReducer;