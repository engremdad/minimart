import {
    ADD_CUSTOMER_SHIPPING_ADDRESS,
    CHANGE_AUTH_USER,
    EDIT_CUSTOMER_SHIPPING_ADDRESS,
    EXPAND_ADD_DELIVERY,
    SET_ALL_AREAS,
    SET_AREAS,
    SET_AUTH_USER,
    SET_CUSTOMER_ALL_ADDRESSES,
    SET_CUSTOMER_SHIPPING_ADDRESS, SET_DELIVERY_TYPE,
    SET_LOCATIONS,
    UPDATE_CUSTOMER_SHIPPING_ADDRESS
} from "../actions/deliveryActions";


let authUser = {
    id: '',
    first_name: '',
    last_name: '',
    email: '',
    phone: ''
}

let initialState = {
    locations: '',
    areas: '',
    allAreas: '',
    customerAllAddresses: '',
    customerShippingAddress: '',
    editableShippingAddress: '',
    authUser: authUser,
    expandAddDelivery: false,
    delivery_type: 'cash_on_delivery'
}


const deliveryReducer = (state = initialState, action) => {
    switch (action.type) {

        case SET_CUSTOMER_ALL_ADDRESSES : {

            let addresses = action.payload.addresses;

            let defaultSelectedAddress = (addresses.length && addresses.find((address) => address.selected == 1) || addresses.length && addresses[0]);

            return {
                ...state,
                customerAllAddresses: addresses,
                customerShippingAddress: defaultSelectedAddress
            }
        }

        case SET_LOCATIONS : {
            return {
                ...state,
                locations: action.payload.locations
            }
        }

        case SET_AREAS : {
            return {
                ...state,
                areas: action.payload.areas
            }
        }

        case SET_ALL_AREAS : {
            return {
                ...state,
                allAreas: action.payload.allAreas
            }
        }

        case SET_CUSTOMER_SHIPPING_ADDRESS: {
            return {
                ...state,
                customerShippingAddress: action.payload.address
            }
        }

        case EDIT_CUSTOMER_SHIPPING_ADDRESS: {
            return {
                ...state,
                editableShippingAddress: action.payload.address ? action.payload.address : ''
            }
        }

        case ADD_CUSTOMER_SHIPPING_ADDRESS: {
            return {
                ...state,
                customerAllAddresses: [action.payload.address, ...state.customerAllAddresses],
                customerShippingAddress: action.payload.address
            }
        }

        case UPDATE_CUSTOMER_SHIPPING_ADDRESS: {

            let updatedAddress = action.payload.address;


            let updatedAllAddress = state.customerAllAddresses.map((address) => {
                if (address.id === updatedAddress.id) {
                    return updatedAddress;
                } else {
                    return address;
                }
            });


            return {
                ...state,
                customerAllAddresses: updatedAllAddress
            }
        }

        case SET_AUTH_USER: {

            let user = action.payload.userData ? action.payload.userData : action.payload.authUser;

            let id = user.id ? user.id : state.authUser.id;
            let first_name = user.first_name ? user.first_name : state.authUser.first_name;
            let last_name = user.last_name ? user.last_name : state.authUser.last_name;
            let email = user.email ? user.email : state.authUser.email;
            let phone = user.phone ? user.phone : state.authUser.phone;


            let userData = {
                id,
                first_name,
                last_name,
                email,
                phone
            }


            return {
                ...state,
                authUser: userData
            }
        }

        case CHANGE_AUTH_USER : {
            let user = action.payload.userData;

            let id = user.id ? user.id : state.authUser.id;
            let first_name = user.first_name ? user.first_name : state.authUser.first_name;
            let last_name = user.last_name ? user.last_name : state.authUser.last_name;
            let email = user.email ? user.email : state.authUser.email;
            let phone = user.phone ? user.phone : state.authUser.phone;

            let userData = {
                id,
                first_name,
                last_name,
                email,
                phone
            }


            return {
                ...state,
                authUser: userData
            }
        }


        case EXPAND_ADD_DELIVERY : {
            return {
                ...state,
                expandAddDelivery: true
            }
        }

        case SET_DELIVERY_TYPE : {
            return {
                ...state,
                delivery_type: action.payload.delivery_type
            }
        }

        default:
            return state;
    }
}

export default deliveryReducer;