import {combineReducers} from "redux";
import cartReducer from "./cartReducer";
import menuReducer from "./menuReducer";
import productReducer from "./productReducer";
import deliveryReducer from "./deliveryReducer";


const rootReducer = combineReducers({
    cartStore: cartReducer,
    menuStore: menuReducer,
    productStore: productReducer,
    deliveryStore: deliveryReducer
});

export default rootReducer;