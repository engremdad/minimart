import {SET_MODAL_PRODUCT, SET_POPULAR_PRODUCT, SET_SINGLE_PRODUCT, SHOW_CART_MENU} from "../actions/productAction";

let initialState = {
    products: [],
    modalProduct: '',
    show: false
}


const productReducer = (state = initialState, action) => {

    switch (action.type) {
        case SET_POPULAR_PRODUCT: {

            return {
                ...state,
                products: action.payload.products
            }
        }

        case SET_MODAL_PRODUCT : {
            return {
                ...state,
                modalProduct: action.payload.modalProduct
            }
        }

        case SHOW_CART_MENU: {
            return {
                ...state,
                show: !state.show
            }
        }

        default :
            return state;
    }
}

export default productReducer;