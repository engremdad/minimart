import SiteHeader from "../../components/Frontend/Header/SiteHeader";
import Layout from "../../components/Frontend/Layout/Layout";
import React from 'react';
import axios from 'axios';
import {useRouter} from "next/router";
import * as process from "../../next.config";
import ProductList from "../../components/Frontend/Container/Category/ProductList";


const CategoryItem = ({products}) => {
    const router = useRouter();
    const {slug} = router.query;
    return (
        <>

            <SiteHeader>Minimart</SiteHeader>

            <Layout>

                <ProductList products={products} slug={slug}/>

            </Layout>

        </>
    );
};

CategoryItem.getInitialProps = async (ctx) => {

    const api_url = process.env.mode === 'development' ? process.env.api_url_dev : process.env.api_url;

    let slug = ctx.query.slug;
    let products = '';

    let response = await axios.post(`${api_url}/search/category`, {slug}, {
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "X-Requested-With"
        }
    }).then((result) => {
        return result.data;
    }).catch((exception) => {
        return exception;
    });

    if (response.success) {
        products = response.data;
    } else {
        products = [];
    }

    return {products};
}


export default CategoryItem;
