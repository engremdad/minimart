import SiteHeader from "../../components/Frontend/Header/SiteHeader";
import Layout from "../../components/Frontend/Layout/Layout";
import React, {useState, useEffect} from 'react';
import axios from 'axios';
import Router, {useRouter} from "next/router";
import * as process from "../../next.config";
import {connect} from "react-redux";
import {ADD_TO_CART} from "../../store/frontend/actions/cartAction";

const SearchResult = ({ addToCart, products}) => {
// const SearchResult = (props) => {
//     const  {} = props;
    const router = useRouter();
    const {keyword} = router.query;

    return (
        <>

            <SiteHeader>Minimart</SiteHeader>

            <Layout>
                <section className="product-block">
                    <div className="container-fluid">
                        <div className="row text-center">
                            <div className="col-md-12">
                                <h3>Search result for : {keyword}</h3>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="product-showcase-wrap">
                                    {products ? products.map(product =>
                                        <div className="product-box-grid" key={product.id}>
                                            <div className="product-box">
                                                <div className="product-box-wrap">
                                                    <div className="product-box-image">
                                                        <img src={product.product_image[0]} alt={product.name}/>
                                                    </div>
                                                    <div className="product-details">
                                                        <h6 className="product-title">{product.name}</h6>
                                                        <p className="product-unit">{product.weight} {product.sale_unit}</p>
                                                        <p className="product-price">৳ {product.selling_price}</p>
                                                    </div>
                                                    <div className="pbw-hover">
                                                        <div className="cart">
                                                            <a href="#"
                                                               onClick={event => {
                                                                   event.preventDefault();
                                                                   addToCart(product)
                                                               }}
                                                            ><i className="icon-shopping-basket"/> Add to Cart</a>
                                                        </div>
                                                        <div className="view-option">
                                                            <a href="#"><i className="icon-eye"> </i>Quick View</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="product-cart">
                                                    <a href={`/product/${encodeURIComponent(product.slug)}`}
                                                       onClick={(e) => {
                                                           e.preventDefault();
                                                           Router.push({
                                                               pathname: '/product/[slug]',
                                                               query: { slug: product.slug },
                                                           })
                                                       }}
                                                    ><i className="icon-eye"/> View</a>
                                                    <a href="#"
                                                       onClick={event => {
                                                           event.preventDefault();
                                                           addToCart(product)
                                                       }}
                                                    ><i className="icon-shopping-basket"/> Cart</a>
                                                </div>
                                            </div>
                                        </div>
                                    ): <h1>No Data Found</h1>}
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>

        </>
    );
};

SearchResult.getInitialProps = async (ctx) => {

    const api_url = process.env.api_url;

    let keyword = ctx.query.keyword;
    if(keyword){
        let response = await axios.post(`${api_url}/search`, {keyword}).then((result) => {
            return result.data;
        }).catch((exception) => {
            return exception;
        });

        if (response.success){
            return {products: response.data}
        }else{
            return {products: []}
        }
    }
    else{
        return {products: []}
    }
}
function mapDispatchToProps(dispatch){

    return{
        addToCart: (product) => dispatch({ type: ADD_TO_CART, payload: { product} })
    }
}


export default connect(null, mapDispatchToProps)(SearchResult)
