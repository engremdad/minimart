import LayoutBlank from "../../components/Admin/Layout/LayoutBlank"
import LoginForm from "../../components/Auth/LoginForm";
import React from "react";


const Login = () => {
    return (
        <>
            <LayoutBlank>
                <LoginForm />
            </LayoutBlank>
        </>
    )
}


export default Login;