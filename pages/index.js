import SiteHeader from "../components/Frontend/Header/SiteHeader";
import Layout from "../components/Frontend/Layout/Layout";
import React from "react";
import HomeSearch from "../components/Frontend/Container/Home/Search";
import HomeCategory from "../components/Frontend/Container/Home/Category";
import PopularProduct from "../components/Frontend/Container/Home/PopularProduct";
import axios from "axios";
import {SET_HOME_CATEGORIES, SET_MENU_CATEGORIES} from "../store/frontend/actions/menuActions";
import {connect} from "react-redux";

const Index = (props) => {

    const meta = {
        title: 'theShoply',
        keywords: 'bangladesh online grocery, bangladesh bazaar, best bazaar, daily bazaar, daily shop, large shop, bd shop, shop, online store,  buy, sell, home shop, Meat, Oil, Chal, Free home delivery, Fresh vegetables, formalin free, free return',
        description: 'Order grocery and food online with same-day home delivery. Save money, save time',
        ogTitle: 'Chaldal 🥚 Online Grocery Shopping and Delivery in Dhaka | Buy fresh food items, personal care, baby products and more',
        ogType: 'website',
        ogImage: '/img/logo.png',
        ogSiteName: 'DIT POS',
        ogDescription: 'Order grocery and food online with same-day home delivery. Save money, save time'
    }

    if (props.menuCategories.length === 0) {
        props.setMenuCategories(props.categoryData.menu_categories);
    }

    if (props.homeCategories.length === 0) {
        props.setHomeCategories(props.categoryData.home_categories);
    }


    return (
        <>
            <SiteHeader {...meta}/>

            <Layout>
                <HomeSearch/>


                <HomeCategory/>

                <PopularProduct/>


                <section className="class_name">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">

                            </div>
                        </div>
                    </div>
                </section>
            </Layout>

        </>
    )
}

Index.getInitialProps = async (ctx) => {

    const api_url = process.env.api_url;

    let categoryData = {
        home_categories: [],
        menu_categories: []
    }

    let homeCategoryResponse = await axios.post(`${api_url}/category`, {type: "home"})
        .then((result) => {
            return result.data;
        }).catch((error) => {
            return error;
        });

    let menuCategoryResponse = await axios.get(`${api_url}/getAllCategoryWithChild/menuShow`)
        .then((result) => {
            return result.data;
        }).catch((exception) => {
            return exception;
        });

    if (homeCategoryResponse.success) {
        categoryData.home_categories = homeCategoryResponse.data;
    }

    if (menuCategoryResponse.success) {
        categoryData.menu_categories = menuCategoryResponse.data;
    }

    return {categoryData};


}

const mapDispatchToProps = (dispatch) => {
    return {
        setMenuCategories: (menu_categories) => dispatch({type: SET_MENU_CATEGORIES, payload: {menu_categories}}),
        setHomeCategories: (home_categories) => dispatch({type: SET_HOME_CATEGORIES, payload: {home_categories}}),
    }
}

const mapStateToProps = (store) => {

    const {menuCategories, homeCategories} = store.menuStore;

    return {menuCategories, homeCategories};
}


export default connect(mapStateToProps, mapDispatchToProps)(Index);
