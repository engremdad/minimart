import React from "react";
import Layout from "../../../components/Admin/Layout/Layout";
import AdminHeader from "../../../components/Admin/Header/AdminHeader";
import PermissionGroupTable from "../../../components/Admin/PermissionGroup/PermissionGroupTable";

const PermissionGroupList = () => {
  return (
    <>
      <AdminHeader> Permission group | theShoply</AdminHeader>
      <Layout>
        <PermissionGroupTable />
      </Layout>
    </>
  );
};

export default PermissionGroupList;
