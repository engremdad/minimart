import React from 'react';
import AdminHeader from "../../../components/Admin/Header/AdminHeader";
import Layout from "../../../components/Admin/Layout/Layout";
import PermissionTable from "../../../components/Admin/Permission/PermissionTable";

const Permission = () => {
    return (
        <>
            <AdminHeader>List Permission | theShoply</AdminHeader>
            <Layout>
                <PermissionTable/>
            </Layout>
        </>
    );
};

export default Permission;