import AdminHeader from "../../../../components/Admin/Header/AdminHeader";
import Layout from "../../../../components/Admin/Layout/Layout";
import EditPermission from "../../../../components/Admin/Permission/EditPermission";

const UpdatePermission = () => {
    return (
        <>
            <AdminHeader>Permission edit | theShoply</AdminHeader>
            <Layout>
                <EditPermission/>
            </Layout>
        </>
    );
};

export default UpdatePermission;