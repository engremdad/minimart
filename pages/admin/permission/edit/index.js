import AdminHeader from "../../../../components/Admin/Header/AdminHeader";
import Layout from "../../../../components/Admin/Layout/Layout";

const UpdatePermission = () => {
    return (
        <>
            <AdminHeader>Permission edit | theShoply</AdminHeader>
            <Layout>
                <h1 className="text-center mt-5"> 404 - No permission found for edit</h1>
            </Layout>
        </>
    );
};

export default UpdatePermission;