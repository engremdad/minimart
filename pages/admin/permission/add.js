import React from 'react';
import AdminHeader from "../../../components/Admin/Header/AdminHeader";
import Layout from "../../../components/Admin/Layout/Layout";
import AddPermission from "../../../components/Admin/Permission/AddPermission";

const Add = () => {
    return (
        <>
            <AdminHeader>Add Permission | theShoply</AdminHeader>
            <Layout>
                <AddPermission/>
            </Layout>
        </>
    );
};

export default Add;