import AdminHeader from "../../../components/Admin/Header/AdminHeader";
import Layout from "../../../components/Admin/Layout/Layout";
import AddCategory from "../../../components/Admin/Category/Add/AddCategory";

const CreateCategory = () => {
  return (
    <>
      <AdminHeader>Add Category | Minimart</AdminHeader>
      <Layout>
        <AddCategory />
      </Layout>
    </>
  );
};

export default CreateCategory;
