import AdminHeader from "../../../../components/Admin/Header/AdminHeader";
import Layout from "../../../../components/Admin/Layout/Layout";
import EditCategory from "../../../../components/Admin/Category/Edit/EditCategory";
import axios from "axios";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";

const UpdateCategory = () => {
  const { asPath, route, query } = useRouter();
  const [categoryEditData, setCategoryEditData] = useState("");
  useEffect(() => {
    let isMounted = true;

    const getCategory = async (categoryId) => {
      const categoryApiUrl = `${process.env.api_backend_url}/categories/${categoryId}`;
      const apiResponses = await axios.get(categoryApiUrl);
      return await apiResponses.data;
    };

    if (asPath !== route) {
      getCategory(query.edit)
        .then((category) => {
          if (category.success) {
            isMounted && setCategoryEditData(category);
          } else {
            throw "something goes wrong on server";
          }
        })
        .catch((errors) => {
          console.error(errors);
        });
    }

    return () => (isMounted = false);
  }, [asPath, route, query]);

  return (
    <>
      <AdminHeader>Category | Minimart</AdminHeader>
      <Layout>
        {categoryEditData.success ? (
          <EditCategory categoryEditData={categoryEditData.data} />
        ) : (
          ""
        )}
      </Layout>
    </>
  );
};

export default UpdateCategory;
