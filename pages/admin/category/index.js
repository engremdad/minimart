import AdminHeader from "../../../components/Admin/Header/AdminHeader";
import Layout from "../../../components/Admin/Layout/Layout";
import CategoryTable from "../../../components/Admin/Category/List/CategoryTable";

const Category = () => {
  return (
    <>
      <AdminHeader>Category List | Minimart</AdminHeader>
      <Layout>
        <CategoryTable />
      </Layout>
    </>
  );
};
export default Category;
