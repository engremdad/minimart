import React from "react";
import AdminHeader from "../../../components/Admin/Header/AdminHeader";
import Layout from "../../../components/Admin/Layout/Layout";
import OrderList from "../../../components/Admin/Sales/OrderList";
import OrderDetails from "../../../components/Admin/Sales/OrderDetails";
import OrderProcessing from "../../../components/Admin/Sales/OrderProcessing";
import OrderPaymentHistory from "../../../components/Admin/Sales/OrderPaymentHistory";

const SalesInfo = () => {


    return (

        <>
            <AdminHeader>Product Sales List | Minimart</AdminHeader>
            <Layout>

                <section className="custom-card">
                    <div className="container-fluid">
                        <div className="row">
                            <OrderList/>
                            <div className="col-md-5">
                                <OrderDetails/>
                                <OrderProcessing/>
                                <OrderPaymentHistory/>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    );
}
export default SalesInfo;