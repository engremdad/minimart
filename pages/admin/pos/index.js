import AdminHeader from "../../../components/Admin/Header/AdminHeader";
import React from "react";
import LayoutPos from "../../../components/Admin/Layout/LayoutPos";
import SearchBar from "../../../components/Admin/Pos/SearchBar";
import CategorySearch from "../../../components/Admin/Pos/CategorySearch";
import BrandSearch from "../../../components/Admin/Pos/BrandSearch";
import PosProducts from "../../../components/Admin/Pos/PosProducts";
import Customer from "../../../components/Admin/Pos/Customer";
import CartProducts from "../../../components/Admin/Pos/CartProducts";
import CartCalculation from "../../../components/Admin/Pos/CartCalculation";
import Checkout from "../../../components/Admin/Pos/Checkout";
import PosModal from "../../../components/Admin/Pos/PosModal";

const POS = (props) => {
  return (
    <>
      <AdminHeader>POS | Minimart</AdminHeader>

      <LayoutPos>
        <section className="pos-container">
          <div className="container-fluid">
            <div className="pos-content-wrap">
              <div className="row">
                <div className="col-lg-6">
                  <div className="pos-content-left shadow">
                    <div className="pos-product-select">
                      <div className="prodict-filter">
                        {/*Searched by product and product code*/}
                        <SearchBar />

                        <div className="row">
                          {/*Product select by warehouse*/}
                          {/*<WarehouseSearch />*/}

                          {/*Product select by Category*/}
                          <CategorySearch />

                          {/*Product select by brand*/}
                          <BrandSearch />
                        </div>
                      </div>

                      <div className="product-showcase">

                          {/*Searched Products*/}
                          <PosProducts />

                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="pos-content-right shadow">
                    <form action="" className="pos-sales-form">
                      <Customer />

                      <CartProducts />

                      <CartCalculation />
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <Checkout />
        <PosModal />
      </LayoutPos>
    </>
  );
};

// POS.getInitialProps = async (ctx) => {

//     // Checked it is on the server
//     if (ctx.res) {

//         let cookies = ctx.req.headers.cookie;

//         let isLoggedIn = cookies ? cookies.includes('token') : false;

//         if (!isLoggedIn) {
//             ctx.res.writeHead(302, { Location: '/auth/login' });
//             ctx.res.end();
//         }

//     }

//     return {};

// }

export default POS;
