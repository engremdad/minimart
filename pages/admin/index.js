import React,{useState,useEffect} from 'react';
import AdminHeader from "../../components/Admin/Header/AdminHeader";
import Layout from "../../components/Admin/Layout/Layout";
import axios from "axios";
const Index = () => {

    const [reports, setReports] = useState('');
    const [reportsType,setReportsType] = useState('daily');

    const getReport = async (reportType) => {
        setReportsType(reportType);
        const url = `${process.env.api_backend_url}/get-reports`;
        axios.post(url,{report_type:reportType}).then((response) => {
            (response.data.success && setReports(response.data.data) || !response.data.success && setReports(''))
        }).catch((errors) => (errors));
    }

    useEffect(() => {
        let mounted = true;
        (mounted && getReport('daily'));
        return () => {
            mounted = false
        };
    }, [])

    return (
        <>
            <AdminHeader>Admin Dashboard | Minimart</AdminHeader>
            <Layout>
                <section className="custom-card">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="custom-card-box">
                                    <div className="custom-card-box-header">

                                        <div className="fbh-title">
                                            <i className="icon-tachometer-alt-solid" />
                                            <h2>Summary Report</h2>

                                        </div>
                                    </div>
                                    <div className="card-body">
                                        <div className="dsr-filter d-flex align-items-center justify-content-center">
                                            <div className="btn-group mx-auto" role="group">
                                                <button type="button" className={`btn ${reportsType==='daily' ? 'active':''}`} onClick={(event)=>{
                                                    event.preventDefault();
                                                    getReport('daily')
                                                }}>Today</button>
                                                <button type="button" className={`btn ${reportsType==='weekly' ? 'active':''}`} onClick={(event)=>{
                                                    event.preventDefault();
                                                    getReport('weekly')
                                                }}>Last 7 Days</button>
                                                <button type="button" className={`btn ${reportsType==='monthly' ? 'active':''}`} onClick={(event)=>{
                                                    event.preventDefault();
                                                    getReport('monthly')
                                                }}>This Month</button>
                                                <button type="button" className={`btn ${reportsType==='yearly' ? 'active':''}`}onClick={(event)=>{
                                                    event.preventDefault();
                                                    getReport('yearly')
                                                }}>This Year</button>
                                            </div>
                                        </div>
                                        <div className="card-group">
                                            <div className="card border-right rounded-0">
                                                <div className="card-body">
                                                    <div className="d-flex d-lg-flex d-md-block align-items-center">
                                                        <div>
                                                            <div className="d-inline-flex align-items-center">
                                                                <h2 className="text-dark mb-1 font-weight-medium">{reports.total_sale}</h2>
                                                                {/*<span*/}
                                                                {/*    className="badge bg-primary font-12 text-white font-weight-medium badge-pill ml-2 d-lg-block d-md-none">+18.33%</span>*/}
                                                            </div>
                                                            <h6 className="text-muted font-weight-normal mb-0 w-100 text-truncate">
                                                                Sales</h6>
                                                        </div>
                                                        <div className="ml-auto mt-md-3 mt-lg-0">
                                                            <span className="opacity-7 text-muted">
                                                                ৳
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card border-right">
                                                <div className="card-body">
                                                    <div className="d-flex d-lg-flex d-md-block align-items-center">
                                                        <div>
                                                            <h2 className="text-dark mb-1 w-100 text-truncate font-weight-medium">
                                                                {reports.total_sales_return}</h2>
                                                            <h6 className="text-muted font-weight-normal mb-0 w-100 text-truncate">
                                                                Sales Return
                                                            </h6>
                                                        </div>
                                                        <div className="ml-auto mt-md-3 mt-lg-0">
                                                            <span className="opacity-7 text-muted">
                                                                ৳
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card border-right">
                                                <div className="card-body">
                                                    <div className="d-flex d-lg-flex d-md-block align-items-center">
                                                        <div>
                                                            <div className="d-inline-flex align-items-center">
                                                                <h2 className="text-dark mb-1 font-weight-medium">{reports.total_purchase_return}</h2>
                                                                {/*<span*/}
                                                                {/*    className="badge bg-danger font-12 text-white font-weight-medium badge-pill ml-2 d-md-none d-lg-block">-18.33%</span>*/}
                                                            </div>
                                                            <h6 className="text-muted font-weight-normal mb-0 w-100 text-truncate">Purchase Return</h6>
                                                        </div>
                                                        <div className="ml-auto mt-md-3 mt-lg-0">
                                                            <span className="opacity-7 text-muted">
                                                                ৳
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card rounded-0">
                                                <div className="card-body">
                                                    <div className="d-flex d-lg-flex d-md-block align-items-center">
                                                        <div>
                                                            <h2 className="text-dark mb-1 font-weight-medium">{reports.gross_profit}</h2>
                                                            <h6 className="text-muted font-weight-normal mb-0 w-100 text-truncate">Profit/Loss</h6>
                                                        </div>
                                                        <div className="ml-auto mt-md-3 mt-lg-0">
                                                            <span className="opacity-7 text-muted">
                                                                ৳
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>

        </>
    );
};

export default Index;