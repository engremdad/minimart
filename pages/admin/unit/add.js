import AdminHeader from "../../../components/Admin/Header/AdminHeader";
import Layout from "../../../components/Admin/Layout/Layout";
import AddUnit from "../../../components/Admin/Unit/AddUnit";

const CreateUnit = () => {
  return (
    <>
      <AdminHeader>Add Unit | Minimart</AdminHeader>
      <Layout>
        <AddUnit />
      </Layout>
    </>
  );
};

export default CreateUnit;
