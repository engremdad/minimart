import AdminHeader from "../../../components/Admin/Header/AdminHeader";
import Layout from "../../../components/Admin/Layout/Layout";
import UnitTable from "../../../components/Admin/Unit/UnitTable";
// import axios from "axios";
const Brand = () => {
  return (
    <>
      <AdminHeader>Unit List | Minimart</AdminHeader>
      <Layout>
        <UnitTable />
      </Layout>
    </>
  );
};

export default Brand;
