import React from "react";

import AdminHeader from "../../../components/Admin/Header/AdminHeader";
import Layout from "../../../components/Admin/Layout/Layout";
import Index from "../../../components/Admin/Reports/ShowExpiredProducts/Index";

const ExpiredProducts = () => {
  return (
    <>
      <AdminHeader>Expired Product List | Minimart</AdminHeader>
      <Layout>
        <Index />
      </Layout>
    </>
  );
};

export default ExpiredProducts;
