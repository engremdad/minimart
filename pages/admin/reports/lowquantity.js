import React from 'react';
import Index from "../../../components/Admin/Reports/LowQuantityAlert/Index";
import AdminHeader from "../../../components/Admin/Header/AdminHeader";
import Layout from "../../../components/Admin/Layout/Layout";

const LowQuantity = () => {
  return (
    <>
      <AdminHeader>Low Quantity Products | Minimart</AdminHeader>
      <Layout>
        <Index />
      </Layout>
    </>
  );
};

export default LowQuantity;
