import React from 'react';
import AdminHeader from "../../../components/Admin/Header/AdminHeader";
import Layout from "../../../components/Admin/Layout/Layout";
import ListModule from "../../../components/Admin/Module/ListModule";

const Index = () => {
    return (
        <>
            <AdminHeader> Module List | theShoply</AdminHeader>
            <Layout>
                <ListModule/>
            </Layout>
        </>
    );
};

export default Index;