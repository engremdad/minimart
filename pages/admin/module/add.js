import AdminHeader from "../../../components/Admin/Header/AdminHeader";
import Layout from "../../../components/Admin/Layout/Layout";
import AddModule from "../../../components/Admin/Module/AddModule";

const Add = () => {
    return (
        <>
            <AdminHeader>Add Module | theShoply</AdminHeader>

            <Layout>
                <AddModule/>
            </Layout>
        </>
    )
}

export default Add;