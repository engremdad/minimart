import React from "react";
import AdminHeader from "../../../components/Admin/Header/AdminHeader";
import Layout from "../../../components/Admin/Layout/Layout";
import AdjustmentList from "../../../components/Admin/Adjustment/List/AdjustmentList";
import AdjustedProductList from "../../../components/Admin/Adjustment/List/AdjustedProductList";

const Index = () => {
  return (
    <>
      <AdminHeader>Product Adjustment List | Minimart</AdminHeader>
      <Layout>
        <section className="custom-card">
          <div className="container-fluid">
            <div className="row">
              <AdjustmentList />

              <AdjustedProductList />
            </div>
          </div>
        </section>
      </Layout>
    </>
  );
};

// Index.getInitialProps = async (ctx) => {

//     // Checked it is on the server
//     if (ctx.res) {

//         let cookies = ctx.req.headers.cookie;
//         let isLoggedIn = cookies ? cookies.includes('token') : false;

//         if (!isLoggedIn) {
//             ctx.res.writeHead(302, {Location: '/auth/login'});
//             ctx.res.end();
//         }

//     }

//     return {};

// }

export default Index;
