import React from "react";
import AdminHeader from "../../../components/Admin/Header/AdminHeader";
import Layout from "../../../components/Admin/Layout/Layout";
import AddAdjustment from "../../../components/Admin/Adjustment/Add/AddAdjustment";

const Add = () => {
  return (
    <>
      <AdminHeader>Add Adjustment | Minimart</AdminHeader>
      <Layout>
        <AddAdjustment />
      </Layout>
    </>
  );
};

// Add.getInitialProps = async (ctx) => {

//     // Checked it is on the server
//     if (ctx.res) {

//         let cookies = ctx.req.headers.cookie;
//         let isLoggedIn = cookies ? cookies.includes('token') : false;

//         if (!isLoggedIn) {
//             ctx.res.writeHead(302, {Location: '/auth/login'});
//             ctx.res.end();
//         }

//     }

//     return {};

// }

export default Add;
