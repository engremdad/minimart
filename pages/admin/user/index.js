import AdminHeader from "../../../components/Admin/Header/AdminHeader";
import Layout from "../../../components/Admin/Layout/Layout";
import UserTable from "../../../components/Admin/User/UserTable";
import {useState, useEffect} from "react";
import axios from "axios";
import process from "../../../next.config";

const User = () => {

    const [users, setUsers] = useState(null);

    const userListApi = async () => {
        const userApiUrl = `${process.env.api_url}/users`;
        return await axios.get(userApiUrl).then((apiResponse) => (apiResponse.data)).catch((errors) => (errors));
    }

    useEffect(() => {
        let mounted = true;

        userListApi().then((userList) => {
            mounted && (
                userList.success && setUsers(userList.data) || !userList.success && setUsers('')
            )
        });

        return () => {
            mounted = false
        };
    }, [])

    if(users=== null){
        return (
            <>
                <AdminHeader>List User | Minimart</AdminHeader>

                <Layout>
                    <h1 className="text-center"> Please Wait user list is loading</h1>
                </Layout>
            </>
        );
    }

    return (
        <>
            <AdminHeader>List User | theShoply</AdminHeader>

            <Layout>
                <UserTable users={users}/>
            </Layout>
        </>
    );
};

export default User;