import AdminHeader from "../../../components/Admin/Header/AdminHeader";
import Layout from "../../../components/Admin/Layout/Layout";
import AddUser from "../../../components/Admin/User/AddUser";

const Add = () => {
    return (
        <>
            <AdminHeader>Add User | Minimart</AdminHeader>

            <Layout>
                <AddUser/>
            </Layout>
        </>
    );
}

export default Add;