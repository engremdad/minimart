import Layout from "../../../../components/Admin/Layout/Layout";
import AdminHeader from "../../../../components/Admin/Header/AdminHeader";
import EditUser from "../../../../components/Admin/User/EditUser";
import {useRouter} from "next/router";
import process from "../../../../next.config";
import {useEffect, useState} from "react";
import axios from "axios";

const UpdateUser = () => {

    const userUrlSlug = useRouter();
    const [userEditData, setUserEditData] = useState(null);

    const userEditApi = async (slug) => {
        const userApiUrl = `${process.env.api_url}/users/${slug}`;
        return axios.get(userApiUrl).then(apiResponse => apiResponse.data).catch(errors => errors);
    };

    useEffect(() => {
        let mounted = true;
        if (userUrlSlug.asPath !== userUrlSlug.route) {
            userEditApi(userUrlSlug.query.edit).then((user) => {
                mounted && (user.success && setUserEditData(user.data)) || (!user.success && setUserEditData(user.success));
            })
        }
        return () => {
            mounted = false
        }
    }, [userUrlSlug])

    if (userEditData === null) {
        return (
            <>
                <AdminHeader>List User | Minimart</AdminHeader>
                <Layout>
                    <h1 className="text-center"> Please Wait user data is loading</h1>
                </Layout>
            </>
        );
    }

    return (
        <>
            <AdminHeader>List User | theShoply</AdminHeader>
            <Layout>
                {userEditData ? <EditUser userEditData={userEditData}/>:<><h1 className="text-center"> 404 - User data not found</h1></>}
            </Layout>
        </>
    );
}

export default UpdateUser;