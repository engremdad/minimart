import AdminHeader from "../../components/Admin/Header/AdminHeader";
import React from "react";
import LayoutBlank from "../../components/Admin/Layout/LayoutBlank";
import Invoice from "../../components/Admin/Pos/Invoice"

const POS = (props) => {


    return (
        <>
            <AdminHeader>POS | Minimart </AdminHeader>

            <LayoutBlank>
                <Invoice />
            </LayoutBlank>
        </>
    )
}


export default POS;
