import React from 'react';
import AdminHeader from "../../../../components/Admin/Header/AdminHeader";
import Layout from "../../../../components/Admin/Layout/Layout";
import SaleReturnList from "../../../../components/Admin/SaleReturn/List/SaleReturnList";
import SaleReturnItems from "../../../../components/Admin/SaleReturn/List/SaleReturnItems";

const SaleReturn = () => {
    return (
        <>
            <AdminHeader>Sale Return | Minimart</AdminHeader>
            <Layout>
                <section className="custom-card">
                    <div className="container-fluid">
                        <div className="row">
                            <SaleReturnList/>
                            <div className="col-md-5">
                                <SaleReturnItems/>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    );
};

export default SaleReturn;