import React from 'react';
import AdminHeader from "../../../../components/Admin/Header/AdminHeader";
import Layout from "../../../../components/Admin/Layout/Layout";
import AddSaleReturn from "../../../../components/Admin/SaleReturn/Add/AddSaleReturn";

const Add = () => {
    return (
        <>
            <AdminHeader>Add Sale Return| Minimart</AdminHeader>
            <Layout>
                <AddSaleReturn/>
            </Layout>
        </>
    );
};

export default Add;