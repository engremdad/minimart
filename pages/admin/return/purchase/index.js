import React from 'react';
import AdminHeader from "../../../../components/Admin/Header/AdminHeader";
import Layout from "../../../../components/Admin/Layout/Layout";
import PurchaseReturnList from "../../../../components/Admin/PurchaseReturn/List/PurchaseReturnList";
import PurchaseReturnItems from "../../../../components/Admin/PurchaseReturn/List/PurchaseReturnItems";

const PurchaseReturn = () => {
    return (
        <>
            <AdminHeader>Purchase Return | Minimart</AdminHeader>
            <Layout>
                <section className="custom-card">
                    <div className="container-fluid">
                        <div className="row">
                            <PurchaseReturnList/>
                            <div className="col-md-5">
                                <PurchaseReturnItems/>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    );
};

export default PurchaseReturn;