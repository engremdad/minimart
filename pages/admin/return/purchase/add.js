import React from 'react';
import AdminHeader from "../../../../components/Admin/Header/AdminHeader";
import Layout from "../../../../components/Admin/Layout/Layout";
import AddPurchaseReturn from "../../../../components/Admin/PurchaseReturn/Add/AddPurchaseReturn";

const Add = () => {
    return (
        <>
            <AdminHeader>Add Purchase Return | Minimart</AdminHeader>
            <Layout>
                <AddPurchaseReturn/>
            </Layout>
        </>
    );
};

export default Add;