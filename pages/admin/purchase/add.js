import AdminHeader from "../../../components/Admin/Header/AdminHeader";
import Layout from "../../../components/Admin/Layout/Layout";
import AddPurchase from "../../../components/Admin/Purchase/Add/AddPurchase";
import React from "react";

const PurchaseProduct = () => {

    return (
        <>

            <AdminHeader>Product Purchase | Minimart</AdminHeader>
            <Layout>
                <AddPurchase/>
            </Layout>
        </>
    )
}

export default PurchaseProduct;