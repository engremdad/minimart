import AdminHeader from "../../../../components/Admin/Header/AdminHeader";
import Layout from "../../../../components/Admin/Layout/Layout";

import React from "react";
import EditPurchase from "../../../../components/Admin/Purchase/Edit/EditPurchase";

const PurchaseProduct = (props) => {

    return (
        <>

            <AdminHeader>Purchase Edit | Minimart </AdminHeader>
            <Layout>
                <EditPurchase/>
            </Layout>

        </>
    )
}

export default PurchaseProduct;
