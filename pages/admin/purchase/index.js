import React from "react";
import AdminHeader from "../../../components/Admin/Header/AdminHeader";
import Layout from "../../../components/Admin/Layout/Layout";
import PurchaseList from "../../../components/Admin/Purchase/List/PurchaseList";
import PurchaseDetails from "../../../components/Admin/Purchase/List/PurchaseDetails";
import PurchasePaymentHistory from "../../../components/Admin/Purchase/List/PurchasePaymentHistory";

const Purchase = () => {

    return (
        <>
            <AdminHeader>Product Purchase list | Minimart</AdminHeader>
            <Layout>
                <section className="custom-card">
                    <div className="container-fluid">
                        <div className="row">
                            <PurchaseList/>
                            <div className="col-md-5">
                                <PurchaseDetails/>
                                <PurchasePaymentHistory/>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    );
}
export default Purchase;