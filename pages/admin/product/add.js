import AdminHeader from "../../../components/Admin/Header/AdminHeader";
import Layout from "../../../components/Admin/Layout/Layout";
import AddProduct from "../../../components/Admin/Product/Add/AddProduct";

const Product = () => {
  return (
    <>
      <AdminHeader>Add Product | Minimart</AdminHeader>
      <Layout>
        <AddProduct />
      </Layout>
    </>
  );
};

export default Product;
