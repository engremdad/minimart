import AdminHeader from "../../../../components/Admin/Header/AdminHeader";
import Layout from "../../../../components/Admin/Layout/Layout";

const UpdateProductIndex = () => {
  return (
    <>
      <AdminHeader>Edit Product | Minimart</AdminHeader>
      <Layout>
        <h1 className="mt-5 text-center mb-5">404 - Product Not Found</h1>
      </Layout>
    </>
  );
};

export default UpdateProductIndex;
