import React from "react";
import AdminHeader from "../../../components/Admin/Header/AdminHeader";
import Layout from "../../../components/Admin/Layout/Layout";
import AddBarcode from "../../../components/Admin/Barcode/Add/AddBarcode";

const Print = () => {
  return (
    <>
      <AdminHeader>Print Barcode | Minimart</AdminHeader>
      <Layout>
        <AddBarcode />
      </Layout>
    </>
  );
};

// Print.getInitialProps = async (ctx) => {

//     // Checked it is on the server
//     if (ctx.res) {

//         let cookies = ctx.req.headers.cookie;
//         let isLoggedIn = cookies ? cookies.includes('token') : false;

//         if (!isLoggedIn) {
//             ctx.res.writeHead(302, {Location: '/auth/login'});
//             ctx.res.end();
//         }

//     }

//     return {};

// }

export default Print;
