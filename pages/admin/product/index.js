import AdminHeader from "../../../components/Admin/Header/AdminHeader";
import Layout from "../../../components/Admin/Layout/Layout";
import ProductTable from "../../../components/Admin/Product/List/ProductTable";
import axios from "axios";
import React, {useState, useEffect} from "react";
import {options} from "../../../Helpers/Utils/Utils";
import Loader from "react-loader";

const Product = () => {
        const [products, setProducts] = useState("");
        const [loaded, setLoaded] = useState(false);

        useEffect(() => {
            let isMounted = true;
            const getProductList = async () => {
                const apiResponses = await axios.get(
                    `${process.env.api_backend_url}/getAllProduct?show=10`
                );
                return await apiResponses.data;
            };

            getProductList()
                .then((apiResponses) => {
                    if (apiResponses.success) {
                        isMounted && setProducts(apiResponses.data);
                        setLoaded(true);
                    } else {
                        throw "some thing goes wrong on server";
                    }
                })
                .catch((errors) => {
                    console.error(errors);
                });

            return () => {
                isMounted = false;
            };
        }, []);

        return (
            <>
                <AdminHeader>List Product | Minimart</AdminHeader>
                <Layout>{products ? <ProductTable products={products}/> :
                    <Loader loaded={loaded} options={options} className="spinner"/>}</Layout>
            </>
        );
    }
;

export default Product;
