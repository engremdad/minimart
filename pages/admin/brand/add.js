import AdminHeader from "../../../components/Admin/Header/AdminHeader";
import Layout from "../../../components/Admin/Layout/Layout";
import AddBrand from "../../../components/Admin/Brand/Add/AddBrand";

const CreateBrand = () => {
  return (
    <>
      <AdminHeader>Add Brand | Minimart</AdminHeader>
      <Layout>
        <AddBrand />
      </Layout>
    </>
  );
};

export default CreateBrand;
