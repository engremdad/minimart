import AdminHeader from "../../../components/Admin/Header/AdminHeader";
import Layout from "../../../components/Admin/Layout/Layout";
import BrandTable from "../../../components/Admin/Brand/List/BrandTable";
import axios from "axios";
import { useState, useEffect } from "react";

const Brand = () => {
  const [brands, setBrands] = useState("");

  useEffect(() => {
    let isMounted = true;
    const getBrandList = async () => {
      const apiResponses = await axios.get(
        `${process.env.api_backend_url}/getAllBrand?show=10`
      );
      return await apiResponses.data;
    };

    getBrandList()
      .then((apiResponses) => {
        if (apiResponses.success) {
          isMounted && setBrands(apiResponses.data);
        } else {
          throw "some thing goes wrong on server";
        }
      })
      .catch((errors) => {
        console.error(errors);
      });

    return () => {
      isMounted = false;
    };
  }, []);

  return (
    <>
      <AdminHeader>Brand List | Minimart</AdminHeader>
      <Layout>{brands && <BrandTable brands={brands} />}</Layout>
    </>
  );
};

export default Brand;
