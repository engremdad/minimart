import React from 'react';
import Layout from "../../components/Frontend/Layout/Layout";

const Faq = () => {
    return (
        <>
            <Layout>
                <section className="faq-section" id="faqSection">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-xl-3">
                                <div className="help-sticky">
                                    <div className="list-group rounded-0">
                                        <a href="#" className="list-group-item list-group-item-action active d-flex align-items-center">
                                            <i className="icon-question-circle mr-2"></i>
                                            <span>FAQ</span>
                                        </a>
                                        <a href="#" className="list-group-item list-group-item-action d-flex align-items-center">
                                            <i className="icon-investment mr-2"></i>
                                            <span>Refund Policy</span>
                                        </a>
                                        <a href="#" className="list-group-item list-group-item-action d-flex align-items-center">
                                            <i className="icon-share-alt mr-2"></i>
                                            <span>Return Policy</span>
                                        </a>
                                        <a href="#" className="list-group-item list-group-item-action d-flex align-items-center">
                                            <i className="icon-comments-alt mr-2"></i>
                                            <span>How to Use</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-8">
                                <div className="row">
                                    <div className="col-md-12">
                                        <h3 className="mb-3 d-flex align-items-center faq-type"><i className="icon-question-circle"></i> <span>General Questions</span></h3>
                                        <div className="gq-item shadow-sm">
                                            <p>
                                                <a className="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseExample"
                                                    aria-expanded="false" aria-controls="collapseExample">
                                                    <i className="fas fa-plus-square"></i>
                                                    <i className="fas fa-minus-square"></i>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing
                                            elit ?
                                        </a>
                                            </p>
                                            <div className="collapse" id="collapseExample">
                                                <div className="card card-body gq-content border-0">
                                                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson
                                                    ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                                            nesciunt sapiente ea proident.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="gq-item shadow-sm">
                                            <p>
                                                <a className="btn collapsed" type="button" data-toggle="collapse"
                                                    data-target="#collapseExample1" aria-expanded="false" aria-controls="collapseExample1">
                                                    <i className="fas fa-plus-square"></i>
                                                    <i className="fas fa-minus-square"></i>
                                            Curabitur posuere sapien vitae nisl tincidunt, eget
                                            ornare dui pretium ?
                                        </a>
                                            </p>
                                            <div className="collapse" id="collapseExample1">
                                                <div className="card card-body gq-content border-0">
                                                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson
                                                    ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                                                nesciunt sapiente ea proident. </p>
                                                    <ol type="a" className="">
                                                        <li>Hic quae repellat doloremque!</li>
                                                        <li>Hic quae repellat doloremque!</li>
                                                        <li>Hic quae repellat doloremque!</li>
                                                        <li>Hic quae repellat doloremque!</li>
                                                        <li>Hic quae repellat doloremque!</li>
                                                    </ol>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="gq-item shadow-sm">
                                            <p>
                                                <a className="btn collapsed" type="button" data-toggle="collapse"
                                                    data-target="#collapseExample2" aria-expanded="false" aria-controls="collapseExample2">
                                                    <i className="fas fa-plus-square"></i>
                                                    <i className="fas fa-minus-square"></i>
                                            Curabitur posuere sapien vitae nisl tincidunt, eget
                                            ornare dui pretium ?
                                        </a>
                                            </p>
                                            <div className="collapse" id="collapseExample2">
                                                <div className="card card-body gq-content border-0">
                                                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson
                                                    ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                                                nesciunt sapiente ea proident.</p>
                                                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson
                                                    ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                                            nesciunt sapiente ea proident.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row mt-3">
                                    <div className="col-md-12">
                                        <h3 className="mb-3 d-flex align-items-center faq-type"><i className="icon-question-circle"></i><span>Sales Questions</span></h3>
                                        <div className="gq-item shadow-sm">
                                            <p>
                                                <a className="btn collapsed" type="button" data-toggle="collapse"
                                                    data-target="#collapseExample4" aria-expanded="false" aria-controls="collapseExample4">
                                                    <i className="fas fa-plus-square"></i>
                                                    <i className="fas fa-minus-square"></i>
                                Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit ?
                            </a>
                                            </p>
                                            <div className="collapse" id="collapseExample4">
                                                <div className="card card-body gq-content">
                                                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson
                                                    ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                                                    nesciunt sapiente ea proident.
                                </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="gq-item shadow-sm">
                                            <p>
                                                <a className="btn collapsed" type="button" data-toggle="collapse"
                                                    data-target="#collapseExample5" aria-expanded="false" aria-controls="collapseExample5">
                                                    <i className="fas fa-plus-square"></i>
                                                    <i className="fas fa-minus-square"></i>
                                Curabitur posuere sapien vitae nisl tincidunt, eget
                                ornare dui pretium ?
                            </a>
                                            </p>
                                            <div className="collapse" id="collapseExample5">
                                                <div className="card card-body gq-content">
                                                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson
                                                    ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                                                    nesciunt sapiente ea proident.
                                </p>
                                                    <ol type="a">
                                                        <li>Hic quae repellat doloremque.</li>
                                                        <li>Morbi at dolor ac mauris laoreet lacinia.</li>
                                                        <li>Duis volutpat ipsum vitae mi egestas pulvinar.</li>
                                                        <li>Sed imperdiet dolor eu porta posuere.</li>
                                                        <li>Pellentesque semper erat in convallis ornare.</li>
                                                    </ol>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="gq-item shadow-sm">
                                            <p>
                                                <a className="btn collapsed" type="button" data-toggle="collapse"
                                                    data-target="#collapseExample6" aria-expanded="false" aria-controls="collapseExample6">
                                                    <i className="fas fa-plus-square"></i>
                                                    <i className="fas fa-minus-square"></i>
                                Curabitur posuere sapien vitae nisl tincidunt, eget
                                ornare dui pretium ?
                            </a>
                                            </p>
                                            <div className="collapse" id="collapseExample6">
                                                <div className="card card-body gq-content">
                                                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson
                                                    ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                                                    nesciunt sapiente ea proident.
                                </p>
                                                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson
                                                    ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                                                    nesciunt sapiente ea proident.
                                </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row mt-3">
                                    <div className="col-md-12">
                                        <h3 className="mb-3 d-flex align-items-center faq-type"><i className="icon-question-circle"></i><span>Support Questions</span></h3>
                                        <div className="gq-item shadow-sm">
                                            <p>
                                                <a className="btn collapsed" type="button" data-toggle="collapse"
                                                    data-target="#collapseExample7" aria-expanded="false" aria-controls="collapseExample7">
                                                    <i className="fas fa-plus-square"></i>
                                                    <i className="fas fa-minus-square"></i>
                                Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit ?
                            </a>
                                            </p>
                                            <div className="collapse" id="collapseExample7">
                                                <div className="card card-body gq-content">
                                                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson
                                                    ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                                                    nesciunt sapiente ea proident.
                                </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="gq-item shadow-sm">
                                            <p>
                                                <a className="btn collapsed" type="button" data-toggle="collapse"
                                                    data-target="#collapseExample8" aria-expanded="false" aria-controls="collapseExample8">
                                                    <i className="fas fa-plus-square"></i>
                                                    <i className="fas fa-minus-square"></i>
                                Curabitur posuere sapien vitae nisl tincidunt, eget
                                ornare dui pretium ?
                            </a>
                                            </p>
                                            <div className="collapse" id="collapseExample8">
                                                <div className="card card-body gq-content">
                                                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson
                                                    ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                                                    nesciunt sapiente ea proident.
                                </p>
                                                    <ol type="a">
                                                        <li>Hic quae repellat doloremque!</li>
                                                        <li>Hic quae repellat doloremque!</li>
                                                        <li>Hic quae repellat doloremque!</li>
                                                        <li>Hic quae repellat doloremque!</li>
                                                        <li>Hic quae repellat doloremque!</li>
                                                    </ol>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="gq-item shadow-sm">
                                            <p>
                                                <a className="btn collapsed" type="button" data-toggle="collapse"
                                                    data-target="#collapseExample9" aria-expanded="false" aria-controls="collapseExample9">
                                                    <i className="fas fa-plus-square"></i>
                                                    <i className="fas fa-minus-square"></i>
                                Curabitur posuere sapien vitae nisl tincidunt, eget
                                ornare dui pretium ?
                            </a>
                                            </p>
                                            <div className="collapse" id="collapseExample9">
                                                <div className="card card-body gq-content">
                                                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson
                                                    ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                                                    nesciunt sapiente ea proident.
                                </p>
                                                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson
                                                    ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                                                    nesciunt sapiente ea proident.
                                </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </section>
            </Layout>
        </>
    );
};

export default Faq;