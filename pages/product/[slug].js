import {useRouter} from "next/router";
import SiteHeader from "../../components/Frontend/Header/SiteHeader";
import Layout from "../../components/Frontend/Layout/Layout";
import React, {useRef, useState} from "react";

import * as process from "../../next.config";

import {
    EmailIcon,
    EmailShareButton,
    FacebookIcon,
    FacebookShareButton,
    InstapaperIcon,
    InstapaperShareButton,
    LinkedinIcon,
    LinkedinShareButton,
    TwitterIcon,
    TwitterShareButton,
    WhatsappIcon,
    WhatsappShareButton
} from "react-share";

import axios from "axios";
import {ADD_TO_CART} from "../../store/frontend/actions/cartAction";
import {connect} from "react-redux";

const ProductDetails = (props) => {

    const router = useRouter();
    const {slug} = router.query;

    const productUrl = "http://localhost:3000" + router.asPath;

    const refDiv = useRef(null);
    const [count, setCount] = useState(0);

    return (
        <>
            <SiteHeader>{slug}</SiteHeader>
            <Layout>
                <section className="class_name" ref={refDiv}>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <section className="product-details-main">
                                    <div className="container-fluid">
                                        <div className="product-details-wrap">
                                            <div className="pdw-top">
                                                <div className="pwdt-left pwdt-grid">
                                                    <div id="productDisplayCarousel"
                                                         className="carousel slide carousel-fade"
                                                         data-ride="carousel">
                                                        <div className="carousel-inner">
                                                            {props.product.product_image.map((image, index) =>
                                                                <div key={index}
                                                                     className={`carousel-item ${index ? "" : "active"}`}>
                                                                    <img src={image}
                                                                         className="d-block w-100" alt="Product Name"/>
                                                                </div>
                                                            )}
                                                        </div>
                                                        <ol className="carousel-indicators">
                                                            {props.product.product_image.map((image, index) =>
                                                                <li key={index} data-target="#productDisplayCarousel"
                                                                    data-slide-to={index}
                                                                    className={`${index ? "" : "active"}`}>
                                                                    <img className="d-block w-100"
                                                                         src={image} alt="Product Name"/>
                                                                </li>
                                                            )}
                                                        </ol>

                                                    </div>
                                                </div>
                                                <div className="pwdt-right pwdt-grid">
                                                    <div className="product-title">
                                                        <h1>{props.product.name}</h1>
                                                    </div>
                                                    <div className="product-review">
                                                        <div className="review-summary">
                                                            <i className="icon-star-full"/>
                                                            <i className="icon-star-full"/>
                                                            <i className="icon-star-full"/>
                                                            <i className="icon-star-half"/>
                                                            <i className="icon-star-empty"/>
                                                            <span>(5 reviews)</span>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    <div className="product-qan-price-off">
                                                        <div className="product-qan-price">
                                                            <div className="product-quantity">
                                                                <span>500 gm</span>
                                                            </div>
                                                            <div className="product-price">
                                                                <span className="off-price">$399</span>
                                                                <span className="original-price">$500</span>
                                                            </div>
                                                        </div>
                                                        <div className="off-percentage">
                                                            <span>20% off</span>
                                                        </div>
                                                    </div>
                                                    <div className="product-small-des">
                                                        <p>
                                                            {props.product.description}
                                                        </p>
                                                    </div>
                                                    <div className="availability-sku">
                                                        <div className="sku">
                                                            SKU : <span>{props.product.code}</span>
                                                        </div>
                                                        <div className="availability">
                                                            AVAILABILITY : <span
                                                            className="badge badge-pill badge-primary">In Stock</span>
                                                        </div>
                                                    </div>
                                                    <div className="add-to-cart">
                                                        <div className="cart-count input-group">
                                                            <button className="decrise"
                                                                    onClick={() => {
                                                                        if (count > 0)
                                                                            setCount(count - 1);
                                                                    }}>
                                                            </button>
                                                            <input name="qty" type="text" readOnly value={count}/>
                                                            <button className="incrise"
                                                                    onClick={() => setCount(count + 1)}>
                                                            </button>
                                                        </div>
                                                        <div className="acrt-btn">
                                                            <button
                                                                className="btn btn-atc"
                                                                onClick={event => {
                                                                    event.preventDefault();
                                                                    props.addToCart(props.product, count)
                                                                }}
                                                            >
                                                                Add to cart
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div className="social-share">
                                                        <div className="share-icon">
                                                            <FacebookShareButton
                                                                url={productUrl}
                                                                quote={props.product.description}
                                                                hashtag="#camperstribe">
                                                                <FacebookIcon size={36}/>
                                                            </FacebookShareButton>
                                                            <TwitterShareButton
                                                                url={productUrl}
                                                                quote={props.product.description}
                                                                hashtag="#camperstribe">
                                                                <TwitterIcon size={36}/>
                                                            </TwitterShareButton>
                                                            <LinkedinShareButton
                                                                url={productUrl}
                                                                quote={props.product.description}
                                                                hashtag="#camperstribe">
                                                                <LinkedinIcon size={36}/>
                                                            </LinkedinShareButton>
                                                            <WhatsappShareButton
                                                                url={productUrl}
                                                                quote={props.product.description}
                                                                hashtag="#camperstribe">
                                                                <WhatsappIcon size={36}/>
                                                            </WhatsappShareButton>
                                                            <InstapaperShareButton
                                                                url={productUrl}
                                                                quote={props.product.description}
                                                                hashtag="#camperstribe">
                                                                <InstapaperIcon size={36}/>
                                                            </InstapaperShareButton>
                                                            <EmailShareButton
                                                                url={productUrl}
                                                                quote={props.product.description}
                                                                hashtag="#camperstribe">
                                                                <EmailIcon size={36}/>
                                                            </EmailShareButton>

                                                        </div>
                                                        <div className="add-to-wishlist">
                                                            <button className="btn btn-atw">
                                                                <i className="icon-heart-regular"/>
                                                                <span>Add to wishlist</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div className="pay-with">
                                                        <img src="/img/paywith.png" alt="Pay with"/>
                                                    </div>

                                                </div>
                                            </div>
                                            <div className="pdw-bottom"/>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>

        </>
    );
};

ProductDetails.getInitialProps = async (ctx) => {

    const api_url = process.env.api_url;

    let slug = ctx.query.slug;

    if (slug) {
        let response = await axios.get(`${api_url}/products/${slug}`).then((result) => {
            return result.data;
        }).catch((exception) => {
            return exception;
        });

        if (response.success) {
            return {product: response.data}
        } else {
            return {product: []}
        }
    } else {
        return {product: []}
    }
}


const mapDispatchToProps = (dispatch) => {

    return {
        addToCart: (product, qty) => dispatch({type: ADD_TO_CART, payload: {product, qty}}),
    }
}
export default connect(null, mapDispatchToProps)(ProductDetails)
