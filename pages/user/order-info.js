import SiteHeader from "../../components/Frontend/Header/SiteHeader";
import React, {useEffect, useState} from "react";
import Layout from "../../components/Frontend/Layout/Layout";
import OrderList from "../../components/Frontend/User/OrderList";
import OrderDetails from "../../components/Frontend/User/OrderDetails";
import OrderProcessing from "../../components/Frontend/User/OrderProcessing";
import process from "../../next.config";
import axios from "axios";
import OrderPaymentHistory from "../../components/Frontend/User/OrderPaymentHistory";

const OrderInfo = () => {
    const [ListOrder, setListOrder] = useState('');
    const [OrderInfo, setOrderInfo] = useState('');

    const ListOrderData = async () => {
        const url = `${process.env.api_url}/get-user-orders`;
        return await axios.get(url).then((apiResponse) => (apiResponse.data)).catch((errors) => (errors));
    }

    const showOrderDetails = async (orderId) => {
        const url = `${process.env.api_url}/get-order-info`;
        const response = await axios.post(url,{id: orderId}).then((apiResponse) => (apiResponse.data)).catch((errors) => (errors));
        setOrderInfo(response.data);
        //console.log(OrderInfo)

    }

    useEffect(() => {
        let mounted = true;

        ListOrderData().then((orderList) => {
            mounted && (
                orderList.success && setListOrder(orderList.data) || !orderList.success && setListOrder('')
            )
        });

        return () => {
            mounted = false
        };
    }, [])

    return (

        <>
            <SiteHeader>Minimart</SiteHeader>
            <Layout>

                <section className="custom-card">
                    <div className="container-fluid">
                        <div className="row">
                            <OrderList showOrderDetails={showOrderDetails} ListOrder={ListOrder}/>
                            <div className="col-md-5">
                                <OrderDetails OrderInfo={OrderInfo}/>
                                <OrderProcessing OrderInfo={OrderInfo} />
                                <OrderPaymentHistory OrderInfo={OrderInfo} />
                            </div>
                        </div>
                    </div>
                </section>
            </Layout>
        </>
    );
}
export default OrderInfo;