import React, {useEffect} from 'react';
import SiteHeader from "../../components/Frontend/Header/SiteHeader";
import Layout from "../../components/Frontend/Layout/Layout";
import DeliveryDetails from "../../components/Frontend/Checkout/DeliveryDetails";
import PersonalDetails from "../../components/Frontend/Checkout/PersonalDetails";
import ProceedCheckout from "../../components/Frontend/Checkout/ProceedCheckout";
import PaymentOption from "../../components/Frontend/Checkout/PaymentOption";
import {connect} from "react-redux";
import {
    SET_ALL_AREAS,
    SET_AREAS,
    SET_AUTH_USER,
    SET_CUSTOMER_ALL_ADDRESSES,
    SET_LOCATIONS
} from "../../store/frontend/actions/deliveryActions";
import axios from "axios";

const Checkout = (props) => {

    const api_url = process.env.mode === 'development' ? process.env.api_url_dev : process.env.api_url;

    useEffect(async () => {
        if (props.serverSide) {
            props.setCustomerAllAddresses(props.address);
            props.setLocations(props.locations);
            props.setAreas(props.areas);
            props.setAllAreas(props.areas);
            props.setAuthUser(props.authUser);
        } else {
            await getDeliveryData();
        }
    }, []);


    const getDeliveryData = async () => {

        let customerAddressApiRequest = axios.get(`${api_url}/customer-address-by-id`);
        let locationApiRequest = axios.get(`${api_url}/locations`);
        let areaApiRequest = axios.get(`${api_url}/areas`);
        let authUserRequest = axios.get(`${api_url}/auth/customers/authUser`);

        await axios.all([customerAddressApiRequest, locationApiRequest, areaApiRequest, authUserRequest])
            .then(
                axios.spread((...responses) => {

                    // Customer Address Response
                    if (responses[0].data.success) {
                        props.setCustomerAllAddresses(responses[0].data.data);
                    }


                    // Location Response
                    if (responses[1].data.success) {
                        props.setLocations(responses[1].data.data);
                    }

                    // Area Response
                    if (responses[2].data.success) {
                        props.setAreas(responses[2].data.data);
                        props.setAllAreas(responses[2].data.data);
                    }

                    // Auth User Profile Response
                    if (responses[3].data.success) {
                        props.setAuthUser(responses[3].data.data);
                    }

                }))
            .catch(errors => {
                console.log(errors)
            });
    }

    return (
        <>
            <SiteHeader>Minimart</SiteHeader>
            <Layout>

                <section className="checkout">
                    <div className="container-fluid">

                        <div className="row">
                            <div className="col-xl-8 offset-xl-2">

                                <div className="card checkout-details-card rounded-0 mb-4">
                                    <DeliveryDetails/>
                                </div>


                                <div className="card checkout-details-card rounded-0 mb-4">

                                    <PersonalDetails/>

                                </div>


                                <div className="card checkout-details-card rounded-0 mb-4">

                                    <ProceedCheckout/>

                                </div>


                                <div className="card checkout-details-card rounded-0 mb-4">
                                    <PaymentOption/>
                                </div>

                            </div>
                        </div>
                    </div>
                </section>

            </Layout>
        </>
    );
};

Checkout.getInitialProps = async (ctx) => {

    const api_url = process.env.mode === 'development' ? process.env.api_url_dev : process.env.api_url;

    let address = [];
    let locations = [];
    let areas = [];
    let serverSide = false;
    let authUser = '';

    if (ctx.req) {
        serverSide = true;

        let tokenData = ctx.req.headers.cookie ?
            ctx.req.headers.cookie.split('; ')
                .find(row => row.startsWith('token')) : '';

        let token = tokenData ? tokenData.split('=')[1] : '';

        axios.defaults.headers.Authorization = `Bearer ${token}`;


        let customerAddressApiRequest = axios.get(`${api_url}/customer-address-by-id`);
        let locationApiRequest = axios.get(`${api_url}/locations`);
        let areaApiRequest = axios.get(`${api_url}/areas`);
        let authUserRequest = axios.get(`${api_url}/auth/customers/authUser`);


        await axios.all([customerAddressApiRequest, locationApiRequest, areaApiRequest, authUserRequest])
            .then(
                axios.spread((...responses) => {

                    // Customer Address Response
                    if (responses[0].data.success) {
                        address = responses[0].data.data;
                    }

                    // Location Response
                    if (responses[1].data.success) {
                        locations = responses[1].data.data;
                    }

                    // Area Response
                    if (responses[2].data.success) {
                        areas = responses[2].data.data;
                    }

                    // Auth User Profile Response
                    if (responses[3].data.success) {
                        authUser = responses[3].data.data;
                    }


                }))
            .catch(errors => {
                console.log(errors)
            });

        return {address: address, locations: locations, areas: areas, authUser: authUser, serverSide: serverSide};
    }


    return {address: address, locations: locations, areas: areas, authUser: authUser, serverSide: serverSide};

}

const mapDispatchToProps = (dispatch) => {
    return {
        setCustomerAllAddresses: (addresses) => dispatch({type: SET_CUSTOMER_ALL_ADDRESSES, payload: {addresses}}),
        setLocations: (locations) => dispatch({type: SET_LOCATIONS, payload: {locations}}),
        setAreas: (areas) => dispatch({type: SET_AREAS, payload: {areas}}),
        setAllAreas: (allAreas) => dispatch({type: SET_ALL_AREAS, payload: {allAreas}}),
        setAuthUser: (authUser) => dispatch({type: SET_AUTH_USER, payload: {authUser}})
    }
}

export default connect(null, mapDispatchToProps)(Checkout);