import axios from "axios";
import Router from "next/router";
class Storage {
  storeToken(token, expires) {
    document.cookie =
      "token=" + token + ";" + "expires=" + expires + "; path=/";
  }

  storeUser(user, expires) {
    document.cookie = "user=" + user + ";" + "expires=" + expires + "; path=/";
  }

  store(authData) {
    let authUser = JSON.parse(authData);
    let token = authUser.access_token;
    let expires = new Date(Date.now() + authUser.expires_in).toUTCString();
    let name = authUser.user.name;

    this.storeToken(token, expires);
    this.storeUser(name, expires);
  }

  getToken() {
    return this.getCookie("token");
  }

  getUser() {
    return this.getCookie("user");
  }

  isLoggedIn() {
    this.getToken() ? (axios.defaults.baseURL = process.env.api_url) : "";
    this.getToken()
      ? (axios.defaults.headers.Authorization = `Bearer ${this.getToken()}`)
      : "";
    return !!this.getToken();
  }

  clear() {
    document.cookie = "token=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/";
    document.cookie = "user=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/";
  }

  getCookie(cookieName) {
    let cookies = document.cookie;

    let tokenData = cookies
      ? cookies.split("; ").find((row) => row.startsWith(cookieName))
      : "";

    return tokenData ? tokenData.split("=")[1] : "";
  }
}

export default Storage = new Storage();
